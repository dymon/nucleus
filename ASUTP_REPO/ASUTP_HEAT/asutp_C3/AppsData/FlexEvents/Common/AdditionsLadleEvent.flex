<Event>;;;
Flags;1;;
Operation;OPC.AdditionsLadleEvent;;
;;;
<Arguments>;;;
Converter1;Takeover;S7:[PLC12]DB2,INT42;Takeover ������� � ����
Converter1;Weight1;S7:[PLC12]DB2,REAL44;���.�������,�������  1
Converter1;Weight2;S7:[PLC12]DB2,REAL48;���.�������,�������  2
Converter1;Weight3;S7:[PLC12]DB2,REAL52;���.�������,�������  3
Converter1;Weight4;S7:[PLC12]DB2,REAL56;���.�������,�������  4
Converter1;Weight5;S7:[PLC12]DB2,REAL60;���.�������,�������  5
Converter1;Weight6;S7:[PLC12]DB2,REAL64;���.�������,�������  6
Converter1;Weight7;S7:[PLC12]DB2,REAL68;���.�������,�������  7
Converter1;Weight8;S7:[PLC12]DB2,REAL72;���.�������,�������  8
Converter1;Weight9;S7:[PLC12]DB2,REAL76;���.�������,�������  9
Converter1;Weight10;S7:[PLC12]DB2,REAL80;���.�������,�������  10
Converter1;DryDuration1;S7:[PLC12]DB2,W84;���.����������,����� �����,��������  �������� 1
Converter1;DryDuration2;S7:[PLC12]DB2,W86;���.����������,����� �����,��������  �������� 2
Converter1;DryDuration3;S7:[PLC12]DB2,W88;���.����������,����� �����,��������  �������� 3
Converter1;DryDuration4;S7:[PLC12]DB2,W90;���.����������,����� �����,��������  �������� 4
Converter1;DryDuration5;S7:[PLC12]DB2,W92;���.����������,����� �����,��������  �������� 5
Converter1;DryDuration6;S7:[PLC12]DB2,W94;���.����������,����� �����,��������  �������� 6
Converter1;DryDuration7;S7:[PLC12]DB2,W96;���.����������,����� �����,��������  �������� 7
Converter1;DryDuration8;S7:[PLC12]DB2,W98;���.����������,����� �����,��������  �������� 8
Converter1;DryDuration9;S7:[PLC12]DB2,W100;���.����������,����� �����,��������  �������� 9
Converter1;DryDuration10;S7:[PLC12]DB2,W102;���.����������,����� �����,��������  �������� 10
Converter1;Material1;S7:[PLC12]DB1,B374,6;������� ��� ��������� � ��������� 1 
Converter1;Material2;S7:[PLC12]DB1,B380,6;������� ��� ��������� � ��������� 2
Converter1;Material3;S7:[PLC12]DB1,B386,6;������� ��� ��������� � ��������� 3
Converter1;Material4;S7:[PLC12]DB1,B392,6;������� ��� ��������� � ��������� 4
Converter1;Material5;S7:[PLC12]DB1,B398,6;������� ��� ��������� � ��������� 5
Converter1;Material6;S7:[PLC12]DB1,B404,6;������� ��� ��������� � ��������� 6
Converter1;Material7;S7:[PLC12]DB1,B410,6;������� ��� ��������� � ��������� 7
Converter1;Material8;S7:[PLC12]DB1,B416,6;������� ��� ��������� � ��������� 8
Converter1;Material9;S7:[PLC12]DB1,B422,6;������� ��� ��������� � ��������� 9
Converter1;Material10;S7:[PLC12]DB1,B428,6;������� ��� ��������� � ��������� 10
Converter1;Bunker2Material1;S7:[PLC12]DB2,B110,6;�������� ���������� � ������� 1
Converter1;Bunker2Material2;S7:[PLC12]DB2,B116,6;�������� ���������� � ������� 2
Converter1;Bunker2Material3;S7:[PLC12]DB2,B122,6;�������� ���������� � ������� 3
Converter1;Bunker2Material4;S7:[PLC12]DB2,B128,6;�������� ���������� � ������� 4
Converter1;Bunker2Material5;S7:[PLC12]DB2,B134,6;�������� ���������� � ������� 5
Converter1;Bunker2Material6;S7:[PLC12]DB2,B140,6;�������� ���������� � ������� 6
Converter1;Bunker2Material7;S7:[PLC12]DB2,B146,6;�������� ���������� � ������� 7
Converter1;Bunker2Material8;S7:[PLC12]DB2,B152,6;�������� ���������� � ������� 8
Converter1;Bunker2Material9;S7:[PLC12]DB2,B158,6;�������� ���������� � ������� 9
Converter1;Bunker2Material10;S7:[PLC12]DB2,B164,6;�������� ���������� � ������� 10
Converter1;Bunker2Material11;S7:[PLC12]DB2,B170,6;�������� ���������� � ������� 11
Converter1;Bunker2Material12;S7:[PLC12]DB2,B176,6;�������� ���������� � ������� 12
Converter1;Bunker2Material13;S7:[PLC12]DB2,B182,6;�������� ���������� � ������� 13
Converter1;Bunker2Material14;S7:[PLC12]DB2,B188,6;�������� ���������� � ������� 14
Converter1;Bunker2Material15;S7:[PLC12]DB2,B194,6;�������� ���������� � ������� 15
Converter1;Bunker2Material16;S7:[PLC12]DB2,B200,6;�������� ���������� � ������� 16
Converter1;Bunker2Material17;S7:[PLC12]DB2,B206,6;�������� ���������� � ������� 17
Converter1;W10;S7:[PLC12]DB2,W214;������ ������� ������� ��� ��������� �� W10
Converter1;W11;S7:[PLC12]DB2,W216;������ ������� ������� ��� ��������� �� W11
;;;
Converter2;Takeover;S7:[PLC22]DB2,INT42;Takeover ������� � ����
Converter2;Weight1;S7:[PLC22]DB2,REAL44;���.�������,�������  1
Converter2;Weight2;S7:[PLC22]DB2,REAL48;���.�������,�������  2
Converter2;Weight3;S7:[PLC22]DB2,REAL52;���.�������,�������  3
Converter2;Weight4;S7:[PLC22]DB2,REAL56;���.�������,�������  4
Converter2;Weight5;S7:[PLC22]DB2,REAL60;���.�������,�������  5
Converter2;Weight6;S7:[PLC22]DB2,REAL64;���.�������,�������  6
Converter2;Weight7;S7:[PLC22]DB2,REAL68;���.�������,�������  7
Converter2;Weight8;S7:[PLC22]DB2,REAL72;���.�������,�������  8
Converter2;Weight9;S7:[PLC22]DB2,REAL76;���.�������,�������  9
Converter2;Weight10;S7:[PLC22]DB2,REAL80;���.�������,�������  10
Converter2;DryDuration1;S7:[PLC22]DB2,W84;���.����������,����� �����,��������  �������� 1
Converter2;DryDuration2;S7:[PLC22]DB2,W86;���.����������,����� �����,��������  �������� 2
Converter2;DryDuration3;S7:[PLC22]DB2,W88;���.����������,����� �����,��������  �������� 3
Converter2;DryDuration4;S7:[PLC22]DB2,W90;���.����������,����� �����,��������  �������� 4
Converter2;DryDuration5;S7:[PLC22]DB2,W92;���.����������,����� �����,��������  �������� 5
Converter2;DryDuration6;S7:[PLC22]DB2,W94;���.����������,����� �����,��������  �������� 6
Converter2;DryDuration7;S7:[PLC22]DB2,W96;���.����������,����� �����,��������  �������� 7
Converter2;DryDuration8;S7:[PLC22]DB2,W98;���.����������,����� �����,��������  �������� 8
Converter2;DryDuration9;S7:[PLC22]DB2,W100;���.����������,����� �����,��������  �������� 9
Converter2;DryDuration10;S7:[PLC22]DB2,W102;���.����������,����� �����,��������  �������� 10
Converter2;Material1;S7:[PLC22]DB1,B374,6;������� ��� ��������� � ��������� 1 
Converter2;Material2;S7:[PLC22]DB1,B380,6;������� ��� ��������� � ��������� 2
Converter2;Material3;S7:[PLC22]DB1,B386,6;������� ��� ��������� � ��������� 3
Converter2;Material4;S7:[PLC22]DB1,B392,6;������� ��� ��������� � ��������� 4
Converter2;Material5;S7:[PLC22]DB1,B398,6;������� ��� ��������� � ��������� 5
Converter2;Material6;S7:[PLC22]DB1,B404,6;������� ��� ��������� � ��������� 6
Converter2;Material7;S7:[PLC22]DB1,B410,6;������� ��� ��������� � ��������� 7
Converter2;Material8;S7:[PLC22]DB1,B416,6;������� ��� ��������� � ��������� 8
Converter2;Material9;S7:[PLC22]DB1,B422,6;������� ��� ��������� � ��������� 9
Converter2;Material10;S7:[PLC22]DB1,B428,6;������� ��� ��������� � ��������� 10
Converter2;Bunker2Material1;S7:[PLC22]DB2,B110,6;�������� ���������� � ������� 1
Converter2;Bunker2Material2;S7:[PLC22]DB2,B116,6;�������� ���������� � ������� 2
Converter2;Bunker2Material3;S7:[PLC22]DB2,B122,6;�������� ���������� � ������� 3
Converter2;Bunker2Material4;S7:[PLC22]DB2,B128,6;�������� ���������� � ������� 4
Converter2;Bunker2Material5;S7:[PLC22]DB2,B134,6;�������� ���������� � ������� 5
Converter2;Bunker2Material6;S7:[PLC22]DB2,B140,6;�������� ���������� � ������� 6
Converter2;Bunker2Material7;S7:[PLC22]DB2,B146,6;�������� ���������� � ������� 7
Converter2;Bunker2Material8;S7:[PLC22]DB2,B152,6;�������� ���������� � ������� 8
Converter2;Bunker2Material9;S7:[PLC22]DB2,B158,6;�������� ���������� � ������� 9
Converter2;Bunker2Material10;S7:[PLC22]DB2,B164,6;�������� ���������� � ������� 10
Converter2;Bunker2Material11;S7:[PLC22]DB2,B170,6;�������� ���������� � ������� 11
Converter2;Bunker2Material12;S7:[PLC22]DB2,B176,6;�������� ���������� � ������� 12
Converter2;Bunker2Material13;S7:[PLC22]DB2,B182,6;�������� ���������� � ������� 13
Converter2;Bunker2Material14;S7:[PLC22]DB2,B188,6;�������� ���������� � ������� 14
Converter2;Bunker2Material15;S7:[PLC22]DB2,B194,6;�������� ���������� � ������� 15
Converter2;Bunker2Material16;S7:[PLC22]DB2,B200,6;�������� ���������� � ������� 16
Converter2;Bunker2Material17;S7:[PLC22]DB2,B206,6;�������� ���������� � ������� 17
Converter2;W10;S7:[PLC22]DB2,W214;������ ������� ������� ��� ��������� �� W10
Converter2;W11;S7:[PLC22]DB2,W216;������ ������� ������� ��� ��������� �� W11
;;;
Converter3;Takeover;S7:[PLC32]DB2,INT42;Takeover ������� � ����
Converter3;Weight1;S7:[PLC32]DB2,REAL44;���.�������,�������  1
Converter3;Weight2;S7:[PLC32]DB2,REAL48;���.�������,�������  2
Converter3;Weight3;S7:[PLC32]DB2,REAL52;���.�������,�������  3
Converter3;Weight4;S7:[PLC32]DB2,REAL56;���.�������,�������  4
Converter3;Weight5;S7:[PLC32]DB2,REAL60;���.�������,�������  5
Converter3;Weight6;S7:[PLC32]DB2,REAL64;���.�������,�������  6
Converter3;Weight7;S7:[PLC32]DB2,REAL68;���.�������,�������  7
Converter3;Weight8;S7:[PLC32]DB2,REAL72;���.�������,�������  8
Converter3;Weight9;S7:[PLC32]DB2,REAL76;���.�������,�������  9
Converter3;Weight10;S7:[PLC32]DB2,REAL80;���.�������,�������  10
Converter3;DryDuration1;S7:[PLC32]DB2,W84;���.����������,����� �����,��������  �������� 1
Converter3;DryDuration2;S7:[PLC32]DB2,W86;���.����������,����� �����,��������  �������� 2
Converter3;DryDuration3;S7:[PLC32]DB2,W88;���.����������,����� �����,��������  �������� 3
Converter3;DryDuration4;S7:[PLC32]DB2,W90;���.����������,����� �����,��������  �������� 4
Converter3;DryDuration5;S7:[PLC32]DB2,W92;���.����������,����� �����,��������  �������� 5
Converter3;DryDuration6;S7:[PLC32]DB2,W94;���.����������,����� �����,��������  �������� 6
Converter3;DryDuration7;S7:[PLC32]DB2,W96;���.����������,����� �����,��������  �������� 7
Converter3;DryDuration8;S7:[PLC32]DB2,W98;���.����������,����� �����,��������  �������� 8
Converter3;DryDuration9;S7:[PLC32]DB2,W100;���.����������,����� �����,��������  �������� 9
Converter3;DryDuration10;S7:[PLC32]DB2,W102;���.����������,����� �����,��������  �������� 10
Converter3;Material1;S7:[PLC32]DB1,B374,6;������� ��� ��������� � ��������� 1 
Converter3;Material2;S7:[PLC32]DB1,B380,6;������� ��� ��������� � ��������� 2
Converter3;Material3;S7:[PLC32]DB1,B386,6;������� ��� ��������� � ��������� 3
Converter3;Material4;S7:[PLC32]DB1,B392,6;������� ��� ��������� � ��������� 4
Converter3;Material5;S7:[PLC32]DB1,B398,6;������� ��� ��������� � ��������� 5
Converter3;Material6;S7:[PLC32]DB1,B404,6;������� ��� ��������� � ��������� 6
Converter3;Material7;S7:[PLC32]DB1,B410,6;������� ��� ��������� � ��������� 7
Converter3;Material8;S7:[PLC32]DB1,B416,6;������� ��� ��������� � ��������� 8
Converter3;Material9;S7:[PLC32]DB1,B422,6;������� ��� ��������� � ��������� 9
Converter3;Material10;S7:[PLC32]DB1,B428,6;������� ��� ��������� � ��������� 10
Converter3;Bunker2Material1;S7:[PLC32]DB2,B110,6;�������� ���������� � ������� 1
Converter3;Bunker2Material2;S7:[PLC32]DB2,B116,6;�������� ���������� � ������� 2
Converter3;Bunker2Material3;S7:[PLC32]DB2,B122,6;�������� ���������� � ������� 3
Converter3;Bunker2Material4;S7:[PLC32]DB2,B128,6;�������� ���������� � ������� 4
Converter3;Bunker2Material5;S7:[PLC32]DB2,B134,6;�������� ���������� � ������� 5
Converter3;Bunker2Material6;S7:[PLC32]DB2,B140,6;�������� ���������� � ������� 6
Converter3;Bunker2Material7;S7:[PLC32]DB2,B146,6;�������� ���������� � ������� 7
Converter3;Bunker2Material8;S7:[PLC32]DB2,B152,6;�������� ���������� � ������� 8
Converter3;Bunker2Material9;S7:[PLC32]DB2,B158,6;�������� ���������� � ������� 9
Converter3;Bunker2Material10;S7:[PLC32]DB2,B164,6;�������� ���������� � ������� 10
Converter3;Bunker2Material11;S7:[PLC32]DB2,B170,6;�������� ���������� � ������� 11
Converter3;Bunker2Material12;S7:[PLC32]DB2,B176,6;�������� ���������� � ������� 12
Converter3;Bunker2Material13;S7:[PLC32]DB2,B182,6;�������� ���������� � ������� 13
Converter3;Bunker2Material14;S7:[PLC32]DB2,B188,6;�������� ���������� � ������� 14
Converter3;Bunker2Material15;S7:[PLC32]DB2,B194,6;�������� ���������� � ������� 15
Converter3;Bunker2Material16;S7:[PLC32]DB2,B200,6;�������� ���������� � ������� 16
Converter3;Bunker2Material17;S7:[PLC32]DB2,B206,6;�������� ���������� � ������� 17
Converter3;W10;S7:[PLC32]DB2,W214;������ ������� ������� ��� ��������� �� W10
Converter3;W11;S7:[PLC32]DB2,W216;������ ������� ������� ��� ��������� �� W11
