<Event>;;;
Flags;1;;
Operation;OPC.AdditionsConverterEvent;;
;;;
<Arguments>;;;
Converter1;Takeover;S7:[PLC12]DB2,INT0;Takeover ������� � ���������
Converter1;Weight1;S7:[PLC12]DB2,REAL2;���.�������,�������  1
Converter1;Weight2;S7:[PLC12]DB2,REAL6;���.�������,�������  2
Converter1;Weight3;S7:[PLC12]DB2,REAL10;���.�������,�������  3
Converter1;Weight4;S7:[PLC12]DB2,REAL14;���.�������,�������  4
Converter1;Weight5;S7:[PLC12]DB2,REAL18;���.�������,�������  5
Converter1;Weight6;S7:[PLC12]DB2,REAL22;���.�������,�������  6
Converter1;Weight7;S7:[PLC12]DB2,REAL26;���.�������,�������  7
Converter1;Weight8;S7:[PLC12]DB2,REAL30;���.�������,�������  8
Converter1;Weight9;S7:[PLC12]DB2,REAL34;���.�������,�������  9
Converter1;Weight10;S7:[PLC12]DB2,REAL38;���.�������,�������  10
Converter1;Material1;S7:[PLC12]DB1,B272,6;������� ��� ��������� � ��������� 1 
Converter1;Material2;S7:[PLC12]DB1,B278,6;������� ��� ��������� � ��������� 2
Converter1;Material3;S7:[PLC12]DB1,B284,6;������� ��� ��������� � ��������� 3
Converter1;Material4;S7:[PLC12]DB1,B290,6;������� ��� ��������� � ��������� 4
Converter1;Material5;S7:[PLC12]DB1,B296,6;������� ��� ��������� � ��������� 5
Converter1;Material6;S7:[PLC12]DB1,B302,6;������� ��� ��������� � ��������� 6
Converter1;Material7;S7:[PLC12]DB1,B308,6;������� ��� ��������� � ��������� 7
Converter1;Material8;S7:[PLC12]DB1,B314,6;������� ��� ��������� � ��������� 8
Converter1;Material9;S7:[PLC12]DB1,B320,6;������� ��� ��������� � ��������� 9
Converter1;Material10;S7:[PLC12]DB1,B326,6;������� ��� ��������� � ��������� 10
Converter1;Bunker2Material1;S7:[PLC12]DB2,B110,6;�������� ���������� � ������� 1
Converter1;Bunker2Material2;S7:[PLC12]DB2,B116,6;�������� ���������� � ������� 2
Converter1;Bunker2Material3;S7:[PLC12]DB2,B122,6;�������� ���������� � ������� 3
Converter1;Bunker2Material4;S7:[PLC12]DB2,B128,6;�������� ���������� � ������� 4
Converter1;Bunker2Material5;S7:[PLC12]DB2,B134,6;�������� ���������� � ������� 5
Converter1;Bunker2Material6;S7:[PLC12]DB2,B140,6;�������� ���������� � ������� 6
Converter1;Bunker2Material7;S7:[PLC12]DB2,B146,6;�������� ���������� � ������� 7
Converter1;Bunker2Material8;S7:[PLC12]DB2,B152,6;�������� ���������� � ������� 8
Converter1;Bunker2Material9;S7:[PLC12]DB2,B158,6;�������� ���������� � ������� 9
Converter1;Bunker2Material10;S7:[PLC12]DB2,B164,6;�������� ���������� � ������� 10
Converter1;Bunker2Material11;S7:[PLC12]DB2,B170,6;�������� ���������� � ������� 11
Converter1;Bunker2Material12;S7:[PLC12]DB2,B176,6;�������� ���������� � ������� 12
Converter1;Bunker2Material13;S7:[PLC12]DB2,B182,6;�������� ���������� � ������� 13
Converter1;Bunker2Material14;S7:[PLC12]DB2,B188,6;�������� ���������� � ������� 14
Converter1;Bunker2Material15;S7:[PLC12]DB2,B194,6;�������� ���������� � ������� 15
Converter1;Bunker2Material16;S7:[PLC12]DB2,B200,6;�������� ���������� � ������� 16
Converter1;Bunker2Material17;S7:[PLC12]DB2,B206,6;�������� ���������� � ������� 17
;;;
Converter2;Takeover;S7:[PLC22]DB2,INT0;Takeover ������� � ���������
Converter2;Weight1;S7:[PLC22]DB2,REAL2;���.�������,�������  1
Converter2;Weight2;S7:[PLC22]DB2,REAL6;���.�������,�������  2
Converter2;Weight3;S7:[PLC22]DB2,REAL10;���.�������,�������  3
Converter2;Weight4;S7:[PLC22]DB2,REAL14;���.�������,�������  4
Converter2;Weight5;S7:[PLC22]DB2,REAL18;���.�������,�������  5
Converter2;Weight6;S7:[PLC22]DB2,REAL22;���.�������,�������  6
Converter2;Weight7;S7:[PLC22]DB2,REAL26;���.�������,�������  7
Converter2;Weight8;S7:[PLC22]DB2,REAL30;���.�������,�������  8
Converter2;Weight9;S7:[PLC22]DB2,REAL34;���.�������,�������  9
Converter2;Weight10;S7:[PLC22]DB2,REAL38;���.�������,�������  10
Converter2;Material1;S7:[PLC22]DB1,B272,6;������� ��� ��������� � ��������� 1 
Converter2;Material2;S7:[PLC22]DB1,B278,6;������� ��� ��������� � ��������� 2
Converter2;Material3;S7:[PLC22]DB1,B284,6;������� ��� ��������� � ��������� 3
Converter2;Material4;S7:[PLC22]DB1,B290,6;������� ��� ��������� � ��������� 4
Converter2;Material5;S7:[PLC22]DB1,B296,6;������� ��� ��������� � ��������� 5
Converter2;Material6;S7:[PLC22]DB1,B302,6;������� ��� ��������� � ��������� 6
Converter2;Material7;S7:[PLC22]DB1,B308,6;������� ��� ��������� � ��������� 7
Converter2;Material8;S7:[PLC22]DB1,B314,6;������� ��� ��������� � ��������� 8
Converter2;Material9;S7:[PLC22]DB1,B320,6;������� ��� ��������� � ��������� 9
Converter2;Material10;S7:[PLC22]DB1,B326,6;������� ��� ��������� � ��������� 10
Converter2;Bunker2Material1;S7:[PLC22]DB2,B110,6;�������� ���������� � ������� 1
Converter2;Bunker2Material2;S7:[PLC22]DB2,B116,6;�������� ���������� � ������� 2
Converter2;Bunker2Material3;S7:[PLC22]DB2,B122,6;�������� ���������� � ������� 3
Converter2;Bunker2Material4;S7:[PLC22]DB2,B128,6;�������� ���������� � ������� 4
Converter2;Bunker2Material5;S7:[PLC22]DB2,B134,6;�������� ���������� � ������� 5
Converter2;Bunker2Material6;S7:[PLC22]DB2,B140,6;�������� ���������� � ������� 6
Converter2;Bunker2Material7;S7:[PLC22]DB2,B146,6;�������� ���������� � ������� 7
Converter2;Bunker2Material8;S7:[PLC22]DB2,B152,6;�������� ���������� � ������� 8
Converter2;Bunker2Material9;S7:[PLC22]DB2,B158,6;�������� ���������� � ������� 9
Converter2;Bunker2Material10;S7:[PLC22]DB2,B164,6;�������� ���������� � ������� 10
Converter2;Bunker2Material11;S7:[PLC22]DB2,B170,6;�������� ���������� � ������� 11
Converter2;Bunker2Material12;S7:[PLC22]DB2,B176,6;�������� ���������� � ������� 12
Converter2;Bunker2Material13;S7:[PLC22]DB2,B182,6;�������� ���������� � ������� 13
Converter2;Bunker2Material14;S7:[PLC22]DB2,B188,6;�������� ���������� � ������� 14
Converter2;Bunker2Material15;S7:[PLC22]DB2,B194,6;�������� ���������� � ������� 15
Converter2;Bunker2Material16;S7:[PLC22]DB2,B200,6;�������� ���������� � ������� 16
Converter2;Bunker2Material17;S7:[PLC22]DB2,B206,6;�������� ���������� � ������� 17
;;;
Converter3;Takeover;S7:[PLC32]DB2,INT0;Takeover ������� � ���������
Converter3;Weight1;S7:[PLC32]DB2,REAL2;���.�������,�������  1
Converter3;Weight2;S7:[PLC32]DB2,REAL6;���.�������,�������  2
Converter3;Weight3;S7:[PLC32]DB2,REAL10;���.�������,�������  3
Converter3;Weight4;S7:[PLC32]DB2,REAL14;���.�������,�������  4
Converter3;Weight5;S7:[PLC32]DB2,REAL18;���.�������,�������  5
Converter3;Weight6;S7:[PLC32]DB2,REAL22;���.�������,�������  6
Converter3;Weight7;S7:[PLC32]DB2,REAL26;���.�������,�������  7
Converter3;Weight8;S7:[PLC32]DB2,REAL30;���.�������,�������  8
Converter3;Weight9;S7:[PLC32]DB2,REAL34;���.�������,�������  9
Converter3;Weight10;S7:[PLC32]DB2,REAL38;���.�������,�������  10
Converter3;Material1;S7:[PLC32]DB1,B272,6;������� ��� ��������� � ��������� 1 
Converter3;Material2;S7:[PLC32]DB1,B278,6;������� ��� ��������� � ��������� 2
Converter3;Material3;S7:[PLC32]DB1,B284,6;������� ��� ��������� � ��������� 3
Converter3;Material4;S7:[PLC32]DB1,B290,6;������� ��� ��������� � ��������� 4
Converter3;Material5;S7:[PLC32]DB1,B296,6;������� ��� ��������� � ��������� 5
Converter3;Material6;S7:[PLC32]DB1,B302,6;������� ��� ��������� � ��������� 6
Converter3;Material7;S7:[PLC32]DB1,B308,6;������� ��� ��������� � ��������� 7
Converter3;Material8;S7:[PLC32]DB1,B314,6;������� ��� ��������� � ��������� 8
Converter3;Material9;S7:[PLC32]DB1,B320,6;������� ��� ��������� � ��������� 9
Converter3;Material10;S7:[PLC32]DB1,B326,6;������� ��� ��������� � ��������� 10
Converter3;Bunker2Material1;S7:[PLC32]DB2,B110,6;�������� ���������� � ������� 1
Converter3;Bunker2Material2;S7:[PLC32]DB2,B116,6;�������� ���������� � ������� 2
Converter3;Bunker2Material3;S7:[PLC32]DB2,B122,6;�������� ���������� � ������� 3
Converter3;Bunker2Material4;S7:[PLC32]DB2,B128,6;�������� ���������� � ������� 4
Converter3;Bunker2Material5;S7:[PLC32]DB2,B134,6;�������� ���������� � ������� 5
Converter3;Bunker2Material6;S7:[PLC32]DB2,B140,6;�������� ���������� � ������� 6
Converter3;Bunker2Material7;S7:[PLC32]DB2,B146,6;�������� ���������� � ������� 7
Converter3;Bunker2Material8;S7:[PLC32]DB2,B152,6;�������� ���������� � ������� 8
Converter3;Bunker2Material9;S7:[PLC32]DB2,B158,6;�������� ���������� � ������� 9
Converter3;Bunker2Material10;S7:[PLC32]DB2,B164,6;�������� ���������� � ������� 10
Converter3;Bunker2Material11;S7:[PLC32]DB2,B170,6;�������� ���������� � ������� 11
Converter3;Bunker2Material12;S7:[PLC32]DB2,B176,6;�������� ���������� � ������� 12
Converter3;Bunker2Material13;S7:[PLC32]DB2,B182,6;�������� ���������� � ������� 13
Converter3;Bunker2Material14;S7:[PLC32]DB2,B188,6;�������� ���������� � ������� 14
Converter3;Bunker2Material15;S7:[PLC32]DB2,B194,6;�������� ���������� � ������� 15
Converter3;Bunker2Material16;S7:[PLC32]DB2,B200,6;�������� ���������� � ������� 16
Converter3;Bunker2Material17;S7:[PLC32]DB2,B206,6;�������� ���������� � ������� 17
