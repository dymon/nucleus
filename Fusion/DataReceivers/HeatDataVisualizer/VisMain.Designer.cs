﻿namespace HeatDataVisualizer
{
    partial class VisMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gbW5 = new System.Windows.Forms.GroupBox();
            this.gbRB8 = new System.Windows.Forms.GroupBox();
            this.lblRB8Oxy = new System.Windows.Forms.Label();
            this.lblRB8Mass = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.gbW6 = new System.Windows.Forms.GroupBox();
            this.gbRB9 = new System.Windows.Forms.GroupBox();
            this.lblRB9Oxy = new System.Windows.Forms.Label();
            this.lblRB9Mass = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.gbW7 = new System.Windows.Forms.GroupBox();
            this.gbRB10 = new System.Windows.Forms.GroupBox();
            this.lblRB10Oxy = new System.Windows.Forms.Label();
            this.lblRB10Mass = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.gbRB11 = new System.Windows.Forms.GroupBox();
            this.lblRB11Oxy = new System.Windows.Forms.Label();
            this.lblRB11Mass = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.gbRB12 = new System.Windows.Forms.GroupBox();
            this.lblRB12Oxy = new System.Windows.Forms.Label();
            this.lblRB12Mass = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.gbW4 = new System.Windows.Forms.GroupBox();
            this.gbRB7 = new System.Windows.Forms.GroupBox();
            this.lblRB7Oxy = new System.Windows.Forms.Label();
            this.lblRB7Mass = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.gbW3 = new System.Windows.Forms.GroupBox();
            this.gbRB5 = new System.Windows.Forms.GroupBox();
            this.lblRB5Oxy = new System.Windows.Forms.Label();
            this.lblRB5Mass = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gbRB6 = new System.Windows.Forms.GroupBox();
            this.lblRB6Oxy = new System.Windows.Forms.Label();
            this.lblRB6Mass = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dgPatternLance = new System.Windows.Forms.DataGridView();
            this.dgPatternAdditions = new System.Windows.Forms.DataGridView();
            this.dgHaderLance = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgHaderAdditions = new System.Windows.Forms.DataGridView();
            this.Material = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.W = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.initTimer = new System.Windows.Forms.Timer(this.components);
            this.gbLanceMode = new System.Windows.Forms.GroupBox();
            this.lblLanceMode = new System.Windows.Forms.Label();
            this.gbOxygenMode = new System.Windows.Forms.GroupBox();
            this.lblOxygenMode = new System.Windows.Forms.Label();
            this.gbVerticalPathMode = new System.Windows.Forms.GroupBox();
            this.lblVerticalPathMode = new System.Windows.Forms.Label();
            this.tcMain.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.gbW5.SuspendLayout();
            this.gbRB8.SuspendLayout();
            this.gbW6.SuspendLayout();
            this.gbRB9.SuspendLayout();
            this.gbW7.SuspendLayout();
            this.gbRB10.SuspendLayout();
            this.gbRB11.SuspendLayout();
            this.gbRB12.SuspendLayout();
            this.gbW4.SuspendLayout();
            this.gbRB7.SuspendLayout();
            this.gbW3.SuspendLayout();
            this.gbRB5.SuspendLayout();
            this.gbRB6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPatternLance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPatternAdditions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgHaderLance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgHaderAdditions)).BeginInit();
            this.gbLanceMode.SuspendLayout();
            this.gbOxygenMode.SuspendLayout();
            this.gbVerticalPathMode.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcMain
            // 
            this.tcMain.Controls.Add(this.tabPage1);
            this.tcMain.Controls.Add(this.tabPage2);
            this.tcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMain.Location = new System.Drawing.Point(0, 0);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(1044, 470);
            this.tcMain.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Black;
            this.tabPage1.Controls.Add(this.gbVerticalPathMode);
            this.tabPage1.Controls.Add(this.gbOxygenMode);
            this.tabPage1.Controls.Add(this.gbLanceMode);
            this.tabPage1.Controls.Add(this.gbW5);
            this.tabPage1.Controls.Add(this.gbW6);
            this.tabPage1.Controls.Add(this.gbW7);
            this.tabPage1.Controls.Add(this.gbW4);
            this.tabPage1.Controls.Add(this.gbW3);
            this.tabPage1.Controls.Add(this.dgPatternLance);
            this.tabPage1.Controls.Add(this.dgPatternAdditions);
            this.tabPage1.Controls.Add(this.dgHaderLance);
            this.tabPage1.Controls.Add(this.dgHaderAdditions);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1036, 444);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            // 
            // gbW5
            // 
            this.gbW5.Controls.Add(this.gbRB8);
            this.gbW5.ForeColor = System.Drawing.Color.White;
            this.gbW5.Location = new System.Drawing.Point(396, 295);
            this.gbW5.Name = "gbW5";
            this.gbW5.Size = new System.Drawing.Size(127, 89);
            this.gbW5.TabIndex = 2;
            this.gbW5.TabStop = false;
            this.gbW5.Text = "Weigher5";
            // 
            // gbRB8
            // 
            this.gbRB8.Controls.Add(this.lblRB8Oxy);
            this.gbRB8.Controls.Add(this.lblRB8Mass);
            this.gbRB8.Controls.Add(this.label13);
            this.gbRB8.Controls.Add(this.label14);
            this.gbRB8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gbRB8.Location = new System.Drawing.Point(6, 13);
            this.gbRB8.Name = "gbRB8";
            this.gbRB8.Size = new System.Drawing.Size(115, 66);
            this.gbRB8.TabIndex = 1;
            this.gbRB8.TabStop = false;
            this.gbRB8.Text = "RB8";
            // 
            // lblRB8Oxy
            // 
            this.lblRB8Oxy.AutoSize = true;
            this.lblRB8Oxy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRB8Oxy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblRB8Oxy.Location = new System.Drawing.Point(56, 35);
            this.lblRB8Oxy.Name = "lblRB8Oxy";
            this.lblRB8Oxy.Size = new System.Drawing.Size(49, 20);
            this.lblRB8Oxy.TabIndex = 0;
            this.lblRB8Oxy.Text = "0000";
            // 
            // lblRB8Mass
            // 
            this.lblRB8Mass.AutoSize = true;
            this.lblRB8Mass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRB8Mass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblRB8Mass.Location = new System.Drawing.Point(56, 16);
            this.lblRB8Mass.Name = "lblRB8Mass";
            this.lblRB8Mass.Size = new System.Drawing.Size(49, 20);
            this.lblRB8Mass.TabIndex = 0;
            this.lblRB8Mass.Text = "0000";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label13.Location = new System.Drawing.Point(6, 35);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 20);
            this.label13.TabIndex = 0;
            this.label13.Text = "Oxy =";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label14.Location = new System.Drawing.Point(6, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 20);
            this.label14.TabIndex = 0;
            this.label14.Text = "Вес =";
            // 
            // gbW6
            // 
            this.gbW6.Controls.Add(this.gbRB9);
            this.gbW6.ForeColor = System.Drawing.Color.White;
            this.gbW6.Location = new System.Drawing.Point(529, 295);
            this.gbW6.Name = "gbW6";
            this.gbW6.Size = new System.Drawing.Size(126, 89);
            this.gbW6.TabIndex = 2;
            this.gbW6.TabStop = false;
            this.gbW6.Text = "Weigher6";
            // 
            // gbRB9
            // 
            this.gbRB9.Controls.Add(this.lblRB9Oxy);
            this.gbRB9.Controls.Add(this.lblRB9Mass);
            this.gbRB9.Controls.Add(this.label17);
            this.gbRB9.Controls.Add(this.label18);
            this.gbRB9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gbRB9.Location = new System.Drawing.Point(6, 13);
            this.gbRB9.Name = "gbRB9";
            this.gbRB9.Size = new System.Drawing.Size(115, 65);
            this.gbRB9.TabIndex = 1;
            this.gbRB9.TabStop = false;
            this.gbRB9.Text = "RB9";
            // 
            // lblRB9Oxy
            // 
            this.lblRB9Oxy.AutoSize = true;
            this.lblRB9Oxy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRB9Oxy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblRB9Oxy.Location = new System.Drawing.Point(54, 35);
            this.lblRB9Oxy.Name = "lblRB9Oxy";
            this.lblRB9Oxy.Size = new System.Drawing.Size(49, 20);
            this.lblRB9Oxy.TabIndex = 0;
            this.lblRB9Oxy.Text = "0000";
            // 
            // lblRB9Mass
            // 
            this.lblRB9Mass.AutoSize = true;
            this.lblRB9Mass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRB9Mass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblRB9Mass.Location = new System.Drawing.Point(54, 15);
            this.lblRB9Mass.Name = "lblRB9Mass";
            this.lblRB9Mass.Size = new System.Drawing.Size(49, 20);
            this.lblRB9Mass.TabIndex = 0;
            this.lblRB9Mass.Text = "0000";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label17.Location = new System.Drawing.Point(6, 36);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 20);
            this.label17.TabIndex = 0;
            this.label17.Text = "Oxy =";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label18.Location = new System.Drawing.Point(6, 16);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 20);
            this.label18.TabIndex = 0;
            this.label18.Text = "Вес =";
            // 
            // gbW7
            // 
            this.gbW7.Controls.Add(this.gbRB10);
            this.gbW7.Controls.Add(this.gbRB11);
            this.gbW7.Controls.Add(this.gbRB12);
            this.gbW7.ForeColor = System.Drawing.Color.White;
            this.gbW7.Location = new System.Drawing.Point(661, 295);
            this.gbW7.Name = "gbW7";
            this.gbW7.Size = new System.Drawing.Size(370, 89);
            this.gbW7.TabIndex = 2;
            this.gbW7.TabStop = false;
            this.gbW7.Text = "Weigher7";
            // 
            // gbRB10
            // 
            this.gbRB10.Controls.Add(this.lblRB10Oxy);
            this.gbRB10.Controls.Add(this.lblRB10Mass);
            this.gbRB10.Controls.Add(this.label21);
            this.gbRB10.Controls.Add(this.label22);
            this.gbRB10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gbRB10.Location = new System.Drawing.Point(6, 14);
            this.gbRB10.Name = "gbRB10";
            this.gbRB10.Size = new System.Drawing.Size(115, 65);
            this.gbRB10.TabIndex = 1;
            this.gbRB10.TabStop = false;
            this.gbRB10.Text = "RB10";
            // 
            // lblRB10Oxy
            // 
            this.lblRB10Oxy.AutoSize = true;
            this.lblRB10Oxy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRB10Oxy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblRB10Oxy.Location = new System.Drawing.Point(56, 34);
            this.lblRB10Oxy.Name = "lblRB10Oxy";
            this.lblRB10Oxy.Size = new System.Drawing.Size(49, 20);
            this.lblRB10Oxy.TabIndex = 0;
            this.lblRB10Oxy.Text = "0000";
            // 
            // lblRB10Mass
            // 
            this.lblRB10Mass.AutoSize = true;
            this.lblRB10Mass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRB10Mass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblRB10Mass.Location = new System.Drawing.Point(56, 16);
            this.lblRB10Mass.Name = "lblRB10Mass";
            this.lblRB10Mass.Size = new System.Drawing.Size(49, 20);
            this.lblRB10Mass.TabIndex = 0;
            this.lblRB10Mass.Text = "0000";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label21.Location = new System.Drawing.Point(6, 35);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 20);
            this.label21.TabIndex = 0;
            this.label21.Text = "Oxy =";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label22.Location = new System.Drawing.Point(6, 16);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 20);
            this.label22.TabIndex = 0;
            this.label22.Text = "Вес =";
            // 
            // gbRB11
            // 
            this.gbRB11.Controls.Add(this.lblRB11Oxy);
            this.gbRB11.Controls.Add(this.lblRB11Mass);
            this.gbRB11.Controls.Add(this.label25);
            this.gbRB11.Controls.Add(this.label26);
            this.gbRB11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gbRB11.Location = new System.Drawing.Point(127, 14);
            this.gbRB11.Name = "gbRB11";
            this.gbRB11.Size = new System.Drawing.Size(115, 65);
            this.gbRB11.TabIndex = 1;
            this.gbRB11.TabStop = false;
            this.gbRB11.Text = "RB11";
            // 
            // lblRB11Oxy
            // 
            this.lblRB11Oxy.AutoSize = true;
            this.lblRB11Oxy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRB11Oxy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblRB11Oxy.Location = new System.Drawing.Point(57, 36);
            this.lblRB11Oxy.Name = "lblRB11Oxy";
            this.lblRB11Oxy.Size = new System.Drawing.Size(49, 20);
            this.lblRB11Oxy.TabIndex = 0;
            this.lblRB11Oxy.Text = "0000";
            // 
            // lblRB11Mass
            // 
            this.lblRB11Mass.AutoSize = true;
            this.lblRB11Mass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRB11Mass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblRB11Mass.Location = new System.Drawing.Point(57, 15);
            this.lblRB11Mass.Name = "lblRB11Mass";
            this.lblRB11Mass.Size = new System.Drawing.Size(49, 20);
            this.lblRB11Mass.TabIndex = 0;
            this.lblRB11Mass.Text = "0000";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label25.Location = new System.Drawing.Point(6, 36);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 20);
            this.label25.TabIndex = 0;
            this.label25.Text = "Oxy =";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label26.Location = new System.Drawing.Point(6, 16);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(55, 20);
            this.label26.TabIndex = 0;
            this.label26.Text = "Вес =";
            // 
            // gbRB12
            // 
            this.gbRB12.Controls.Add(this.lblRB12Oxy);
            this.gbRB12.Controls.Add(this.lblRB12Mass);
            this.gbRB12.Controls.Add(this.label29);
            this.gbRB12.Controls.Add(this.label30);
            this.gbRB12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gbRB12.Location = new System.Drawing.Point(248, 14);
            this.gbRB12.Name = "gbRB12";
            this.gbRB12.Size = new System.Drawing.Size(115, 65);
            this.gbRB12.TabIndex = 1;
            this.gbRB12.TabStop = false;
            this.gbRB12.Text = "RB12";
            // 
            // lblRB12Oxy
            // 
            this.lblRB12Oxy.AutoSize = true;
            this.lblRB12Oxy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRB12Oxy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblRB12Oxy.Location = new System.Drawing.Point(55, 36);
            this.lblRB12Oxy.Name = "lblRB12Oxy";
            this.lblRB12Oxy.Size = new System.Drawing.Size(49, 20);
            this.lblRB12Oxy.TabIndex = 0;
            this.lblRB12Oxy.Text = "0000";
            // 
            // lblRB12Mass
            // 
            this.lblRB12Mass.AutoSize = true;
            this.lblRB12Mass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRB12Mass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblRB12Mass.Location = new System.Drawing.Point(55, 16);
            this.lblRB12Mass.Name = "lblRB12Mass";
            this.lblRB12Mass.Size = new System.Drawing.Size(49, 20);
            this.lblRB12Mass.TabIndex = 0;
            this.lblRB12Mass.Text = "0000";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label29.Location = new System.Drawing.Point(6, 35);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(53, 20);
            this.label29.TabIndex = 0;
            this.label29.Text = "Oxy =";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label30.Location = new System.Drawing.Point(6, 16);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(55, 20);
            this.label30.TabIndex = 0;
            this.label30.Text = "Вес =";
            // 
            // gbW4
            // 
            this.gbW4.Controls.Add(this.gbRB7);
            this.gbW4.ForeColor = System.Drawing.Color.White;
            this.gbW4.Location = new System.Drawing.Point(264, 295);
            this.gbW4.Name = "gbW4";
            this.gbW4.Size = new System.Drawing.Size(126, 89);
            this.gbW4.TabIndex = 2;
            this.gbW4.TabStop = false;
            this.gbW4.Text = "Weigher4";
            // 
            // gbRB7
            // 
            this.gbRB7.Controls.Add(this.lblRB7Oxy);
            this.gbRB7.Controls.Add(this.lblRB7Mass);
            this.gbRB7.Controls.Add(this.label9);
            this.gbRB7.Controls.Add(this.label10);
            this.gbRB7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gbRB7.Location = new System.Drawing.Point(6, 12);
            this.gbRB7.Name = "gbRB7";
            this.gbRB7.Size = new System.Drawing.Size(115, 66);
            this.gbRB7.TabIndex = 1;
            this.gbRB7.TabStop = false;
            this.gbRB7.Text = "RB7";
            // 
            // lblRB7Oxy
            // 
            this.lblRB7Oxy.AutoSize = true;
            this.lblRB7Oxy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRB7Oxy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblRB7Oxy.Location = new System.Drawing.Point(56, 36);
            this.lblRB7Oxy.Name = "lblRB7Oxy";
            this.lblRB7Oxy.Size = new System.Drawing.Size(49, 20);
            this.lblRB7Oxy.TabIndex = 0;
            this.lblRB7Oxy.Text = "0000";
            // 
            // lblRB7Mass
            // 
            this.lblRB7Mass.AutoSize = true;
            this.lblRB7Mass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRB7Mass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblRB7Mass.Location = new System.Drawing.Point(56, 18);
            this.lblRB7Mass.Name = "lblRB7Mass";
            this.lblRB7Mass.Size = new System.Drawing.Size(49, 20);
            this.lblRB7Mass.TabIndex = 0;
            this.lblRB7Mass.Text = "0000";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label9.Location = new System.Drawing.Point(6, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 20);
            this.label9.TabIndex = 0;
            this.label9.Text = "Oxy =";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label10.Location = new System.Drawing.Point(6, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 20);
            this.label10.TabIndex = 0;
            this.label10.Text = "Вес =";
            // 
            // gbW3
            // 
            this.gbW3.Controls.Add(this.gbRB5);
            this.gbW3.Controls.Add(this.gbRB6);
            this.gbW3.ForeColor = System.Drawing.Color.White;
            this.gbW3.Location = new System.Drawing.Point(9, 295);
            this.gbW3.Name = "gbW3";
            this.gbW3.Size = new System.Drawing.Size(249, 89);
            this.gbW3.TabIndex = 2;
            this.gbW3.TabStop = false;
            this.gbW3.Text = "Weigher3";
            // 
            // gbRB5
            // 
            this.gbRB5.Controls.Add(this.lblRB5Oxy);
            this.gbRB5.Controls.Add(this.lblRB5Mass);
            this.gbRB5.Controls.Add(this.label2);
            this.gbRB5.Controls.Add(this.label1);
            this.gbRB5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gbRB5.Location = new System.Drawing.Point(6, 12);
            this.gbRB5.Name = "gbRB5";
            this.gbRB5.Size = new System.Drawing.Size(115, 66);
            this.gbRB5.TabIndex = 1;
            this.gbRB5.TabStop = false;
            this.gbRB5.Text = "RB5";
            // 
            // lblRB5Oxy
            // 
            this.lblRB5Oxy.AutoSize = true;
            this.lblRB5Oxy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRB5Oxy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblRB5Oxy.Location = new System.Drawing.Point(58, 36);
            this.lblRB5Oxy.Name = "lblRB5Oxy";
            this.lblRB5Oxy.Size = new System.Drawing.Size(49, 20);
            this.lblRB5Oxy.TabIndex = 0;
            this.lblRB5Oxy.Text = "0000";
            // 
            // lblRB5Mass
            // 
            this.lblRB5Mass.AutoSize = true;
            this.lblRB5Mass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRB5Mass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblRB5Mass.Location = new System.Drawing.Point(58, 16);
            this.lblRB5Mass.Name = "lblRB5Mass";
            this.lblRB5Mass.Size = new System.Drawing.Size(49, 20);
            this.lblRB5Mass.TabIndex = 0;
            this.lblRB5Mass.Text = "0000";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label2.Location = new System.Drawing.Point(6, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Oxy =";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Вес =";
            // 
            // gbRB6
            // 
            this.gbRB6.Controls.Add(this.lblRB6Oxy);
            this.gbRB6.Controls.Add(this.lblRB6Mass);
            this.gbRB6.Controls.Add(this.label5);
            this.gbRB6.Controls.Add(this.label6);
            this.gbRB6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gbRB6.Location = new System.Drawing.Point(127, 12);
            this.gbRB6.Name = "gbRB6";
            this.gbRB6.Size = new System.Drawing.Size(115, 66);
            this.gbRB6.TabIndex = 1;
            this.gbRB6.TabStop = false;
            this.gbRB6.Text = "RB6";
            // 
            // lblRB6Oxy
            // 
            this.lblRB6Oxy.AutoSize = true;
            this.lblRB6Oxy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRB6Oxy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblRB6Oxy.Location = new System.Drawing.Point(57, 36);
            this.lblRB6Oxy.Name = "lblRB6Oxy";
            this.lblRB6Oxy.Size = new System.Drawing.Size(49, 20);
            this.lblRB6Oxy.TabIndex = 0;
            this.lblRB6Oxy.Text = "0000";
            // 
            // lblRB6Mass
            // 
            this.lblRB6Mass.AutoSize = true;
            this.lblRB6Mass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRB6Mass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblRB6Mass.Location = new System.Drawing.Point(57, 16);
            this.lblRB6Mass.Name = "lblRB6Mass";
            this.lblRB6Mass.Size = new System.Drawing.Size(49, 20);
            this.lblRB6Mass.TabIndex = 0;
            this.lblRB6Mass.Text = "0000";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label5.Location = new System.Drawing.Point(6, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Oxy =";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "Вес =";
            // 
            // dgPatternLance
            // 
            this.dgPatternLance.AllowUserToAddRows = false;
            this.dgPatternLance.AllowUserToDeleteRows = false;
            this.dgPatternLance.AllowUserToResizeColumns = false;
            this.dgPatternLance.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.dgPatternLance.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgPatternLance.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dgPatternLance.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPatternLance.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgPatternLance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPatternLance.ColumnHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgPatternLance.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgPatternLance.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.dgPatternLance.Location = new System.Drawing.Point(143, 6);
            this.dgPatternLance.Name = "dgPatternLance";
            this.dgPatternLance.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPatternLance.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgPatternLance.RowHeadersVisible = false;
            this.dgPatternLance.RowHeadersWidth = 10;
            this.dgPatternLance.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.dgPatternLance.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgPatternLance.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgPatternLance.Size = new System.Drawing.Size(888, 73);
            this.dgPatternLance.TabIndex = 0;
            // 
            // dgPatternAdditions
            // 
            this.dgPatternAdditions.AllowUserToAddRows = false;
            this.dgPatternAdditions.AllowUserToDeleteRows = false;
            this.dgPatternAdditions.AllowUserToResizeColumns = false;
            this.dgPatternAdditions.AllowUserToResizeRows = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.dgPatternAdditions.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgPatternAdditions.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dgPatternAdditions.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPatternAdditions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgPatternAdditions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPatternAdditions.ColumnHeadersVisible = false;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgPatternAdditions.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgPatternAdditions.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.dgPatternAdditions.Location = new System.Drawing.Point(143, 85);
            this.dgPatternAdditions.Name = "dgPatternAdditions";
            this.dgPatternAdditions.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPatternAdditions.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgPatternAdditions.RowHeadersVisible = false;
            this.dgPatternAdditions.RowHeadersWidth = 10;
            this.dgPatternAdditions.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.dgPatternAdditions.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgPatternAdditions.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dgPatternAdditions.Size = new System.Drawing.Size(888, 203);
            this.dgPatternAdditions.TabIndex = 0;
            this.dgPatternAdditions.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgPatternAdditions_Scroll);
            // 
            // dgHaderLance
            // 
            this.dgHaderLance.AllowUserToAddRows = false;
            this.dgHaderLance.AllowUserToDeleteRows = false;
            this.dgHaderLance.AllowUserToResizeColumns = false;
            this.dgHaderLance.AllowUserToResizeRows = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.dgHaderLance.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dgHaderLance.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dgHaderLance.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgHaderLance.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgHaderLance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgHaderLance.ColumnHeadersVisible = false;
            this.dgHaderLance.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn3});
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgHaderLance.DefaultCellStyle = dataGridViewCellStyle13;
            this.dgHaderLance.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.dgHaderLance.Location = new System.Drawing.Point(8, 6);
            this.dgHaderLance.Name = "dgHaderLance";
            this.dgHaderLance.ReadOnly = true;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgHaderLance.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgHaderLance.RowHeadersVisible = false;
            this.dgHaderLance.RowHeadersWidth = 10;
            this.dgHaderLance.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.dgHaderLance.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dgHaderLance.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgHaderLance.Size = new System.Drawing.Size(129, 73);
            this.dgHaderLance.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "about";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.Width = 75;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "ed";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn3.Width = 50;
            // 
            // dgHaderAdditions
            // 
            this.dgHaderAdditions.AllowUserToAddRows = false;
            this.dgHaderAdditions.AllowUserToDeleteRows = false;
            this.dgHaderAdditions.AllowUserToResizeColumns = false;
            this.dgHaderAdditions.AllowUserToResizeRows = false;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.dgHaderAdditions.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle16;
            this.dgHaderAdditions.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dgHaderAdditions.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgHaderAdditions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dgHaderAdditions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgHaderAdditions.ColumnHeadersVisible = false;
            this.dgHaderAdditions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Material,
            this.rb,
            this.W});
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgHaderAdditions.DefaultCellStyle = dataGridViewCellStyle18;
            this.dgHaderAdditions.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.dgHaderAdditions.Location = new System.Drawing.Point(8, 85);
            this.dgHaderAdditions.Name = "dgHaderAdditions";
            this.dgHaderAdditions.ReadOnly = true;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgHaderAdditions.RowHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dgHaderAdditions.RowHeadersVisible = false;
            this.dgHaderAdditions.RowHeadersWidth = 10;
            this.dgHaderAdditions.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.dgHaderAdditions.RowsDefaultCellStyle = dataGridViewCellStyle20;
            this.dgHaderAdditions.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgHaderAdditions.Size = new System.Drawing.Size(129, 203);
            this.dgHaderAdditions.TabIndex = 0;
            // 
            // Material
            // 
            this.Material.HeaderText = "Material";
            this.Material.Name = "Material";
            this.Material.ReadOnly = true;
            this.Material.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Material.Width = 60;
            // 
            // rb
            // 
            this.rb.HeaderText = "RB";
            this.rb.Name = "rb";
            this.rb.ReadOnly = true;
            this.rb.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.rb.Width = 35;
            // 
            // W
            // 
            this.W.HeaderText = "W";
            this.W.Name = "W";
            this.W.ReadOnly = true;
            this.W.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.W.Width = 30;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1036, 394);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // initTimer
            // 
            this.initTimer.Interval = 500;
            this.initTimer.Tick += new System.EventHandler(this.initTimer_Tick);
            // 
            // gbLanceMode
            // 
            this.gbLanceMode.Controls.Add(this.lblLanceMode);
            this.gbLanceMode.ForeColor = System.Drawing.Color.White;
            this.gbLanceMode.Location = new System.Drawing.Point(15, 390);
            this.gbLanceMode.Name = "gbLanceMode";
            this.gbLanceMode.Size = new System.Drawing.Size(107, 45);
            this.gbLanceMode.TabIndex = 3;
            this.gbLanceMode.TabStop = false;
            this.gbLanceMode.Text = "Режим фурмы";
            // 
            // lblLanceMode
            // 
            this.lblLanceMode.AutoSize = true;
            this.lblLanceMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLanceMode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblLanceMode.Location = new System.Drawing.Point(32, 16);
            this.lblLanceMode.Name = "lblLanceMode";
            this.lblLanceMode.Size = new System.Drawing.Size(39, 20);
            this.lblLanceMode.TabIndex = 0;
            this.lblLanceMode.Text = "###";
            // 
            // gbOxygenMode
            // 
            this.gbOxygenMode.Controls.Add(this.lblOxygenMode);
            this.gbOxygenMode.ForeColor = System.Drawing.Color.White;
            this.gbOxygenMode.Location = new System.Drawing.Point(136, 390);
            this.gbOxygenMode.Name = "gbOxygenMode";
            this.gbOxygenMode.Size = new System.Drawing.Size(107, 45);
            this.gbOxygenMode.TabIndex = 3;
            this.gbOxygenMode.TabStop = false;
            this.gbOxygenMode.Text = "Режим O2";
            // 
            // lblOxygenMode
            // 
            this.lblOxygenMode.AutoSize = true;
            this.lblOxygenMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblOxygenMode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblOxygenMode.Location = new System.Drawing.Point(32, 16);
            this.lblOxygenMode.Name = "lblOxygenMode";
            this.lblOxygenMode.Size = new System.Drawing.Size(39, 20);
            this.lblOxygenMode.TabIndex = 0;
            this.lblOxygenMode.Text = "###";
            // 
            // gbVerticalPathMode
            // 
            this.gbVerticalPathMode.Controls.Add(this.lblVerticalPathMode);
            this.gbVerticalPathMode.ForeColor = System.Drawing.Color.White;
            this.gbVerticalPathMode.Location = new System.Drawing.Point(270, 390);
            this.gbVerticalPathMode.Name = "gbVerticalPathMode";
            this.gbVerticalPathMode.Size = new System.Drawing.Size(107, 45);
            this.gbVerticalPathMode.TabIndex = 3;
            this.gbVerticalPathMode.TabStop = false;
            this.gbVerticalPathMode.Text = "Режим ВТ";
            // 
            // lblVerticalPathMode
            // 
            this.lblVerticalPathMode.AutoSize = true;
            this.lblVerticalPathMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVerticalPathMode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblVerticalPathMode.Location = new System.Drawing.Point(32, 16);
            this.lblVerticalPathMode.Name = "lblVerticalPathMode";
            this.lblVerticalPathMode.Size = new System.Drawing.Size(39, 20);
            this.lblVerticalPathMode.TabIndex = 0;
            this.lblVerticalPathMode.Text = "###";
            // 
            // VisMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1044, 470);
            this.Controls.Add(this.tcMain);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "VisMain";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Визуализация данных";
            this.tcMain.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.gbW5.ResumeLayout(false);
            this.gbRB8.ResumeLayout(false);
            this.gbRB8.PerformLayout();
            this.gbW6.ResumeLayout(false);
            this.gbRB9.ResumeLayout(false);
            this.gbRB9.PerformLayout();
            this.gbW7.ResumeLayout(false);
            this.gbRB10.ResumeLayout(false);
            this.gbRB10.PerformLayout();
            this.gbRB11.ResumeLayout(false);
            this.gbRB11.PerformLayout();
            this.gbRB12.ResumeLayout(false);
            this.gbRB12.PerformLayout();
            this.gbW4.ResumeLayout(false);
            this.gbRB7.ResumeLayout(false);
            this.gbRB7.PerformLayout();
            this.gbW3.ResumeLayout(false);
            this.gbRB5.ResumeLayout(false);
            this.gbRB5.PerformLayout();
            this.gbRB6.ResumeLayout(false);
            this.gbRB6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPatternLance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPatternAdditions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgHaderLance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgHaderAdditions)).EndInit();
            this.gbLanceMode.ResumeLayout(false);
            this.gbLanceMode.PerformLayout();
            this.gbOxygenMode.ResumeLayout(false);
            this.gbOxygenMode.PerformLayout();
            this.gbVerticalPathMode.ResumeLayout(false);
            this.gbVerticalPathMode.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgHaderAdditions;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dgPatternAdditions;
        private System.Windows.Forms.Timer initTimer;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material;
        private System.Windows.Forms.DataGridViewTextBoxColumn rb;
        private System.Windows.Forms.DataGridViewTextBoxColumn W;
        private System.Windows.Forms.DataGridView dgPatternLance;
        private System.Windows.Forms.DataGridView dgHaderLance;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.GroupBox gbRB5;
        private System.Windows.Forms.Label lblRB5Mass;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRB5Oxy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gbRB12;
        private System.Windows.Forms.Label lblRB12Oxy;
        private System.Windows.Forms.Label lblRB12Mass;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.GroupBox gbRB11;
        private System.Windows.Forms.Label lblRB11Oxy;
        private System.Windows.Forms.Label lblRB11Mass;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.GroupBox gbRB10;
        private System.Windows.Forms.Label lblRB10Oxy;
        private System.Windows.Forms.Label lblRB10Mass;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.GroupBox gbRB9;
        private System.Windows.Forms.Label lblRB9Oxy;
        private System.Windows.Forms.Label lblRB9Mass;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox gbRB8;
        private System.Windows.Forms.Label lblRB8Oxy;
        private System.Windows.Forms.Label lblRB8Mass;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox gbRB7;
        private System.Windows.Forms.Label lblRB7Oxy;
        private System.Windows.Forms.Label lblRB7Mass;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox gbRB6;
        private System.Windows.Forms.Label lblRB6Oxy;
        private System.Windows.Forms.Label lblRB6Mass;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox gbW5;
        private System.Windows.Forms.GroupBox gbW6;
        private System.Windows.Forms.GroupBox gbW7;
        private System.Windows.Forms.GroupBox gbW4;
        private System.Windows.Forms.GroupBox gbW3;
        private System.Windows.Forms.GroupBox gbLanceMode;
        private System.Windows.Forms.GroupBox gbVerticalPathMode;
        private System.Windows.Forms.Label lblVerticalPathMode;
        private System.Windows.Forms.GroupBox gbOxygenMode;
        private System.Windows.Forms.Label lblOxygenMode;
        private System.Windows.Forms.Label lblLanceMode;
    }
}

