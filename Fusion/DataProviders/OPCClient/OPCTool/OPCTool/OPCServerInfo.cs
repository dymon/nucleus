﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OPCTool
{
    class OPCServerInfo
    {
        public string ServerName { get; set; }
        public string PointAdress { get; set; }
        public string ServerVersion { get; set; }
    }
}
