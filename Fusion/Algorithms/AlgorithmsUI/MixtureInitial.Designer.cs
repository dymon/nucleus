﻿namespace AlgorithmsUI
{
    partial class MixtureInitial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        public System.Windows.Forms.TextBox txbHeatNum;
        public System.Windows.Forms.TextBox txbSteelTemp;
        public System.Windows.Forms.TextBox txbCarbonPercent;
        public System.Windows.Forms.TextBox txbMgO;
        public System.Windows.Forms.TextBox txbFeO;
        public System.Windows.Forms.TextBox txbBasiticy;
        public System.Windows.Forms.TextBox txbIronTask;
        public System.Windows.Forms.TextBox txbIronTemp;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MixtureInitial));
            this.basement = new System.Windows.Forms.Panel();
            this.splitIronScrap = new System.Windows.Forms.SplitContainer();
            this.rtbReport = new System.Windows.Forms.RichTextBox();
            this.panSlagInput = new System.Windows.Forms.TableLayoutPanel();
            this.txbIronTask = new System.Windows.Forms.TextBox();
            this.panScrapTemp = new System.Windows.Forms.Panel();
            this.lblScrapTemp = new System.Windows.Forms.Label();
            this.txbScrapTemp = new System.Windows.Forms.TextBox();
            this.lblDolom = new System.Windows.Forms.Label();
            this.lblVapno = new System.Windows.Forms.Label();
            this.btnIronChem = new System.Windows.Forms.Button();
            this.btnScrapChem = new System.Windows.Forms.Button();
            this.txbLimeStoneIn = new System.Windows.Forms.TextBox();
            this.lblScrap = new System.Windows.Forms.Label();
            this.lblIron = new System.Windows.Forms.Label();
            this.lblLimeStoneIn = new System.Windows.Forms.Label();
            this.lblFomIn = new System.Windows.Forms.Label();
            this.txbFomIn = new System.Windows.Forms.TextBox();
            this.btnFomChem = new System.Windows.Forms.Button();
            this.btnCaCO3Chem = new System.Windows.Forms.Button();
            this.txbLimeIn = new System.Windows.Forms.TextBox();
            this.txbDolomIn = new System.Windows.Forms.TextBox();
            this.panIronTemp = new System.Windows.Forms.Panel();
            this.lblIronTemp = new System.Windows.Forms.Label();
            this.txbIronTemp = new System.Windows.Forms.TextBox();
            this.calcDolmax = new System.Windows.Forms.CheckBox();
            this.calcFom = new System.Windows.Forms.CheckBox();
            this.calcVapenec = new System.Windows.Forms.CheckBox();
            this.calcVapno = new System.Windows.Forms.CheckBox();
            this.btnDolomiteChem = new System.Windows.Forms.Button();
            this.txbCokeIn = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnVapno = new System.Windows.Forms.Button();
            this.btnIronSel = new System.Windows.Forms.Button();
            this.btnScrapSel = new System.Windows.Forms.Button();
            this.panControl = new System.Windows.Forms.Panel();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.panAuxOut = new System.Windows.Forms.TableLayoutPanel();
            this.txtSn = new System.Windows.Forms.TextBox();
            this.txtW = new System.Windows.Forms.TextBox();
            this.txtSb = new System.Windows.Forms.TextBox();
            this.txtNi = new System.Windows.Forms.TextBox();
            this.txtAs = new System.Windows.Forms.TextBox();
            this.txtMo = new System.Windows.Forms.TextBox();
            this.txtCo = new System.Windows.Forms.TextBox();
            this.lblSb = new System.Windows.Forms.Label();
            this.txtCu = new System.Windows.Forms.TextBox();
            this.lblCu = new System.Windows.Forms.Label();
            this.lblMo = new System.Windows.Forms.Label();
            this.lblNi = new System.Windows.Forms.Label();
            this.lblW = new System.Windows.Forms.Label();
            this.lblCo = new System.Windows.Forms.Label();
            this.lblAs = new System.Windows.Forms.Label();
            this.lblSn = new System.Windows.Forms.Label();
            this.panCommonOut = new System.Windows.Forms.TableLayoutPanel();
            this.panSteelOut = new System.Windows.Forms.Panel();
            this.txbSteelOut = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txbLimeStoneOut = new System.Windows.Forms.TextBox();
            this.btnScrapOut = new System.Windows.Forms.Button();
            this.txbFomOut = new System.Windows.Forms.TextBox();
            this.btnIronOut = new System.Windows.Forms.Button();
            this.panIronMass = new System.Windows.Forms.Panel();
            this.txbIronOut = new System.Windows.Forms.TextBox();
            this.lblIronMass = new System.Windows.Forms.Label();
            this.lblDolomite = new System.Windows.Forms.Label();
            this.lblCokeOut = new System.Windows.Forms.Label();
            this.txbDolomitOut = new System.Windows.Forms.TextBox();
            this.lblLimeStoneOut = new System.Windows.Forms.Label();
            this.txbLimeOut = new System.Windows.Forms.TextBox();
            this.lblIronOut = new System.Windows.Forms.Label();
            this.lblLime = new System.Windows.Forms.Label();
            this.lblFomOut = new System.Windows.Forms.Label();
            this.lblScrapOut = new System.Windows.Forms.Label();
            this.panScrapMass = new System.Windows.Forms.Panel();
            this.txbScrapOut = new System.Windows.Forms.TextBox();
            this.lblScrapMass = new System.Windows.Forms.Label();
            this.lblSteelOut = new System.Windows.Forms.Label();
            this.txbCokeOut = new System.Windows.Forms.TextBox();
            this.heading = new System.Windows.Forms.FlowLayoutPanel();
            this.panTitleHeading = new System.Windows.Forms.Panel();
            this.lblTitleHeading = new System.Windows.Forms.Label();
            this.panHeatNum = new System.Windows.Forms.Panel();
            this.txbHeatNum = new System.Windows.Forms.TextBox();
            this.lblHeatNum = new System.Windows.Forms.Label();
            this.panSteelBrand = new System.Windows.Forms.Panel();
            this.cmbSteelBrand = new System.Windows.Forms.ComboBox();
            this.lblSteelBrand = new System.Windows.Forms.Label();
            this.panSteelTemp = new System.Windows.Forms.Panel();
            this.txbSteelTemp = new System.Windows.Forms.TextBox();
            this.lblSteelTemp = new System.Windows.Forms.Label();
            this.panCarbonPercent = new System.Windows.Forms.Panel();
            this.lblCarbonPercent = new System.Windows.Forms.Label();
            this.txbCarbonPercent = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblTgtMgO = new System.Windows.Forms.Label();
            this.txbMgO = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblTgtFeO = new System.Windows.Forms.Label();
            this.txbFeO = new System.Windows.Forms.TextBox();
            this.panAligner = new System.Windows.Forms.Panel();
            this.panBasiticy = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.txbBasiticy = new System.Windows.Forms.TextBox();
            this.basement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitIronScrap)).BeginInit();
            this.splitIronScrap.Panel1.SuspendLayout();
            this.splitIronScrap.Panel2.SuspendLayout();
            this.splitIronScrap.SuspendLayout();
            this.panSlagInput.SuspendLayout();
            this.panScrapTemp.SuspendLayout();
            this.panIronTemp.SuspendLayout();
            this.panControl.SuspendLayout();
            this.panAuxOut.SuspendLayout();
            this.panCommonOut.SuspendLayout();
            this.panSteelOut.SuspendLayout();
            this.panIronMass.SuspendLayout();
            this.panScrapMass.SuspendLayout();
            this.heading.SuspendLayout();
            this.panTitleHeading.SuspendLayout();
            this.panHeatNum.SuspendLayout();
            this.panSteelBrand.SuspendLayout();
            this.panSteelTemp.SuspendLayout();
            this.panCarbonPercent.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panBasiticy.SuspendLayout();
            this.SuspendLayout();
            // 
            // basement
            // 
            this.basement.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.basement.Controls.Add(this.splitIronScrap);
            this.basement.Controls.Add(this.heading);
            this.basement.Location = new System.Drawing.Point(0, 0);
            this.basement.Name = "basement";
            this.basement.Size = new System.Drawing.Size(1123, 695);
            this.basement.TabIndex = 0;
            // 
            // splitIronScrap
            // 
            this.splitIronScrap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitIronScrap.Location = new System.Drawing.Point(0, 110);
            this.splitIronScrap.Name = "splitIronScrap";
            // 
            // splitIronScrap.Panel1
            // 
            this.splitIronScrap.Panel1.Controls.Add(this.rtbReport);
            this.splitIronScrap.Panel1.Controls.Add(this.panSlagInput);
            // 
            // splitIronScrap.Panel2
            // 
            this.splitIronScrap.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.splitIronScrap.Panel2.Controls.Add(this.panControl);
            this.splitIronScrap.Panel2.Controls.Add(this.panAuxOut);
            this.splitIronScrap.Panel2.Controls.Add(this.panCommonOut);
            this.splitIronScrap.Size = new System.Drawing.Size(1123, 585);
            this.splitIronScrap.SplitterDistance = 758;
            this.splitIronScrap.TabIndex = 2;
            // 
            // rtbReport
            // 
            this.rtbReport.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rtbReport.BackColor = System.Drawing.Color.DimGray;
            this.rtbReport.Font = new System.Drawing.Font("Lucida Console", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbReport.ForeColor = System.Drawing.Color.GreenYellow;
            this.rtbReport.Location = new System.Drawing.Point(2, 324);
            this.rtbReport.Name = "rtbReport";
            this.rtbReport.Size = new System.Drawing.Size(754, 261);
            this.rtbReport.TabIndex = 2;
            this.rtbReport.Text = "";
            // 
            // panSlagInput
            // 
            this.panSlagInput.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panSlagInput.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.panSlagInput.ColumnCount = 5;
            this.panSlagInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.panSlagInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.34908F));
            this.panSlagInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.02315F));
            this.panSlagInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.81389F));
            this.panSlagInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.81389F));
            this.panSlagInput.Controls.Add(this.txbIronTask, 1, 0);
            this.panSlagInput.Controls.Add(this.panScrapTemp, 2, 1);
            this.panSlagInput.Controls.Add(this.lblDolom, 0, 4);
            this.panSlagInput.Controls.Add(this.lblVapno, 0, 3);
            this.panSlagInput.Controls.Add(this.btnIronChem, 3, 0);
            this.panSlagInput.Controls.Add(this.btnScrapChem, 3, 1);
            this.panSlagInput.Controls.Add(this.txbLimeStoneIn, 1, 6);
            this.panSlagInput.Controls.Add(this.lblScrap, 0, 1);
            this.panSlagInput.Controls.Add(this.lblIron, 0, 0);
            this.panSlagInput.Controls.Add(this.lblLimeStoneIn, 0, 6);
            this.panSlagInput.Controls.Add(this.lblFomIn, 0, 5);
            this.panSlagInput.Controls.Add(this.txbFomIn, 1, 5);
            this.panSlagInput.Controls.Add(this.btnFomChem, 3, 5);
            this.panSlagInput.Controls.Add(this.btnCaCO3Chem, 3, 6);
            this.panSlagInput.Controls.Add(this.txbLimeIn, 1, 3);
            this.panSlagInput.Controls.Add(this.txbDolomIn, 1, 4);
            this.panSlagInput.Controls.Add(this.panIronTemp, 2, 0);
            this.panSlagInput.Controls.Add(this.calcDolmax, 2, 4);
            this.panSlagInput.Controls.Add(this.calcFom, 2, 5);
            this.panSlagInput.Controls.Add(this.calcVapenec, 2, 6);
            this.panSlagInput.Controls.Add(this.calcVapno, 2, 3);
            this.panSlagInput.Controls.Add(this.btnDolomiteChem, 3, 4);
            this.panSlagInput.Controls.Add(this.txbCokeIn, 1, 7);
            this.panSlagInput.Controls.Add(this.label3, 0, 7);
            this.panSlagInput.Controls.Add(this.btnVapno, 3, 3);
            this.panSlagInput.Controls.Add(this.btnIronSel, 4, 0);
            this.panSlagInput.Controls.Add(this.btnScrapSel, 4, 1);
            this.panSlagInput.Location = new System.Drawing.Point(0, 0);
            this.panSlagInput.Name = "panSlagInput";
            this.panSlagInput.RowCount = 8;
            this.panSlagInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panSlagInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panSlagInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panSlagInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panSlagInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panSlagInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panSlagInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panSlagInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panSlagInput.Size = new System.Drawing.Size(758, 323);
            this.panSlagInput.TabIndex = 1;
            // 
            // txbIronTask
            // 
            this.txbIronTask.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txbIronTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbIronTask.Location = new System.Drawing.Point(204, 10);
            this.txbIronTask.Name = "txbIronTask";
            this.txbIronTask.Size = new System.Drawing.Size(95, 21);
            this.txbIronTask.TabIndex = 49;
            // 
            // panScrapTemp
            // 
            this.panScrapTemp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panScrapTemp.Controls.Add(this.lblScrapTemp);
            this.panScrapTemp.Controls.Add(this.txbScrapTemp);
            this.panScrapTemp.Location = new System.Drawing.Point(372, 45);
            this.panScrapTemp.Name = "panScrapTemp";
            this.panScrapTemp.Size = new System.Drawing.Size(112, 34);
            this.panScrapTemp.TabIndex = 28;
            // 
            // lblScrapTemp
            // 
            this.lblScrapTemp.AutoSize = true;
            this.lblScrapTemp.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblScrapTemp.Location = new System.Drawing.Point(0, 0);
            this.lblScrapTemp.Name = "lblScrapTemp";
            this.lblScrapTemp.Size = new System.Drawing.Size(91, 13);
            this.lblScrapTemp.TabIndex = 22;
            this.lblScrapTemp.Text = "Температура, °C";
            // 
            // txbScrapTemp
            // 
            this.txbScrapTemp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txbScrapTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbScrapTemp.Location = new System.Drawing.Point(0, 13);
            this.txbScrapTemp.Name = "txbScrapTemp";
            this.txbScrapTemp.Size = new System.Drawing.Size(112, 21);
            this.txbScrapTemp.TabIndex = 22;
            // 
            // lblDolom
            // 
            this.lblDolom.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDolom.AutoSize = true;
            this.lblDolom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDolom.Location = new System.Drawing.Point(4, 175);
            this.lblDolom.Name = "lblDolom";
            this.lblDolom.Size = new System.Drawing.Size(153, 20);
            this.lblDolom.TabIndex = 44;
            this.lblDolom.Text = "Доломит суш., кг";
            // 
            // lblVapno
            // 
            this.lblVapno.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblVapno.AutoSize = true;
            this.lblVapno.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVapno.Location = new System.Drawing.Point(4, 134);
            this.lblVapno.Name = "lblVapno";
            this.lblVapno.Size = new System.Drawing.Size(106, 20);
            this.lblVapno.TabIndex = 44;
            this.lblVapno.Text = "Известь, кг";
            // 
            // btnIronChem
            // 
            this.btnIronChem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnIronChem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnIronChem.Location = new System.Drawing.Point(528, 4);
            this.btnIronChem.Name = "btnIronChem";
            this.btnIronChem.Size = new System.Drawing.Size(109, 34);
            this.btnIronChem.TabIndex = 30;
            this.btnIronChem.TabStop = false;
            this.btnIronChem.Text = "Хим.состав\r\n";
            this.btnIronChem.UseVisualStyleBackColor = true;
            this.btnIronChem.Click += new System.EventHandler(this.btnIronChem_Click);
            // 
            // btnScrapChem
            // 
            this.btnScrapChem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnScrapChem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnScrapChem.Location = new System.Drawing.Point(528, 45);
            this.btnScrapChem.Name = "btnScrapChem";
            this.btnScrapChem.Size = new System.Drawing.Size(109, 34);
            this.btnScrapChem.TabIndex = 28;
            this.btnScrapChem.TabStop = false;
            this.btnScrapChem.Text = "Хим.состав\r\n";
            this.btnScrapChem.UseVisualStyleBackColor = true;
            this.btnScrapChem.Click += new System.EventHandler(this.btnIronControl_Click);
            // 
            // txbLimeStoneIn
            // 
            this.txbLimeStoneIn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txbLimeStoneIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbLimeStoneIn.Location = new System.Drawing.Point(204, 256);
            this.txbLimeStoneIn.Name = "txbLimeStoneIn";
            this.txbLimeStoneIn.Size = new System.Drawing.Size(95, 21);
            this.txbLimeStoneIn.TabIndex = 27;
            // 
            // lblScrap
            // 
            this.lblScrap.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblScrap.AutoSize = true;
            this.lblScrap.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblScrap.Location = new System.Drawing.Point(4, 52);
            this.lblScrap.Name = "lblScrap";
            this.lblScrap.Size = new System.Drawing.Size(44, 20);
            this.lblScrap.TabIndex = 0;
            this.lblScrap.Text = "Лом";
            // 
            // lblIron
            // 
            this.lblIron.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblIron.AutoSize = true;
            this.lblIron.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblIron.Location = new System.Drawing.Point(4, 11);
            this.lblIron.Name = "lblIron";
            this.lblIron.Size = new System.Drawing.Size(56, 20);
            this.lblIron.TabIndex = 0;
            this.lblIron.Text = "Чугун";
            // 
            // lblLimeStoneIn
            // 
            this.lblLimeStoneIn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblLimeStoneIn.AutoSize = true;
            this.lblLimeStoneIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLimeStoneIn.Location = new System.Drawing.Point(4, 257);
            this.lblLimeStoneIn.Name = "lblLimeStoneIn";
            this.lblLimeStoneIn.Size = new System.Drawing.Size(125, 20);
            this.lblLimeStoneIn.TabIndex = 31;
            this.lblLimeStoneIn.Text = "Шлакообр., кг";
            // 
            // lblFomIn
            // 
            this.lblFomIn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblFomIn.AutoSize = true;
            this.lblFomIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFomIn.Location = new System.Drawing.Point(4, 216);
            this.lblFomIn.Name = "lblFomIn";
            this.lblFomIn.Size = new System.Drawing.Size(79, 20);
            this.lblFomIn.TabIndex = 5;
            this.lblFomIn.Text = "ФОМ, кг";
            // 
            // txbFomIn
            // 
            this.txbFomIn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txbFomIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbFomIn.Location = new System.Drawing.Point(204, 215);
            this.txbFomIn.Name = "txbFomIn";
            this.txbFomIn.Size = new System.Drawing.Size(95, 21);
            this.txbFomIn.TabIndex = 26;
            // 
            // btnFomChem
            // 
            this.btnFomChem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFomChem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnFomChem.Location = new System.Drawing.Point(528, 209);
            this.btnFomChem.Name = "btnFomChem";
            this.btnFomChem.Size = new System.Drawing.Size(109, 34);
            this.btnFomChem.TabIndex = 45;
            this.btnFomChem.TabStop = false;
            this.btnFomChem.Text = "Хим.состав\r\n";
            this.btnFomChem.UseVisualStyleBackColor = true;
            this.btnFomChem.Click += new System.EventHandler(this.btnFomChem_Click);
            // 
            // btnCaCO3Chem
            // 
            this.btnCaCO3Chem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCaCO3Chem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCaCO3Chem.Location = new System.Drawing.Point(528, 250);
            this.btnCaCO3Chem.Name = "btnCaCO3Chem";
            this.btnCaCO3Chem.Size = new System.Drawing.Size(109, 34);
            this.btnCaCO3Chem.TabIndex = 46;
            this.btnCaCO3Chem.TabStop = false;
            this.btnCaCO3Chem.Text = "Хим.состав\r\n";
            this.btnCaCO3Chem.UseVisualStyleBackColor = true;
            this.btnCaCO3Chem.Click += new System.EventHandler(this.btnLimeStoneChem_Click);
            // 
            // txbLimeIn
            // 
            this.txbLimeIn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txbLimeIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbLimeIn.Location = new System.Drawing.Point(204, 133);
            this.txbLimeIn.Name = "txbLimeIn";
            this.txbLimeIn.Size = new System.Drawing.Size(95, 21);
            this.txbLimeIn.TabIndex = 47;
            // 
            // txbDolomIn
            // 
            this.txbDolomIn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txbDolomIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbDolomIn.Location = new System.Drawing.Point(204, 174);
            this.txbDolomIn.Name = "txbDolomIn";
            this.txbDolomIn.Size = new System.Drawing.Size(95, 21);
            this.txbDolomIn.TabIndex = 48;
            // 
            // panIronTemp
            // 
            this.panIronTemp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panIronTemp.Controls.Add(this.lblIronTemp);
            this.panIronTemp.Controls.Add(this.txbIronTemp);
            this.panIronTemp.Location = new System.Drawing.Point(372, 4);
            this.panIronTemp.Name = "panIronTemp";
            this.panIronTemp.Size = new System.Drawing.Size(112, 34);
            this.panIronTemp.TabIndex = 27;
            // 
            // lblIronTemp
            // 
            this.lblIronTemp.AutoSize = true;
            this.lblIronTemp.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblIronTemp.Location = new System.Drawing.Point(0, 0);
            this.lblIronTemp.Name = "lblIronTemp";
            this.lblIronTemp.Size = new System.Drawing.Size(91, 13);
            this.lblIronTemp.TabIndex = 22;
            this.lblIronTemp.Text = "Температура, °C";
            // 
            // txbIronTemp
            // 
            this.txbIronTemp.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txbIronTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbIronTemp.Location = new System.Drawing.Point(0, 13);
            this.txbIronTemp.Name = "txbIronTemp";
            this.txbIronTemp.Size = new System.Drawing.Size(112, 21);
            this.txbIronTemp.TabIndex = 22;
            // 
            // calcDolmax
            // 
            this.calcDolmax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.calcDolmax.AutoSize = true;
            this.calcDolmax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.calcDolmax.Location = new System.Drawing.Point(376, 175);
            this.calcDolmax.Name = "calcDolmax";
            this.calcDolmax.Size = new System.Drawing.Size(104, 19);
            this.calcDolmax.TabIndex = 51;
            this.calcDolmax.Text = "Рассчитать";
            this.calcDolmax.UseVisualStyleBackColor = true;
            this.calcDolmax.CheckedChanged += new System.EventHandler(this.aCalcCheckedChanged);
            // 
            // calcFom
            // 
            this.calcFom.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.calcFom.AutoSize = true;
            this.calcFom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.calcFom.Location = new System.Drawing.Point(376, 216);
            this.calcFom.Name = "calcFom";
            this.calcFom.Size = new System.Drawing.Size(104, 19);
            this.calcFom.TabIndex = 52;
            this.calcFom.Tag = "txbFomIn";
            this.calcFom.Text = "Рассчитать";
            this.calcFom.UseVisualStyleBackColor = true;
            this.calcFom.CheckStateChanged += new System.EventHandler(this.aCalcCheckedChanged);
            // 
            // calcVapenec
            // 
            this.calcVapenec.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.calcVapenec.AutoSize = true;
            this.calcVapenec.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.calcVapenec.Location = new System.Drawing.Point(376, 257);
            this.calcVapenec.Name = "calcVapenec";
            this.calcVapenec.Size = new System.Drawing.Size(104, 19);
            this.calcVapenec.TabIndex = 53;
            this.calcVapenec.Tag = "txbLimeStoneIn";
            this.calcVapenec.Text = "Рассчитать";
            this.calcVapenec.UseVisualStyleBackColor = true;
            this.calcVapenec.CheckStateChanged += new System.EventHandler(this.aCalcCheckedChanged);
            // 
            // calcVapno
            // 
            this.calcVapno.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.calcVapno.AutoSize = true;
            this.calcVapno.CausesValidation = false;
            this.calcVapno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.calcVapno.Location = new System.Drawing.Point(376, 134);
            this.calcVapno.Name = "calcVapno";
            this.calcVapno.Size = new System.Drawing.Size(104, 19);
            this.calcVapno.TabIndex = 50;
            this.calcVapno.Text = "Рассчитать";
            this.calcVapno.UseVisualStyleBackColor = true;
            this.calcVapno.CheckedChanged += new System.EventHandler(this.aCalcCheckedChanged);
            // 
            // btnDolomiteChem
            // 
            this.btnDolomiteChem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDolomiteChem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnDolomiteChem.Location = new System.Drawing.Point(528, 168);
            this.btnDolomiteChem.Name = "btnDolomiteChem";
            this.btnDolomiteChem.Size = new System.Drawing.Size(109, 34);
            this.btnDolomiteChem.TabIndex = 54;
            this.btnDolomiteChem.TabStop = false;
            this.btnDolomiteChem.Text = "Хим.состав\r\n";
            this.btnDolomiteChem.UseVisualStyleBackColor = true;
            this.btnDolomiteChem.Click += new System.EventHandler(this.btnDolomiteChem_Click);
            // 
            // txbCokeIn
            // 
            this.txbCokeIn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txbCokeIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbCokeIn.Location = new System.Drawing.Point(204, 297);
            this.txbCokeIn.Name = "txbCokeIn";
            this.txbCokeIn.Size = new System.Drawing.Size(95, 21);
            this.txbCokeIn.TabIndex = 57;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(4, 298);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 20);
            this.label3.TabIndex = 56;
            this.label3.Text = "Кокс, кг";
            // 
            // btnVapno
            // 
            this.btnVapno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnVapno.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnVapno.Location = new System.Drawing.Point(528, 127);
            this.btnVapno.Name = "btnVapno";
            this.btnVapno.Size = new System.Drawing.Size(109, 34);
            this.btnVapno.TabIndex = 55;
            this.btnVapno.TabStop = false;
            this.btnVapno.Text = "Хим.состав\r\n";
            this.btnVapno.UseVisualStyleBackColor = true;
            this.btnVapno.Click += new System.EventHandler(this.btnVapno_Click);
            // 
            // btnIronSel
            // 
            this.btnIronSel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnIronSel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnIronSel.Location = new System.Drawing.Point(644, 4);
            this.btnIronSel.Name = "btnIronSel";
            this.btnIronSel.Size = new System.Drawing.Size(110, 34);
            this.btnIronSel.TabIndex = 58;
            this.btnIronSel.Text = "Выбрать...";
            this.btnIronSel.UseVisualStyleBackColor = true;
            this.btnIronSel.Click += new System.EventHandler(this.btnIronSel_Click);
            // 
            // btnScrapSel
            // 
            this.btnScrapSel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnScrapSel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnScrapSel.Location = new System.Drawing.Point(644, 45);
            this.btnScrapSel.Name = "btnScrapSel";
            this.btnScrapSel.Size = new System.Drawing.Size(110, 34);
            this.btnScrapSel.TabIndex = 59;
            this.btnScrapSel.Text = "Выбрать...";
            this.btnScrapSel.UseVisualStyleBackColor = true;
            this.btnScrapSel.Click += new System.EventHandler(this.btnScrapSel_Click);
            // 
            // panControl
            // 
            this.panControl.BackColor = System.Drawing.Color.CadetBlue;
            this.panControl.Controls.Add(this.btnCalculate);
            this.panControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panControl.Location = new System.Drawing.Point(0, 478);
            this.panControl.Name = "panControl";
            this.panControl.Size = new System.Drawing.Size(361, 107);
            this.panControl.TabIndex = 100;
            // 
            // btnCalculate
            // 
            this.btnCalculate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCalculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCalculate.ForeColor = System.Drawing.Color.Black;
            this.btnCalculate.Location = new System.Drawing.Point(40, 13);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btnCalculate.Size = new System.Drawing.Size(280, 72);
            this.btnCalculate.TabIndex = 99;
            this.btnCalculate.Text = "Выполнить расчёт";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // panAuxOut
            // 
            this.panAuxOut.ColumnCount = 7;
            this.panAuxOut.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.panAuxOut.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.panAuxOut.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.panAuxOut.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.panAuxOut.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.panAuxOut.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.panAuxOut.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.panAuxOut.Controls.Add(this.txtSn, 5, 3);
            this.panAuxOut.Controls.Add(this.txtW, 2, 3);
            this.panAuxOut.Controls.Add(this.txtSb, 5, 2);
            this.panAuxOut.Controls.Add(this.txtNi, 2, 2);
            this.panAuxOut.Controls.Add(this.txtAs, 5, 1);
            this.panAuxOut.Controls.Add(this.txtMo, 2, 1);
            this.panAuxOut.Controls.Add(this.txtCo, 5, 0);
            this.panAuxOut.Controls.Add(this.lblSb, 4, 2);
            this.panAuxOut.Controls.Add(this.txtCu, 2, 0);
            this.panAuxOut.Controls.Add(this.lblCu, 1, 0);
            this.panAuxOut.Controls.Add(this.lblMo, 1, 1);
            this.panAuxOut.Controls.Add(this.lblNi, 1, 2);
            this.panAuxOut.Controls.Add(this.lblW, 1, 3);
            this.panAuxOut.Controls.Add(this.lblCo, 4, 0);
            this.panAuxOut.Controls.Add(this.lblAs, 4, 1);
            this.panAuxOut.Controls.Add(this.lblSn, 4, 3);
            this.panAuxOut.Enabled = false;
            this.panAuxOut.Location = new System.Drawing.Point(0, 350);
            this.panAuxOut.Name = "panAuxOut";
            this.panAuxOut.RowCount = 4;
            this.panAuxOut.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.panAuxOut.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.panAuxOut.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.panAuxOut.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.panAuxOut.Size = new System.Drawing.Size(359, 108);
            this.panAuxOut.TabIndex = 3;
            // 
            // txtSn
            // 
            this.txtSn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtSn.Location = new System.Drawing.Point(259, 84);
            this.txtSn.Name = "txtSn";
            this.txtSn.Size = new System.Drawing.Size(76, 21);
            this.txtSn.TabIndex = 54;
            this.txtSn.TabStop = false;
            // 
            // txtW
            // 
            this.txtW.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtW.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtW.Location = new System.Drawing.Point(90, 84);
            this.txtW.Name = "txtW";
            this.txtW.Size = new System.Drawing.Size(76, 21);
            this.txtW.TabIndex = 53;
            this.txtW.TabStop = false;
            // 
            // txtSb
            // 
            this.txtSb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtSb.Location = new System.Drawing.Point(259, 57);
            this.txtSb.Name = "txtSb";
            this.txtSb.Size = new System.Drawing.Size(76, 21);
            this.txtSb.TabIndex = 52;
            this.txtSb.TabStop = false;
            // 
            // txtNi
            // 
            this.txtNi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtNi.Location = new System.Drawing.Point(90, 57);
            this.txtNi.Name = "txtNi";
            this.txtNi.Size = new System.Drawing.Size(76, 21);
            this.txtNi.TabIndex = 51;
            this.txtNi.TabStop = false;
            // 
            // txtAs
            // 
            this.txtAs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtAs.Location = new System.Drawing.Point(259, 30);
            this.txtAs.Name = "txtAs";
            this.txtAs.Size = new System.Drawing.Size(76, 21);
            this.txtAs.TabIndex = 50;
            this.txtAs.TabStop = false;
            // 
            // txtMo
            // 
            this.txtMo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMo.Location = new System.Drawing.Point(90, 30);
            this.txtMo.Name = "txtMo";
            this.txtMo.Size = new System.Drawing.Size(76, 21);
            this.txtMo.TabIndex = 49;
            this.txtMo.TabStop = false;
            // 
            // txtCo
            // 
            this.txtCo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtCo.Location = new System.Drawing.Point(259, 3);
            this.txtCo.Name = "txtCo";
            this.txtCo.Size = new System.Drawing.Size(76, 21);
            this.txtCo.TabIndex = 48;
            this.txtCo.TabStop = false;
            // 
            // lblSb
            // 
            this.lblSb.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSb.AutoSize = true;
            this.lblSb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSb.Location = new System.Drawing.Point(192, 60);
            this.lblSb.Name = "lblSb";
            this.lblSb.Size = new System.Drawing.Size(44, 15);
            this.lblSb.TabIndex = 42;
            this.lblSb.Text = "Sb, %";
            // 
            // txtCu
            // 
            this.txtCu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtCu.Location = new System.Drawing.Point(90, 3);
            this.txtCu.Name = "txtCu";
            this.txtCu.Size = new System.Drawing.Size(76, 21);
            this.txtCu.TabIndex = 39;
            this.txtCu.TabStop = false;
            // 
            // lblCu
            // 
            this.lblCu.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblCu.AutoSize = true;
            this.lblCu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCu.Location = new System.Drawing.Point(23, 6);
            this.lblCu.Name = "lblCu";
            this.lblCu.Size = new System.Drawing.Size(44, 15);
            this.lblCu.TabIndex = 41;
            this.lblCu.Text = "Cu, %";
            // 
            // lblMo
            // 
            this.lblMo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblMo.AutoSize = true;
            this.lblMo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblMo.Location = new System.Drawing.Point(23, 33);
            this.lblMo.Name = "lblMo";
            this.lblMo.Size = new System.Drawing.Size(47, 15);
            this.lblMo.TabIndex = 42;
            this.lblMo.Text = "Mo, %";
            // 
            // lblNi
            // 
            this.lblNi.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblNi.AutoSize = true;
            this.lblNi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblNi.Location = new System.Drawing.Point(23, 60);
            this.lblNi.Name = "lblNi";
            this.lblNi.Size = new System.Drawing.Size(41, 15);
            this.lblNi.TabIndex = 43;
            this.lblNi.Text = "Ni, %";
            // 
            // lblW
            // 
            this.lblW.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblW.AutoSize = true;
            this.lblW.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblW.Location = new System.Drawing.Point(23, 87);
            this.lblW.Name = "lblW";
            this.lblW.Size = new System.Drawing.Size(39, 15);
            this.lblW.TabIndex = 44;
            this.lblW.Text = "W, %";
            // 
            // lblCo
            // 
            this.lblCo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblCo.AutoSize = true;
            this.lblCo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCo.Location = new System.Drawing.Point(192, 6);
            this.lblCo.Name = "lblCo";
            this.lblCo.Size = new System.Drawing.Size(44, 15);
            this.lblCo.TabIndex = 45;
            this.lblCo.Text = "Co, %";
            // 
            // lblAs
            // 
            this.lblAs.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblAs.AutoSize = true;
            this.lblAs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblAs.Location = new System.Drawing.Point(192, 33);
            this.lblAs.Name = "lblAs";
            this.lblAs.Size = new System.Drawing.Size(42, 15);
            this.lblAs.TabIndex = 46;
            this.lblAs.Text = "As, %";
            // 
            // lblSn
            // 
            this.lblSn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSn.AutoSize = true;
            this.lblSn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSn.Location = new System.Drawing.Point(192, 87);
            this.lblSn.Name = "lblSn";
            this.lblSn.Size = new System.Drawing.Size(44, 15);
            this.lblSn.TabIndex = 47;
            this.lblSn.Text = "Sn, %";
            // 
            // panCommonOut
            // 
            this.panCommonOut.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.panCommonOut.ColumnCount = 3;
            this.panCommonOut.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.panCommonOut.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.panCommonOut.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.panCommonOut.Controls.Add(this.panSteelOut, 1, 2);
            this.panCommonOut.Controls.Add(this.txbLimeStoneOut, 1, 6);
            this.panCommonOut.Controls.Add(this.btnScrapOut, 2, 1);
            this.panCommonOut.Controls.Add(this.txbFomOut, 1, 5);
            this.panCommonOut.Controls.Add(this.btnIronOut, 2, 0);
            this.panCommonOut.Controls.Add(this.panIronMass, 1, 0);
            this.panCommonOut.Controls.Add(this.lblDolomite, 0, 4);
            this.panCommonOut.Controls.Add(this.lblCokeOut, 0, 7);
            this.panCommonOut.Controls.Add(this.txbDolomitOut, 1, 4);
            this.panCommonOut.Controls.Add(this.lblLimeStoneOut, 0, 6);
            this.panCommonOut.Controls.Add(this.txbLimeOut, 1, 3);
            this.panCommonOut.Controls.Add(this.lblIronOut, 0, 0);
            this.panCommonOut.Controls.Add(this.lblLime, 0, 3);
            this.panCommonOut.Controls.Add(this.lblFomOut, 0, 5);
            this.panCommonOut.Controls.Add(this.lblScrapOut, 0, 1);
            this.panCommonOut.Controls.Add(this.panScrapMass, 1, 1);
            this.panCommonOut.Controls.Add(this.lblSteelOut, 0, 2);
            this.panCommonOut.Controls.Add(this.txbCokeOut, 1, 7);
            this.panCommonOut.Dock = System.Windows.Forms.DockStyle.Top;
            this.panCommonOut.Enabled = false;
            this.panCommonOut.Location = new System.Drawing.Point(0, 0);
            this.panCommonOut.Name = "panCommonOut";
            this.panCommonOut.RowCount = 8;
            this.panCommonOut.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panCommonOut.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panCommonOut.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panCommonOut.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panCommonOut.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panCommonOut.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panCommonOut.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panCommonOut.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panCommonOut.Size = new System.Drawing.Size(361, 323);
            this.panCommonOut.TabIndex = 2;
            // 
            // panSteelOut
            // 
            this.panSteelOut.Controls.Add(this.txbSteelOut);
            this.panSteelOut.Controls.Add(this.label1);
            this.panSteelOut.Location = new System.Drawing.Point(175, 86);
            this.panSteelOut.Name = "panSteelOut";
            this.panSteelOut.Size = new System.Drawing.Size(112, 34);
            this.panSteelOut.TabIndex = 28;
            // 
            // txbSteelOut
            // 
            this.txbSteelOut.Enabled = false;
            this.txbSteelOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbSteelOut.Location = new System.Drawing.Point(4, 14);
            this.txbSteelOut.Name = "txbSteelOut";
            this.txbSteelOut.Size = new System.Drawing.Size(100, 21);
            this.txbSteelOut.TabIndex = 31;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Общая масса, т";
            // 
            // txbLimeStoneOut
            // 
            this.txbLimeStoneOut.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txbLimeStoneOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbLimeStoneOut.Location = new System.Drawing.Point(175, 256);
            this.txbLimeStoneOut.Name = "txbLimeStoneOut";
            this.txbLimeStoneOut.Size = new System.Drawing.Size(112, 21);
            this.txbLimeStoneOut.TabIndex = 35;
            // 
            // btnScrapOut
            // 
            this.btnScrapOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnScrapOut.Enabled = false;
            this.btnScrapOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnScrapOut.Location = new System.Drawing.Point(297, 45);
            this.btnScrapOut.Name = "btnScrapOut";
            this.btnScrapOut.Size = new System.Drawing.Size(60, 34);
            this.btnScrapOut.TabIndex = 32;
            this.btnScrapOut.TabStop = false;
            this.btnScrapOut.Text = "Состав...";
            this.btnScrapOut.UseVisualStyleBackColor = true;
            this.btnScrapOut.Visible = false;
            // 
            // txbFomOut
            // 
            this.txbFomOut.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txbFomOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbFomOut.Location = new System.Drawing.Point(175, 215);
            this.txbFomOut.Name = "txbFomOut";
            this.txbFomOut.Size = new System.Drawing.Size(112, 21);
            this.txbFomOut.TabIndex = 34;
            // 
            // btnIronOut
            // 
            this.btnIronOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnIronOut.Enabled = false;
            this.btnIronOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnIronOut.Location = new System.Drawing.Point(297, 4);
            this.btnIronOut.Name = "btnIronOut";
            this.btnIronOut.Size = new System.Drawing.Size(60, 34);
            this.btnIronOut.TabIndex = 31;
            this.btnIronOut.TabStop = false;
            this.btnIronOut.Text = "Состав...";
            this.btnIronOut.UseVisualStyleBackColor = true;
            this.btnIronOut.Visible = false;
            // 
            // panIronMass
            // 
            this.panIronMass.Controls.Add(this.txbIronOut);
            this.panIronMass.Controls.Add(this.lblIronMass);
            this.panIronMass.Location = new System.Drawing.Point(175, 4);
            this.panIronMass.Name = "panIronMass";
            this.panIronMass.Size = new System.Drawing.Size(112, 34);
            this.panIronMass.TabIndex = 27;
            // 
            // txbIronOut
            // 
            this.txbIronOut.Enabled = false;
            this.txbIronOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbIronOut.Location = new System.Drawing.Point(4, 14);
            this.txbIronOut.Name = "txbIronOut";
            this.txbIronOut.Size = new System.Drawing.Size(100, 21);
            this.txbIronOut.TabIndex = 29;
            // 
            // lblIronMass
            // 
            this.lblIronMass.AutoSize = true;
            this.lblIronMass.Location = new System.Drawing.Point(3, 0);
            this.lblIronMass.Name = "lblIronMass";
            this.lblIronMass.Size = new System.Drawing.Size(88, 13);
            this.lblIronMass.TabIndex = 22;
            this.lblIronMass.Text = "Общая масса, т";
            // 
            // lblDolomite
            // 
            this.lblDolomite.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDolomite.AutoSize = true;
            this.lblDolomite.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDolomite.Location = new System.Drawing.Point(4, 175);
            this.lblDolomite.Name = "lblDolomite";
            this.lblDolomite.Size = new System.Drawing.Size(153, 20);
            this.lblDolomite.TabIndex = 7;
            this.lblDolomite.Text = "Доломит суш., кг";
            // 
            // lblCokeOut
            // 
            this.lblCokeOut.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblCokeOut.AutoSize = true;
            this.lblCokeOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCokeOut.Location = new System.Drawing.Point(4, 295);
            this.lblCokeOut.Name = "lblCokeOut";
            this.lblCokeOut.Size = new System.Drawing.Size(75, 20);
            this.lblCokeOut.TabIndex = 9;
            this.lblCokeOut.Text = "Кокс, кг";
            // 
            // txbDolomitOut
            // 
            this.txbDolomitOut.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txbDolomitOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbDolomitOut.Location = new System.Drawing.Point(175, 174);
            this.txbDolomitOut.Name = "txbDolomitOut";
            this.txbDolomitOut.Size = new System.Drawing.Size(112, 21);
            this.txbDolomitOut.TabIndex = 33;
            // 
            // lblLimeStoneOut
            // 
            this.lblLimeStoneOut.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblLimeStoneOut.AutoSize = true;
            this.lblLimeStoneOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLimeStoneOut.Location = new System.Drawing.Point(4, 257);
            this.lblLimeStoneOut.Name = "lblLimeStoneOut";
            this.lblLimeStoneOut.Size = new System.Drawing.Size(125, 20);
            this.lblLimeStoneOut.TabIndex = 5;
            this.lblLimeStoneOut.Text = "Шлакообр., кг";
            // 
            // txbLimeOut
            // 
            this.txbLimeOut.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txbLimeOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbLimeOut.Location = new System.Drawing.Point(175, 133);
            this.txbLimeOut.Name = "txbLimeOut";
            this.txbLimeOut.Size = new System.Drawing.Size(112, 21);
            this.txbLimeOut.TabIndex = 32;
            // 
            // lblIronOut
            // 
            this.lblIronOut.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblIronOut.AutoSize = true;
            this.lblIronOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblIronOut.Location = new System.Drawing.Point(4, 11);
            this.lblIronOut.Name = "lblIronOut";
            this.lblIronOut.Size = new System.Drawing.Size(56, 20);
            this.lblIronOut.TabIndex = 4;
            this.lblIronOut.Text = "Чугун";
            // 
            // lblLime
            // 
            this.lblLime.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblLime.AutoSize = true;
            this.lblLime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLime.Location = new System.Drawing.Point(4, 134);
            this.lblLime.Name = "lblLime";
            this.lblLime.Size = new System.Drawing.Size(106, 20);
            this.lblLime.TabIndex = 8;
            this.lblLime.Text = "Известь, кг";
            // 
            // lblFomOut
            // 
            this.lblFomOut.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblFomOut.AutoSize = true;
            this.lblFomOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFomOut.Location = new System.Drawing.Point(4, 216);
            this.lblFomOut.Name = "lblFomOut";
            this.lblFomOut.Size = new System.Drawing.Size(79, 20);
            this.lblFomOut.TabIndex = 8;
            this.lblFomOut.Text = "ФОМ, кг";
            // 
            // lblScrapOut
            // 
            this.lblScrapOut.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblScrapOut.AutoSize = true;
            this.lblScrapOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblScrapOut.Location = new System.Drawing.Point(4, 52);
            this.lblScrapOut.Name = "lblScrapOut";
            this.lblScrapOut.Size = new System.Drawing.Size(44, 20);
            this.lblScrapOut.TabIndex = 6;
            this.lblScrapOut.Text = "Лом";
            // 
            // panScrapMass
            // 
            this.panScrapMass.Controls.Add(this.txbScrapOut);
            this.panScrapMass.Controls.Add(this.lblScrapMass);
            this.panScrapMass.Dock = System.Windows.Forms.DockStyle.Left;
            this.panScrapMass.Enabled = false;
            this.panScrapMass.Location = new System.Drawing.Point(175, 45);
            this.panScrapMass.Name = "panScrapMass";
            this.panScrapMass.Size = new System.Drawing.Size(112, 34);
            this.panScrapMass.TabIndex = 28;
            // 
            // txbScrapOut
            // 
            this.txbScrapOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbScrapOut.Location = new System.Drawing.Point(4, 14);
            this.txbScrapOut.Name = "txbScrapOut";
            this.txbScrapOut.Size = new System.Drawing.Size(100, 21);
            this.txbScrapOut.TabIndex = 30;
            // 
            // lblScrapMass
            // 
            this.lblScrapMass.AutoSize = true;
            this.lblScrapMass.Location = new System.Drawing.Point(3, 0);
            this.lblScrapMass.Name = "lblScrapMass";
            this.lblScrapMass.Size = new System.Drawing.Size(88, 13);
            this.lblScrapMass.TabIndex = 22;
            this.lblScrapMass.Text = "Общая масса, т";
            // 
            // lblSteelOut
            // 
            this.lblSteelOut.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSteelOut.AutoSize = true;
            this.lblSteelOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSteelOut.Location = new System.Drawing.Point(4, 93);
            this.lblSteelOut.Name = "lblSteelOut";
            this.lblSteelOut.Size = new System.Drawing.Size(62, 20);
            this.lblSteelOut.TabIndex = 43;
            this.lblSteelOut.Text = "Сталь";
            // 
            // txbCokeOut
            // 
            this.txbCokeOut.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txbCokeOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbCokeOut.Location = new System.Drawing.Point(175, 294);
            this.txbCokeOut.Name = "txbCokeOut";
            this.txbCokeOut.Size = new System.Drawing.Size(112, 21);
            this.txbCokeOut.TabIndex = 38;
            this.txbCokeOut.TabStop = false;
            // 
            // heading
            // 
            this.heading.BackColor = System.Drawing.Color.LightSalmon;
            this.heading.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.heading.Controls.Add(this.panTitleHeading);
            this.heading.Controls.Add(this.panHeatNum);
            this.heading.Controls.Add(this.panSteelBrand);
            this.heading.Controls.Add(this.panSteelTemp);
            this.heading.Controls.Add(this.panCarbonPercent);
            this.heading.Controls.Add(this.panel1);
            this.heading.Controls.Add(this.panel2);
            this.heading.Controls.Add(this.panAligner);
            this.heading.Controls.Add(this.panBasiticy);
            this.heading.Dock = System.Windows.Forms.DockStyle.Top;
            this.heading.Location = new System.Drawing.Point(0, 0);
            this.heading.Name = "heading";
            this.heading.Size = new System.Drawing.Size(1123, 110);
            this.heading.TabIndex = 0;
            // 
            // panTitleHeading
            // 
            this.panTitleHeading.Controls.Add(this.lblTitleHeading);
            this.panTitleHeading.Location = new System.Drawing.Point(3, 3);
            this.panTitleHeading.Name = "panTitleHeading";
            this.panTitleHeading.Size = new System.Drawing.Size(205, 43);
            this.panTitleHeading.TabIndex = 29;
            // 
            // lblTitleHeading
            // 
            this.lblTitleHeading.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitleHeading.AutoSize = true;
            this.lblTitleHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTitleHeading.Location = new System.Drawing.Point(14, 11);
            this.lblTitleHeading.Name = "lblTitleHeading";
            this.lblTitleHeading.Size = new System.Drawing.Size(189, 26);
            this.lblTitleHeading.TabIndex = 28;
            this.lblTitleHeading.Text = "Паспорт плавки";
            // 
            // panHeatNum
            // 
            this.panHeatNum.Controls.Add(this.txbHeatNum);
            this.panHeatNum.Controls.Add(this.lblHeatNum);
            this.panHeatNum.Location = new System.Drawing.Point(214, 3);
            this.panHeatNum.Name = "panHeatNum";
            this.panHeatNum.Size = new System.Drawing.Size(161, 43);
            this.panHeatNum.TabIndex = 24;
            // 
            // txbHeatNum
            // 
            this.txbHeatNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbHeatNum.Location = new System.Drawing.Point(4, 18);
            this.txbHeatNum.Name = "txbHeatNum";
            this.txbHeatNum.Size = new System.Drawing.Size(143, 21);
            this.txbHeatNum.TabIndex = 17;
            this.txbHeatNum.TabStop = false;
            // 
            // lblHeatNum
            // 
            this.lblHeatNum.AutoSize = true;
            this.lblHeatNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblHeatNum.Location = new System.Drawing.Point(1, 3);
            this.lblHeatNum.Name = "lblHeatNum";
            this.lblHeatNum.Size = new System.Drawing.Size(92, 13);
            this.lblHeatNum.TabIndex = 18;
            this.lblHeatNum.Text = "Номер плавки";
            // 
            // panSteelBrand
            // 
            this.panSteelBrand.Controls.Add(this.cmbSteelBrand);
            this.panSteelBrand.Controls.Add(this.lblSteelBrand);
            this.panSteelBrand.Location = new System.Drawing.Point(381, 3);
            this.panSteelBrand.Name = "panSteelBrand";
            this.panSteelBrand.Size = new System.Drawing.Size(114, 43);
            this.panSteelBrand.TabIndex = 23;
            // 
            // cmbSteelBrand
            // 
            this.cmbSteelBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbSteelBrand.FormattingEnabled = true;
            this.cmbSteelBrand.Items.AddRange(new object[] {
            "Группа 1",
            "Группа 2",
            "Группа 3",
            "Группа 4",
            "Группа 5"});
            this.cmbSteelBrand.Location = new System.Drawing.Point(3, 17);
            this.cmbSteelBrand.Name = "cmbSteelBrand";
            this.cmbSteelBrand.Size = new System.Drawing.Size(92, 23);
            this.cmbSteelBrand.TabIndex = 13;
            this.cmbSteelBrand.TabStop = false;
            // 
            // lblSteelBrand
            // 
            this.lblSteelBrand.AutoSize = true;
            this.lblSteelBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSteelBrand.Location = new System.Drawing.Point(1, 1);
            this.lblSteelBrand.Name = "lblSteelBrand";
            this.lblSteelBrand.Size = new System.Drawing.Size(86, 13);
            this.lblSteelBrand.TabIndex = 14;
            this.lblSteelBrand.Text = "Группа стали";
            // 
            // panSteelTemp
            // 
            this.panSteelTemp.Controls.Add(this.txbSteelTemp);
            this.panSteelTemp.Controls.Add(this.lblSteelTemp);
            this.panSteelTemp.Location = new System.Drawing.Point(501, 3);
            this.panSteelTemp.Name = "panSteelTemp";
            this.panSteelTemp.Size = new System.Drawing.Size(119, 43);
            this.panSteelTemp.TabIndex = 26;
            // 
            // txbSteelTemp
            // 
            this.txbSteelTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbSteelTemp.Location = new System.Drawing.Point(4, 20);
            this.txbSteelTemp.Name = "txbSteelTemp";
            this.txbSteelTemp.Size = new System.Drawing.Size(100, 21);
            this.txbSteelTemp.TabIndex = 21;
            // 
            // lblSteelTemp
            // 
            this.lblSteelTemp.AutoSize = true;
            this.lblSteelTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSteelTemp.Location = new System.Drawing.Point(-1, 2);
            this.lblSteelTemp.Name = "lblSteelTemp";
            this.lblSteelTemp.Size = new System.Drawing.Size(106, 13);
            this.lblSteelTemp.TabIndex = 22;
            this.lblSteelTemp.Text = "Температура, °C";
            // 
            // panCarbonPercent
            // 
            this.panCarbonPercent.Controls.Add(this.lblCarbonPercent);
            this.panCarbonPercent.Controls.Add(this.txbCarbonPercent);
            this.panCarbonPercent.Location = new System.Drawing.Point(626, 3);
            this.panCarbonPercent.Name = "panCarbonPercent";
            this.panCarbonPercent.Size = new System.Drawing.Size(120, 43);
            this.panCarbonPercent.TabIndex = 27;
            // 
            // lblCarbonPercent
            // 
            this.lblCarbonPercent.AutoSize = true;
            this.lblCarbonPercent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCarbonPercent.Location = new System.Drawing.Point(0, 2);
            this.lblCarbonPercent.Name = "lblCarbonPercent";
            this.lblCarbonPercent.Size = new System.Drawing.Size(115, 13);
            this.lblCarbonPercent.TabIndex = 20;
            this.lblCarbonPercent.Text = "Конц. углерода, %";
            // 
            // txbCarbonPercent
            // 
            this.txbCarbonPercent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbCarbonPercent.Location = new System.Drawing.Point(3, 20);
            this.txbCarbonPercent.Name = "txbCarbonPercent";
            this.txbCarbonPercent.Size = new System.Drawing.Size(100, 21);
            this.txbCarbonPercent.TabIndex = 19;
            this.txbCarbonPercent.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblTgtMgO);
            this.panel1.Controls.Add(this.txbMgO);
            this.panel1.Location = new System.Drawing.Point(752, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(120, 43);
            this.panel1.TabIndex = 28;
            // 
            // lblTgtMgO
            // 
            this.lblTgtMgO.AutoSize = true;
            this.lblTgtMgO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTgtMgO.Location = new System.Drawing.Point(0, 2);
            this.lblTgtMgO.Name = "lblTgtMgO";
            this.lblTgtMgO.Size = new System.Drawing.Size(87, 13);
            this.lblTgtMgO.TabIndex = 20;
            this.lblTgtMgO.Text = "Конц. MgO, %";
            // 
            // txbMgO
            // 
            this.txbMgO.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbMgO.Location = new System.Drawing.Point(3, 20);
            this.txbMgO.Name = "txbMgO";
            this.txbMgO.Size = new System.Drawing.Size(100, 21);
            this.txbMgO.TabIndex = 19;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblTgtFeO);
            this.panel2.Controls.Add(this.txbFeO);
            this.panel2.Location = new System.Drawing.Point(878, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(120, 43);
            this.panel2.TabIndex = 30;
            // 
            // lblTgtFeO
            // 
            this.lblTgtFeO.AutoSize = true;
            this.lblTgtFeO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTgtFeO.Location = new System.Drawing.Point(0, 2);
            this.lblTgtFeO.Name = "lblTgtFeO";
            this.lblTgtFeO.Size = new System.Drawing.Size(84, 13);
            this.lblTgtFeO.TabIndex = 20;
            this.lblTgtFeO.Text = "Конц. FeO, %";
            // 
            // txbFeO
            // 
            this.txbFeO.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbFeO.Location = new System.Drawing.Point(3, 20);
            this.txbFeO.Name = "txbFeO";
            this.txbFeO.Size = new System.Drawing.Size(100, 21);
            this.txbFeO.TabIndex = 19;
            // 
            // panAligner
            // 
            this.panAligner.Location = new System.Drawing.Point(3, 52);
            this.panAligner.Name = "panAligner";
            this.panAligner.Size = new System.Drawing.Size(205, 44);
            this.panAligner.TabIndex = 31;
            // 
            // panBasiticy
            // 
            this.panBasiticy.Controls.Add(this.label2);
            this.panBasiticy.Controls.Add(this.txbBasiticy);
            this.panBasiticy.Location = new System.Drawing.Point(214, 52);
            this.panBasiticy.Name = "panBasiticy";
            this.panBasiticy.Size = new System.Drawing.Size(161, 43);
            this.panBasiticy.TabIndex = 28;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(0, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Отношение CaO/SiO2";
            // 
            // txbBasiticy
            // 
            this.txbBasiticy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbBasiticy.Location = new System.Drawing.Point(3, 20);
            this.txbBasiticy.Name = "txbBasiticy";
            this.txbBasiticy.Size = new System.Drawing.Size(144, 21);
            this.txbBasiticy.TabIndex = 19;
            // 
            // MixtureInitial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1123, 695);
            this.Controls.Add(this.basement);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1133, 727);
            this.MinimumSize = new System.Drawing.Size(560, 320);
            this.Name = "MixtureInitial";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Алгоритм рассчета компонентов \"Шихта-1\"";
            this.Load += new System.EventHandler(this.MixtureInitial_Load);
            this.basement.ResumeLayout(false);
            this.splitIronScrap.Panel1.ResumeLayout(false);
            this.splitIronScrap.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitIronScrap)).EndInit();
            this.splitIronScrap.ResumeLayout(false);
            this.panSlagInput.ResumeLayout(false);
            this.panSlagInput.PerformLayout();
            this.panScrapTemp.ResumeLayout(false);
            this.panScrapTemp.PerformLayout();
            this.panIronTemp.ResumeLayout(false);
            this.panIronTemp.PerformLayout();
            this.panControl.ResumeLayout(false);
            this.panAuxOut.ResumeLayout(false);
            this.panAuxOut.PerformLayout();
            this.panCommonOut.ResumeLayout(false);
            this.panCommonOut.PerformLayout();
            this.panSteelOut.ResumeLayout(false);
            this.panSteelOut.PerformLayout();
            this.panIronMass.ResumeLayout(false);
            this.panIronMass.PerformLayout();
            this.panScrapMass.ResumeLayout(false);
            this.panScrapMass.PerformLayout();
            this.heading.ResumeLayout(false);
            this.panTitleHeading.ResumeLayout(false);
            this.panTitleHeading.PerformLayout();
            this.panHeatNum.ResumeLayout(false);
            this.panHeatNum.PerformLayout();
            this.panSteelBrand.ResumeLayout(false);
            this.panSteelBrand.PerformLayout();
            this.panSteelTemp.ResumeLayout(false);
            this.panSteelTemp.PerformLayout();
            this.panCarbonPercent.ResumeLayout(false);
            this.panCarbonPercent.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panBasiticy.ResumeLayout(false);
            this.panBasiticy.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel basement;
        private System.Windows.Forms.FlowLayoutPanel heading;
        private System.Windows.Forms.Label lblHeatNum;
        private System.Windows.Forms.Label lblCarbonPercent;
        private System.Windows.Forms.Label lblSteelTemp;
        private System.Windows.Forms.Panel panSteelBrand;
        private System.Windows.Forms.Label lblSteelBrand;
        private System.Windows.Forms.ComboBox cmbSteelBrand;
        private System.Windows.Forms.Panel panHeatNum;
        private System.Windows.Forms.Panel panSteelTemp;
        private System.Windows.Forms.Panel panCarbonPercent;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.SplitContainer splitIronScrap;
        private System.Windows.Forms.TableLayoutPanel panCommonOut;
        private System.Windows.Forms.Label lblFomOut;
        private System.Windows.Forms.Label lblLime;
        private System.Windows.Forms.Label lblDolomite;
        private System.Windows.Forms.Label lblScrapOut;
        private System.Windows.Forms.Label lblIronOut;
        private System.Windows.Forms.Label lblLimeStoneOut;
        private System.Windows.Forms.Label lblCokeOut;
        private System.Windows.Forms.Button btnScrapOut;
        private System.Windows.Forms.Button btnIronOut;
        private System.Windows.Forms.Panel panScrapMass;
        private System.Windows.Forms.TextBox txbScrapOut;
        private System.Windows.Forms.Label lblScrapMass;
        private System.Windows.Forms.TextBox txbLimeStoneOut;
        private System.Windows.Forms.TextBox txbFomOut;
        private System.Windows.Forms.TextBox txtCu;
        private System.Windows.Forms.TextBox txbCokeOut;
        private System.Windows.Forms.TextBox txbDolomitOut;
        private System.Windows.Forms.TextBox txbLimeOut;
        private System.Windows.Forms.TableLayoutPanel panAuxOut;
        private System.Windows.Forms.TextBox txtSn;
        private System.Windows.Forms.TextBox txtW;
        private System.Windows.Forms.TextBox txtSb;
        private System.Windows.Forms.TextBox txtNi;
        private System.Windows.Forms.TextBox txtAs;
        private System.Windows.Forms.TextBox txtMo;
        private System.Windows.Forms.TextBox txtCo;
        private System.Windows.Forms.Label lblSb;
        private System.Windows.Forms.Label lblCu;
        private System.Windows.Forms.Label lblMo;
        private System.Windows.Forms.Label lblNi;
        private System.Windows.Forms.Label lblW;
        private System.Windows.Forms.Label lblCo;
        private System.Windows.Forms.Label lblAs;
        private System.Windows.Forms.Label lblSn;
        private System.Windows.Forms.RichTextBox rtbReport;
        private System.Windows.Forms.Panel panSteelOut;
        private System.Windows.Forms.TextBox txbSteelOut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panIronMass;
        private System.Windows.Forms.TextBox txbIronOut;
        private System.Windows.Forms.Label lblIronMass;
        private System.Windows.Forms.Label lblSteelOut;
        private System.Windows.Forms.TableLayoutPanel panSlagInput;
        private System.Windows.Forms.Label lblDolom;
        private System.Windows.Forms.Label lblVapno;
        private System.Windows.Forms.Button btnIronChem;
        private System.Windows.Forms.Button btnScrapChem;
        private System.Windows.Forms.Panel panIronTemp;
        private System.Windows.Forms.TextBox txbLimeStoneIn;
        private System.Windows.Forms.Label lblScrap;
        private System.Windows.Forms.Label lblIron;
        private System.Windows.Forms.Label lblLimeStoneIn;
        private System.Windows.Forms.Label lblFomIn;
        private System.Windows.Forms.TextBox txbFomIn;
        private System.Windows.Forms.Button btnFomChem;
        private System.Windows.Forms.Button btnCaCO3Chem;
        private System.Windows.Forms.TextBox txbLimeIn;
        private System.Windows.Forms.TextBox txbDolomIn;
        private System.Windows.Forms.Panel panScrapTemp;
        private System.Windows.Forms.Label lblScrapTemp;
        private System.Windows.Forms.TextBox txbScrapTemp;
        private System.Windows.Forms.Label lblIronTemp;
        private System.Windows.Forms.CheckBox calcDolmax;
        private System.Windows.Forms.CheckBox calcFom;
        private System.Windows.Forms.CheckBox calcVapenec;
        private System.Windows.Forms.CheckBox calcVapno;
        private System.Windows.Forms.Panel panControl;
        private System.Windows.Forms.Button btnDolomiteChem;
        private System.Windows.Forms.Button btnVapno;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTgtMgO;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblTgtFeO;
        private System.Windows.Forms.Panel panBasiticy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txbCokeIn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panTitleHeading;
        private System.Windows.Forms.Label lblTitleHeading;
        private System.Windows.Forms.Panel panAligner;
        private System.Windows.Forms.Button btnIronSel;
        private System.Windows.Forms.Button btnScrapSel;



    }
}