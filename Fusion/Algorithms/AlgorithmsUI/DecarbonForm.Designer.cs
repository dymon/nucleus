﻿namespace AlgorithmsUI
{
    partial class DecarbonForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCheck = new System.Windows.Forms.Button();
            this.txbHeatNumber = new System.Windows.Forms.TextBox();
            this.txbIronMass = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txbIronSulph = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txbIronPhos = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txbIronManga = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txbIronSilicon = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txbIronTemp = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txbIronCarbon = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txbScrapPhos = new System.Windows.Forms.TextBox();
            this.txbScrapCarbon = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txbScrapManga = new System.Windows.Forms.TextBox();
            this.txbScrapMass = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txbScrapSilicon = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txbSteelCarbon = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txbSteelMass = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txbGasCO2 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txbGasCO = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txbGasQ = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txbLanceH = new System.Windows.Forms.TextBox();
            this.txbOxyQ = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCheck
            // 
            this.btnCheck.Location = new System.Drawing.Point(312, 527);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(159, 44);
            this.btnCheck.TabIndex = 0;
            this.btnCheck.Text = "Проверить";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.button1_Click);
            // 
            // txbHeatNumber
            // 
            this.txbHeatNumber.Location = new System.Drawing.Point(12, 33);
            this.txbHeatNumber.Name = "txbHeatNumber";
            this.txbHeatNumber.Size = new System.Drawing.Size(100, 20);
            this.txbHeatNumber.TabIndex = 1;
            // 
            // txbIronMass
            // 
            this.txbIronMass.Location = new System.Drawing.Point(16, 38);
            this.txbIronMass.Name = "txbIronMass";
            this.txbIronMass.Size = new System.Drawing.Size(100, 20);
            this.txbIronMass.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "№ плавки";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Масса, кг";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txbIronSulph);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txbIronPhos);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txbIronManga);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txbIronSilicon);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txbIronTemp);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txbIronCarbon);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txbIronMass);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(15, 73);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(142, 411);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Чугун";
            // 
            // txbIronSulph
            // 
            this.txbIronSulph.Location = new System.Drawing.Point(16, 371);
            this.txbIronSulph.Name = "txbIronSulph";
            this.txbIronSulph.Size = new System.Drawing.Size(100, 20);
            this.txbIronSulph.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 355);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "Конц. серы, %";
            // 
            // txbIronPhos
            // 
            this.txbIronPhos.Location = new System.Drawing.Point(16, 320);
            this.txbIronPhos.Name = "txbIronPhos";
            this.txbIronPhos.Size = new System.Drawing.Size(100, 20);
            this.txbIronPhos.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 304);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Конц. фосфора, %";
            // 
            // txbIronManga
            // 
            this.txbIronManga.Location = new System.Drawing.Point(16, 270);
            this.txbIronManga.Name = "txbIronManga";
            this.txbIronManga.Size = new System.Drawing.Size(100, 20);
            this.txbIronManga.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 254);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Конц. марганца, %";
            // 
            // txbIronSilicon
            // 
            this.txbIronSilicon.Location = new System.Drawing.Point(16, 220);
            this.txbIronSilicon.Name = "txbIronSilicon";
            this.txbIronSilicon.Size = new System.Drawing.Size(100, 20);
            this.txbIronSilicon.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 204);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Конц. кремния, %";
            // 
            // txbIronTemp
            // 
            this.txbIronTemp.Location = new System.Drawing.Point(16, 146);
            this.txbIronTemp.Name = "txbIronTemp";
            this.txbIronTemp.Size = new System.Drawing.Size(100, 20);
            this.txbIronTemp.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Температура, С";
            // 
            // txbIronCarbon
            // 
            this.txbIronCarbon.Location = new System.Drawing.Point(16, 81);
            this.txbIronCarbon.Name = "txbIronCarbon";
            this.txbIronCarbon.Size = new System.Drawing.Size(100, 20);
            this.txbIronCarbon.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Конц. углерода, %";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txbScrapPhos);
            this.groupBox2.Controls.Add(this.txbScrapCarbon);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txbScrapManga);
            this.groupBox2.Controls.Add(this.txbScrapMass);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txbScrapSilicon);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Location = new System.Drawing.Point(172, 73);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(142, 368);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Лом";
            // 
            // txbScrapPhos
            // 
            this.txbScrapPhos.Location = new System.Drawing.Point(15, 320);
            this.txbScrapPhos.Name = "txbScrapPhos";
            this.txbScrapPhos.Size = new System.Drawing.Size(100, 20);
            this.txbScrapPhos.TabIndex = 21;
            // 
            // txbScrapCarbon
            // 
            this.txbScrapCarbon.Location = new System.Drawing.Point(16, 81);
            this.txbScrapCarbon.Name = "txbScrapCarbon";
            this.txbScrapCarbon.Size = new System.Drawing.Size(100, 20);
            this.txbScrapCarbon.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 304);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(98, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "Конц. фосфора, %";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Конц. углерода, %";
            // 
            // txbScrapManga
            // 
            this.txbScrapManga.Location = new System.Drawing.Point(15, 270);
            this.txbScrapManga.Name = "txbScrapManga";
            this.txbScrapManga.Size = new System.Drawing.Size(100, 20);
            this.txbScrapManga.TabIndex = 19;
            // 
            // txbScrapMass
            // 
            this.txbScrapMass.Location = new System.Drawing.Point(16, 38);
            this.txbScrapMass.Name = "txbScrapMass";
            this.txbScrapMass.Size = new System.Drawing.Size(100, 20);
            this.txbScrapMass.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 254);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(101, 13);
            this.label14.TabIndex = 20;
            this.label14.Text = "Конц. марганца, %";
            // 
            // txbScrapSilicon
            // 
            this.txbScrapSilicon.Location = new System.Drawing.Point(15, 220);
            this.txbScrapSilicon.Name = "txbScrapSilicon";
            this.txbScrapSilicon.Size = new System.Drawing.Size(100, 20);
            this.txbScrapSilicon.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Масса, кг";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 204);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "Конц. кремния, %";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txbSteelCarbon);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txbSteelMass);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Location = new System.Drawing.Point(329, 73);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(142, 127);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Сталь";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // txbSteelCarbon
            // 
            this.txbSteelCarbon.Location = new System.Drawing.Point(16, 81);
            this.txbSteelCarbon.Name = "txbSteelCarbon";
            this.txbSteelCarbon.Size = new System.Drawing.Size(100, 20);
            this.txbSteelCarbon.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Конц. углерода, %";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // txbSteelMass
            // 
            this.txbSteelMass.Location = new System.Drawing.Point(16, 38);
            this.txbSteelMass.Name = "txbSteelMass";
            this.txbSteelMass.Size = new System.Drawing.Size(100, 20);
            this.txbSteelMass.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Масса, кг";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txbGasCO2);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.txbGasCO);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.txbGasQ);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.txbLanceH);
            this.groupBox4.Controls.Add(this.txbOxyQ);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Location = new System.Drawing.Point(491, 71);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(349, 319);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Тренды";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // txbGasCO2
            // 
            this.txbGasCO2.Location = new System.Drawing.Point(19, 272);
            this.txbGasCO2.Name = "txbGasCO2";
            this.txbGasCO2.Size = new System.Drawing.Size(311, 20);
            this.txbGasCO2.TabIndex = 26;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(19, 256);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(255, 13);
            this.label21.TabIndex = 27;
            this.label21.Text = "Объемная доля диоксида углерода (СО2), [об. %]";
            this.label21.Click += new System.EventHandler(this.label21_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(19, 168);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(211, 13);
            this.label20.TabIndex = 25;
            this.label20.Text = "В отходящих конвертерных газах:";
            this.label20.Click += new System.EventHandler(this.label20_Click);
            // 
            // txbGasCO
            // 
            this.txbGasCO.Location = new System.Drawing.Point(19, 222);
            this.txbGasCO.Name = "txbGasCO";
            this.txbGasCO.Size = new System.Drawing.Size(311, 20);
            this.txbGasCO.TabIndex = 23;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(19, 206);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(237, 13);
            this.label19.TabIndex = 24;
            this.label19.Text = "Объемная доля оксида углерода (СО), [об. %]";
            // 
            // txbGasQ
            // 
            this.txbGasQ.Location = new System.Drawing.Point(18, 132);
            this.txbGasQ.Name = "txbGasQ";
            this.txbGasQ.Size = new System.Drawing.Size(311, 20);
            this.txbGasQ.TabIndex = 21;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(18, 116);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(255, 13);
            this.label18.TabIndex = 22;
            this.label18.Text = "Расход отходящих конвертерных газов, [м3/час]";
            // 
            // txbLanceH
            // 
            this.txbLanceH.Location = new System.Drawing.Point(18, 83);
            this.txbLanceH.Name = "txbLanceH";
            this.txbLanceH.Size = new System.Drawing.Size(311, 20);
            this.txbLanceH.TabIndex = 19;
            // 
            // txbOxyQ
            // 
            this.txbOxyQ.Location = new System.Drawing.Point(18, 40);
            this.txbOxyQ.Name = "txbOxyQ";
            this.txbOxyQ.Size = new System.Drawing.Size(311, 20);
            this.txbOxyQ.TabIndex = 17;
            this.txbOxyQ.TextChanged += new System.EventHandler(this.textBox13_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(18, 67);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(129, 13);
            this.label16.TabIndex = 20;
            this.label16.Text = "Положение фурмы, [см]";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(18, 24);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(201, 13);
            this.label17.TabIndex = 18;
            this.label17.Text = "Расход кислорода на фурму, [м3/мин]";
            // 
            // btnNext
            // 
            this.btnNext.Enabled = false;
            this.btnNext.Location = new System.Drawing.Point(491, 527);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(159, 44);
            this.btnNext.TabIndex = 10;
            this.btnNext.Text = "Далее";
            this.btnNext.UseVisualStyleBackColor = true;
            // 
            // DecarbonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(857, 625);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txbHeatNumber);
            this.Controls.Add(this.btnCheck);
            this.Name = "DecarbonForm";
            this.Text = "Входные параметры алгоритма";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.TextBox txbHeatNumber;
        private System.Windows.Forms.TextBox txbIronMass;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txbIronCarbon;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txbScrapCarbon;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txbScrapMass;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txbSteelCarbon;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txbSteelMass;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txbIronSulph;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txbIronPhos;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txbIronManga;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txbIronSilicon;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txbIronTemp;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txbScrapPhos;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txbScrapManga;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txbScrapSilicon;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txbGasCO;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txbGasQ;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txbLanceH;
        private System.Windows.Forms.TextBox txbOxyQ;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txbGasCO2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button btnNext;
    }
}

