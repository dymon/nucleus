﻿namespace Kurkin
{
    partial class KurkinForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabSimple = new System.Windows.Forms.TabControl();
            this.CSimple = new System.Windows.Forms.TabPage();
            this.btnCGas = new System.Windows.Forms.Button();
            this.btnCalcC = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnKGasan = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txbGasCO2 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txbGasCO = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txbGasQ = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txbLanceH = new System.Windows.Forms.TextBox();
            this.txbOxyQ = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxResult = new System.Windows.Forms.GroupBox();
            this.lblTime = new System.Windows.Forms.Label();
            this.pBarCarbonPercent = new System.Windows.Forms.ProgressBar();
            this.pBarGasanCarbonMass = new System.Windows.Forms.ProgressBar();
            this.txbCarbonPercent = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txbGasanCarbonMass = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txbHeatCarbonMass = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txbSteelCarbon = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txbSteelMass = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txbHeatNumber = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txbScrapPhos = new System.Windows.Forms.TextBox();
            this.txbScrapCarbon = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txbScrapManga = new System.Windows.Forms.TextBox();
            this.txbScrapMass = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txbScrapSilicon = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txbIronSulph = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txbIronPhos = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txbIronManga = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txbIronSilicon = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txbIronTemp = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txbIronCarbon = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txbIronMass = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CPoly = new System.Windows.Forms.TabPage();
            this.splitContTable_ToolBar = new System.Windows.Forms.SplitContainer();
            this.dGMatrixState = new System.Windows.Forms.DataGridView();
            this.ColIdHeat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNumberHeat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColHeightLance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColOxygenVolumeRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCarboneFact = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCarboneCalculation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lblMatrixCarbone = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txbMatrixOxigenVolumeRate = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txbMatrixLance = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txbMatrixCO = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnMatrixSave = new System.Windows.Forms.Button();
            this.btnMatrixLoad = new System.Windows.Forms.Button();
            this.btnCalcMultiFactor = new System.Windows.Forms.Button();
            this.tabSimple.SuspendLayout();
            this.CSimple.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBoxResult.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.CPoly.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContTable_ToolBar)).BeginInit();
            this.splitContTable_ToolBar.Panel1.SuspendLayout();
            this.splitContTable_ToolBar.Panel2.SuspendLayout();
            this.splitContTable_ToolBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dGMatrixState)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabSimple
            // 
            this.tabSimple.Controls.Add(this.CSimple);
            this.tabSimple.Controls.Add(this.CPoly);
            this.tabSimple.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabSimple.Location = new System.Drawing.Point(0, 0);
            this.tabSimple.Name = "tabSimple";
            this.tabSimple.SelectedIndex = 0;
            this.tabSimple.Size = new System.Drawing.Size(877, 574);
            this.tabSimple.TabIndex = 0;
            // 
            // CSimple
            // 
            this.CSimple.BackColor = System.Drawing.Color.White;
            this.CSimple.Controls.Add(this.btnCGas);
            this.CSimple.Controls.Add(this.btnCalcC);
            this.CSimple.Controls.Add(this.groupBox4);
            this.CSimple.Controls.Add(this.label1);
            this.CSimple.Controls.Add(this.groupBoxResult);
            this.CSimple.Controls.Add(this.groupBox3);
            this.CSimple.Controls.Add(this.txbHeatNumber);
            this.CSimple.Controls.Add(this.groupBox2);
            this.CSimple.Controls.Add(this.groupBox1);
            this.CSimple.Location = new System.Drawing.Point(4, 22);
            this.CSimple.Name = "CSimple";
            this.CSimple.Padding = new System.Windows.Forms.Padding(3);
            this.CSimple.Size = new System.Drawing.Size(869, 548);
            this.CSimple.TabIndex = 0;
            this.CSimple.Text = "Однофакторная модель";
            // 
            // btnCGas
            // 
            this.btnCGas.Location = new System.Drawing.Point(304, 450);
            this.btnCGas.Name = "btnCGas";
            this.btnCGas.Size = new System.Drawing.Size(147, 78);
            this.btnCGas.TabIndex = 17;
            this.btnCGas.Text = "Запустить расчет остаточного углерода";
            this.btnCGas.UseVisualStyleBackColor = true;
            this.btnCGas.Click += new System.EventHandler(this.btnCGas_Click);
            // 
            // btnCalcC
            // 
            this.btnCalcC.Location = new System.Drawing.Point(304, 407);
            this.btnCalcC.Name = "btnCalcC";
            this.btnCalcC.Size = new System.Drawing.Size(147, 36);
            this.btnCalcC.TabIndex = 16;
            this.btnCalcC.Text = "Расчитать общий C";
            this.btnCalcC.UseVisualStyleBackColor = true;
            this.btnCalcC.Click += new System.EventHandler(this.btnCalcC_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.groupBox4.Controls.Add(this.btnKGasan);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.txbGasCO2);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.txbGasCO);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.txbGasQ);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.txbLanceH);
            this.groupBox4.Controls.Add(this.txbOxyQ);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Location = new System.Drawing.Point(457, 60);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(404, 319);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Тренды";
            // 
            // btnKGasan
            // 
            this.btnKGasan.Location = new System.Drawing.Point(19, 293);
            this.btnKGasan.Name = "btnKGasan";
            this.btnKGasan.Size = new System.Drawing.Size(360, 20);
            this.btnKGasan.TabIndex = 26;
            this.btnKGasan.Text = "0,6357";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(19, 277);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(301, 13);
            this.label24.TabIndex = 27;
            this.label24.Text = "Коэффициент систематической ошибки газоанализатора";
            // 
            // txbGasCO2
            // 
            this.txbGasCO2.Location = new System.Drawing.Point(19, 251);
            this.txbGasCO2.Name = "txbGasCO2";
            this.txbGasCO2.Size = new System.Drawing.Size(360, 20);
            this.txbGasCO2.TabIndex = 26;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(19, 235);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(255, 13);
            this.label21.TabIndex = 27;
            this.label21.Text = "Объемная доля диоксида углерода (СО2), [об. %]";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(19, 168);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(211, 13);
            this.label20.TabIndex = 25;
            this.label20.Text = "В отходящих конвертерных газах:";
            // 
            // txbGasCO
            // 
            this.txbGasCO.Location = new System.Drawing.Point(19, 207);
            this.txbGasCO.Name = "txbGasCO";
            this.txbGasCO.Size = new System.Drawing.Size(360, 20);
            this.txbGasCO.TabIndex = 23;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(19, 191);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(237, 13);
            this.label19.TabIndex = 24;
            this.label19.Text = "Объемная доля оксида углерода (СО), [об. %]";
            // 
            // txbGasQ
            // 
            this.txbGasQ.Location = new System.Drawing.Point(18, 132);
            this.txbGasQ.Name = "txbGasQ";
            this.txbGasQ.Size = new System.Drawing.Size(360, 20);
            this.txbGasQ.TabIndex = 21;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(18, 116);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(255, 13);
            this.label18.TabIndex = 22;
            this.label18.Text = "Расход отходящих конвертерных газов, [м3/час]";
            // 
            // txbLanceH
            // 
            this.txbLanceH.Location = new System.Drawing.Point(18, 83);
            this.txbLanceH.Name = "txbLanceH";
            this.txbLanceH.Size = new System.Drawing.Size(360, 20);
            this.txbLanceH.TabIndex = 19;
            // 
            // txbOxyQ
            // 
            this.txbOxyQ.Location = new System.Drawing.Point(18, 40);
            this.txbOxyQ.Name = "txbOxyQ";
            this.txbOxyQ.Size = new System.Drawing.Size(360, 20);
            this.txbOxyQ.TabIndex = 17;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(18, 67);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(129, 13);
            this.label16.TabIndex = 20;
            this.label16.Text = "Положение фурмы, [см]";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(18, 24);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(201, 13);
            this.label17.TabIndex = 18;
            this.label17.Text = "Расход кислорода на фурму, [м3/мин]";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "№ плавки";
            // 
            // groupBoxResult
            // 
            this.groupBoxResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(170)))), ((int)(((byte)(150)))));
            this.groupBoxResult.Controls.Add(this.lblTime);
            this.groupBoxResult.Controls.Add(this.pBarCarbonPercent);
            this.groupBoxResult.Controls.Add(this.pBarGasanCarbonMass);
            this.groupBoxResult.Controls.Add(this.txbCarbonPercent);
            this.groupBoxResult.Controls.Add(this.label28);
            this.groupBoxResult.Controls.Add(this.txbGasanCarbonMass);
            this.groupBoxResult.Controls.Add(this.label22);
            this.groupBoxResult.Controls.Add(this.txbHeatCarbonMass);
            this.groupBoxResult.Controls.Add(this.label23);
            this.groupBoxResult.Location = new System.Drawing.Point(457, 385);
            this.groupBoxResult.Name = "groupBoxResult";
            this.groupBoxResult.Size = new System.Drawing.Size(404, 155);
            this.groupBoxResult.TabIndex = 14;
            this.groupBoxResult.TabStop = false;
            this.groupBoxResult.Text = "Результат";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTime.Location = new System.Drawing.Point(224, 38);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(49, 30);
            this.lblTime.TabIndex = 8;
            this.lblTime.Text = "t=0";
            // 
            // pBarCarbonPercent
            // 
            this.pBarCarbonPercent.BackColor = System.Drawing.Color.Blue;
            this.pBarCarbonPercent.Location = new System.Drawing.Point(142, 110);
            this.pBarCarbonPercent.Name = "pBarCarbonPercent";
            this.pBarCarbonPercent.Size = new System.Drawing.Size(256, 33);
            this.pBarCarbonPercent.TabIndex = 7;
            // 
            // pBarGasanCarbonMass
            // 
            this.pBarGasanCarbonMass.Location = new System.Drawing.Point(142, 81);
            this.pBarGasanCarbonMass.Name = "pBarGasanCarbonMass";
            this.pBarGasanCarbonMass.Size = new System.Drawing.Size(256, 23);
            this.pBarGasanCarbonMass.TabIndex = 7;
            // 
            // txbCarbonPercent
            // 
            this.txbCarbonPercent.Location = new System.Drawing.Point(18, 123);
            this.txbCarbonPercent.Name = "txbCarbonPercent";
            this.txbCarbonPercent.Size = new System.Drawing.Size(118, 20);
            this.txbCarbonPercent.TabIndex = 5;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(18, 107);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(99, 13);
            this.label28.TabIndex = 6;
            this.label28.Text = "Процент углерода";
            // 
            // txbGasanCarbonMass
            // 
            this.txbGasanCarbonMass.Location = new System.Drawing.Point(16, 81);
            this.txbGasanCarbonMass.Name = "txbGasanCarbonMass";
            this.txbGasanCarbonMass.Size = new System.Drawing.Size(120, 20);
            this.txbGasanCarbonMass.TabIndex = 5;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(16, 65);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(111, 13);
            this.label22.TabIndex = 6;
            this.label22.Text = "Остаточный углерод";
            // 
            // txbHeatCarbonMass
            // 
            this.txbHeatCarbonMass.Location = new System.Drawing.Point(16, 38);
            this.txbHeatCarbonMass.Name = "txbHeatCarbonMass";
            this.txbHeatCarbonMass.Size = new System.Drawing.Size(120, 20);
            this.txbHeatCarbonMass.TabIndex = 2;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(16, 22);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(186, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "Масса углерода по данным плавки";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(255)))));
            this.groupBox3.Controls.Add(this.txbSteelCarbon);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txbSteelMass);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Location = new System.Drawing.Point(308, 62);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(142, 127);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Сталь";
            // 
            // txbSteelCarbon
            // 
            this.txbSteelCarbon.Location = new System.Drawing.Point(16, 81);
            this.txbSteelCarbon.Name = "txbSteelCarbon";
            this.txbSteelCarbon.Size = new System.Drawing.Size(100, 20);
            this.txbSteelCarbon.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Конц. углерода, %";
            // 
            // txbSteelMass
            // 
            this.txbSteelMass.Location = new System.Drawing.Point(16, 38);
            this.txbSteelMass.Name = "txbSteelMass";
            this.txbSteelMass.Size = new System.Drawing.Size(100, 20);
            this.txbSteelMass.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Масса, кг";
            // 
            // txbHeatNumber
            // 
            this.txbHeatNumber.Location = new System.Drawing.Point(8, 22);
            this.txbHeatNumber.Name = "txbHeatNumber";
            this.txbHeatNumber.Size = new System.Drawing.Size(100, 20);
            this.txbHeatNumber.TabIndex = 10;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(175)))), ((int)(((byte)(255)))));
            this.groupBox2.Controls.Add(this.txbScrapPhos);
            this.groupBox2.Controls.Add(this.txbScrapCarbon);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txbScrapManga);
            this.groupBox2.Controls.Add(this.txbScrapMass);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txbScrapSilicon);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Location = new System.Drawing.Point(159, 62);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(142, 267);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Лом";
            // 
            // txbScrapPhos
            // 
            this.txbScrapPhos.Location = new System.Drawing.Point(16, 223);
            this.txbScrapPhos.Name = "txbScrapPhos";
            this.txbScrapPhos.Size = new System.Drawing.Size(100, 20);
            this.txbScrapPhos.TabIndex = 21;
            // 
            // txbScrapCarbon
            // 
            this.txbScrapCarbon.Location = new System.Drawing.Point(16, 81);
            this.txbScrapCarbon.Name = "txbScrapCarbon";
            this.txbScrapCarbon.Size = new System.Drawing.Size(100, 20);
            this.txbScrapCarbon.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 207);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(98, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "Конц. фосфора, %";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Конц. углерода, %";
            // 
            // txbScrapManga
            // 
            this.txbScrapManga.Location = new System.Drawing.Point(16, 173);
            this.txbScrapManga.Name = "txbScrapManga";
            this.txbScrapManga.Size = new System.Drawing.Size(100, 20);
            this.txbScrapManga.TabIndex = 19;
            // 
            // txbScrapMass
            // 
            this.txbScrapMass.Location = new System.Drawing.Point(16, 38);
            this.txbScrapMass.Name = "txbScrapMass";
            this.txbScrapMass.Size = new System.Drawing.Size(100, 20);
            this.txbScrapMass.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(16, 157);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(101, 13);
            this.label14.TabIndex = 20;
            this.label14.Text = "Конц. марганца, %";
            // 
            // txbScrapSilicon
            // 
            this.txbScrapSilicon.Location = new System.Drawing.Point(16, 123);
            this.txbScrapSilicon.Name = "txbScrapSilicon";
            this.txbScrapSilicon.Size = new System.Drawing.Size(100, 20);
            this.txbScrapSilicon.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Масса, кг";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 107);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "Конц. кремния, %";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            this.groupBox1.Controls.Add(this.txbIronSulph);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txbIronPhos);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txbIronManga);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txbIronSilicon);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txbIronTemp);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txbIronCarbon);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txbIronMass);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(11, 62);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(142, 358);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Чугун";
            // 
            // txbIronSulph
            // 
            this.txbIronSulph.Location = new System.Drawing.Point(16, 314);
            this.txbIronSulph.Name = "txbIronSulph";
            this.txbIronSulph.Size = new System.Drawing.Size(100, 20);
            this.txbIronSulph.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 298);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "Конц. серы, %";
            // 
            // txbIronPhos
            // 
            this.txbIronPhos.Location = new System.Drawing.Point(16, 266);
            this.txbIronPhos.Name = "txbIronPhos";
            this.txbIronPhos.Size = new System.Drawing.Size(100, 20);
            this.txbIronPhos.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 250);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Конц. фосфора, %";
            // 
            // txbIronManga
            // 
            this.txbIronManga.Location = new System.Drawing.Point(16, 216);
            this.txbIronManga.Name = "txbIronManga";
            this.txbIronManga.Size = new System.Drawing.Size(100, 20);
            this.txbIronManga.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 200);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Конц. марганца, %";
            // 
            // txbIronSilicon
            // 
            this.txbIronSilicon.Location = new System.Drawing.Point(16, 166);
            this.txbIronSilicon.Name = "txbIronSilicon";
            this.txbIronSilicon.Size = new System.Drawing.Size(100, 20);
            this.txbIronSilicon.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 150);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Конц. кремния, %";
            // 
            // txbIronTemp
            // 
            this.txbIronTemp.Location = new System.Drawing.Point(16, 123);
            this.txbIronTemp.Name = "txbIronTemp";
            this.txbIronTemp.Size = new System.Drawing.Size(100, 20);
            this.txbIronTemp.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 107);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Температура, С";
            // 
            // txbIronCarbon
            // 
            this.txbIronCarbon.Location = new System.Drawing.Point(16, 81);
            this.txbIronCarbon.Name = "txbIronCarbon";
            this.txbIronCarbon.Size = new System.Drawing.Size(100, 20);
            this.txbIronCarbon.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Конц. углерода, %";
            // 
            // txbIronMass
            // 
            this.txbIronMass.Location = new System.Drawing.Point(16, 38);
            this.txbIronMass.Name = "txbIronMass";
            this.txbIronMass.Size = new System.Drawing.Size(100, 20);
            this.txbIronMass.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Масса, кг";
            // 
            // CPoly
            // 
            this.CPoly.Controls.Add(this.splitContTable_ToolBar);
            this.CPoly.Location = new System.Drawing.Point(4, 22);
            this.CPoly.Name = "CPoly";
            this.CPoly.Padding = new System.Windows.Forms.Padding(3);
            this.CPoly.Size = new System.Drawing.Size(869, 548);
            this.CPoly.TabIndex = 1;
            this.CPoly.Text = "Многофакторная модель";
            this.CPoly.UseVisualStyleBackColor = true;
            // 
            // splitContTable_ToolBar
            // 
            this.splitContTable_ToolBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContTable_ToolBar.Location = new System.Drawing.Point(3, 3);
            this.splitContTable_ToolBar.Name = "splitContTable_ToolBar";
            this.splitContTable_ToolBar.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContTable_ToolBar.Panel1
            // 
            this.splitContTable_ToolBar.Panel1.Controls.Add(this.dGMatrixState);
            // 
            // splitContTable_ToolBar.Panel2
            // 
            this.splitContTable_ToolBar.Panel2.Controls.Add(this.groupBox7);
            this.splitContTable_ToolBar.Panel2.Controls.Add(this.groupBox6);
            this.splitContTable_ToolBar.Panel2.Controls.Add(this.groupBox5);
            this.splitContTable_ToolBar.Panel2.Controls.Add(this.btnCalcMultiFactor);
            this.splitContTable_ToolBar.Size = new System.Drawing.Size(863, 542);
            this.splitContTable_ToolBar.SplitterDistance = 438;
            this.splitContTable_ToolBar.TabIndex = 0;
            // 
            // dGMatrixState
            // 
            this.dGMatrixState.AllowUserToAddRows = false;
            this.dGMatrixState.AllowUserToDeleteRows = false;
            this.dGMatrixState.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGMatrixState.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColIdHeat,
            this.ColNumberHeat,
            this.ColCO,
            this.ColHeightLance,
            this.ColOxygenVolumeRate,
            this.ColCarboneFact,
            this.ColCarboneCalculation});
            this.dGMatrixState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dGMatrixState.Location = new System.Drawing.Point(0, 0);
            this.dGMatrixState.Name = "dGMatrixState";
            this.dGMatrixState.Size = new System.Drawing.Size(863, 438);
            this.dGMatrixState.TabIndex = 0;
            this.dGMatrixState.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dGMatrixState_CellValueChanged);
            // 
            // ColIdHeat
            // 
            this.ColIdHeat.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColIdHeat.HeaderText = "№п/п";
            this.ColIdHeat.Name = "ColIdHeat";
            // 
            // ColNumberHeat
            // 
            this.ColNumberHeat.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColNumberHeat.HeaderText = "№ пл";
            this.ColNumberHeat.Name = "ColNumberHeat";
            // 
            // ColCO
            // 
            this.ColCO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColCO.HeaderText = "CO,%";
            this.ColCO.Name = "ColCO";
            // 
            // ColHeightLance
            // 
            this.ColHeightLance.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColHeightLance.HeaderText = "H фурмы, см";
            this.ColHeightLance.Name = "ColHeightLance";
            // 
            // ColOxygenVolumeRate
            // 
            this.ColOxygenVolumeRate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColOxygenVolumeRate.HeaderText = "Интенс, н.м³/мин";
            this.ColOxygenVolumeRate.Name = "ColOxygenVolumeRate";
            // 
            // ColCarboneFact
            // 
            this.ColCarboneFact.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColCarboneFact.HeaderText = "Сфакт, %";
            this.ColCarboneFact.Name = "ColCarboneFact";
            // 
            // ColCarboneCalculation
            // 
            this.ColCarboneCalculation.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColCarboneCalculation.HeaderText = "Cрасч, %";
            this.ColCarboneCalculation.Name = "ColCarboneCalculation";
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(170)))), ((int)(((byte)(150)))));
            this.groupBox7.Controls.Add(this.lblMatrixCarbone);
            this.groupBox7.Location = new System.Drawing.Point(554, 7);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(224, 83);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Расчитанный углерод в %";
            // 
            // lblMatrixCarbone
            // 
            this.lblMatrixCarbone.AutoSize = true;
            this.lblMatrixCarbone.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblMatrixCarbone.Location = new System.Drawing.Point(51, 30);
            this.lblMatrixCarbone.Name = "lblMatrixCarbone";
            this.lblMatrixCarbone.Size = new System.Drawing.Size(139, 30);
            this.lblMatrixCarbone.TabIndex = 9;
            this.lblMatrixCarbone.Text = "░▒▓█▓▒░";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.groupBox6.Controls.Add(this.txbMatrixOxigenVolumeRate);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.txbMatrixLance);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.txbMatrixCO);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Location = new System.Drawing.Point(223, 7);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(371, 83);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Исходные данные для расчета";
            // 
            // txbMatrixOxigenVolumeRate
            // 
            this.txbMatrixOxigenVolumeRate.Location = new System.Drawing.Point(223, 39);
            this.txbMatrixOxigenVolumeRate.Name = "txbMatrixOxigenVolumeRate";
            this.txbMatrixOxigenVolumeRate.Size = new System.Drawing.Size(100, 20);
            this.txbMatrixOxigenVolumeRate.TabIndex = 21;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(223, 23);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(92, 13);
            this.label27.TabIndex = 22;
            this.label27.Text = "Интенс н.м³/мин";
            // 
            // txbMatrixLance
            // 
            this.txbMatrixLance.Location = new System.Drawing.Point(117, 39);
            this.txbMatrixLance.Name = "txbMatrixLance";
            this.txbMatrixLance.Size = new System.Drawing.Size(100, 20);
            this.txbMatrixLance.TabIndex = 21;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(117, 23);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(73, 13);
            this.label26.TabIndex = 22;
            this.label26.Text = "H фурмы, см";
            // 
            // txbMatrixCO
            // 
            this.txbMatrixCO.Location = new System.Drawing.Point(11, 39);
            this.txbMatrixCO.Name = "txbMatrixCO";
            this.txbMatrixCO.Size = new System.Drawing.Size(100, 20);
            this.txbMatrixCO.TabIndex = 21;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(11, 23);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(36, 13);
            this.label25.TabIndex = 22;
            this.label25.Text = "CO, %";
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(255)))), ((int)(((byte)(200)))));
            this.groupBox5.Controls.Add(this.btnMatrixSave);
            this.groupBox5.Controls.Add(this.btnMatrixLoad);
            this.groupBox5.Location = new System.Drawing.Point(5, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(218, 84);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Матрица состояний";
            // 
            // btnMatrixSave
            // 
            this.btnMatrixSave.Enabled = false;
            this.btnMatrixSave.Location = new System.Drawing.Point(110, 19);
            this.btnMatrixSave.Name = "btnMatrixSave";
            this.btnMatrixSave.Size = new System.Drawing.Size(88, 59);
            this.btnMatrixSave.TabIndex = 1;
            this.btnMatrixSave.Text = "Сохранить";
            this.btnMatrixSave.UseVisualStyleBackColor = true;
            // 
            // btnMatrixLoad
            // 
            this.btnMatrixLoad.Location = new System.Drawing.Point(15, 19);
            this.btnMatrixLoad.Name = "btnMatrixLoad";
            this.btnMatrixLoad.Size = new System.Drawing.Size(89, 59);
            this.btnMatrixLoad.TabIndex = 0;
            this.btnMatrixLoad.Text = "Загрузить";
            this.btnMatrixLoad.UseVisualStyleBackColor = true;
            this.btnMatrixLoad.Click += new System.EventHandler(this.btnMatrixLoad_Click);
            // 
            // btnCalcMultiFactor
            // 
            this.btnCalcMultiFactor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCalcMultiFactor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCalcMultiFactor.Location = new System.Drawing.Point(784, 9);
            this.btnCalcMultiFactor.Name = "btnCalcMultiFactor";
            this.btnCalcMultiFactor.Size = new System.Drawing.Size(74, 81);
            this.btnCalcMultiFactor.TabIndex = 0;
            this.btnCalcMultiFactor.Text = "Расчет";
            this.btnCalcMultiFactor.UseVisualStyleBackColor = true;
            this.btnCalcMultiFactor.Click += new System.EventHandler(this.btnCalcMultiFactor_Click);
            // 
            // KurkinForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 574);
            this.Controls.Add(this.tabSimple);
            this.Name = "KurkinForm";
            this.Text = "Алгоритм С";
            this.tabSimple.ResumeLayout(false);
            this.CSimple.ResumeLayout(false);
            this.CSimple.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBoxResult.ResumeLayout(false);
            this.groupBoxResult.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.CPoly.ResumeLayout(false);
            this.splitContTable_ToolBar.Panel1.ResumeLayout(false);
            this.splitContTable_ToolBar.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContTable_ToolBar)).EndInit();
            this.splitContTable_ToolBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dGMatrixState)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabSimple;
        private System.Windows.Forms.TabPage CSimple;
        private System.Windows.Forms.TabPage CPoly;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txbGasCO2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txbGasCO;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txbGasQ;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txbLanceH;
        private System.Windows.Forms.TextBox txbOxyQ;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txbSteelCarbon;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txbSteelMass;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txbHeatNumber;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txbScrapPhos;
        private System.Windows.Forms.TextBox txbScrapCarbon;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txbScrapManga;
        private System.Windows.Forms.TextBox txbScrapMass;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txbScrapSilicon;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txbIronSulph;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txbIronPhos;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txbIronManga;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txbIronSilicon;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txbIronTemp;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txbIronCarbon;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txbIronMass;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCalcC;
        private System.Windows.Forms.GroupBox groupBoxResult;
        private System.Windows.Forms.TextBox txbGasanCarbonMass;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txbHeatCarbonMass;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button btnCGas;
        private System.Windows.Forms.TextBox btnKGasan;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ProgressBar pBarGasanCarbonMass;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.SplitContainer splitContTable_ToolBar;
        private System.Windows.Forms.DataGridView dGMatrixState;
        private System.Windows.Forms.Button btnCalcMultiFactor;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnMatrixSave;
        private System.Windows.Forms.Button btnMatrixLoad;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColIdHeat;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNumberHeat;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColHeightLance;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColOxygenVolumeRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCarboneFact;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCarboneCalculation;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label lblMatrixCarbone;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txbMatrixOxigenVolumeRate;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txbMatrixLance;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txbMatrixCO;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txbCarbonPercent;
        private System.Windows.Forms.Label label28;
        public System.Windows.Forms.ProgressBar pBarCarbonPercent;
    }
}

