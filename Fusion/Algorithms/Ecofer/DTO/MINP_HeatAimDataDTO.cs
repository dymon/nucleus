// Generated with EntitiesToDTOs.v2.1 (entitiestodtos.codeplex.com).
// Timestamp: 29.6.2012 - 0:15:01
using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public partial class MINP_HeatAimDataDTO
    {
        public Guid ID { get; set; }

        public DateTime C__Created { get; set; }

        public Guid MINP_GD_SteelGradeID { get; set; }

        public Nullable<Int32> HeatID { get; set; }

        public String HeatNumber { get; set; }

        public Int32 FinalTemperature { get; set; }

        public Double FinalC_p { get; set; }

        public Nullable<Int32> HotMetal_t { get; set; }

        public Nullable<Int32> Scrap_t { get; set; }

        public Nullable<Int32> Total_t { get; set; }

        public Nullable<Int32> HotMetal_Temperature { get; set; }

        public Nullable<Guid> HotMetal1_MINP_GD_Material { get; set; }

        public Int32 HotMetal1_t { get; set; }

        public Nullable<Guid> HotMetal2_MINP_GD_Material { get; set; }

        public Int32 HotMetal2_t { get; set; }

        public Nullable<Guid> HotMetal3_MINP_GD_Material { get; set; }

        public Int32 HotMetal3_t { get; set; }

        public Double Basicity { get; set; }

        public Int32 Lime_kg { get; set; }

        public Int32 Dolomit_kg { get; set; }

        public Int32 FOM_kg { get; set; }

        public Int32 S1_kg { get; set; }

        public Int32 S2_kg { get; set; }

        public Int32 Coke_kg { get; set; }

        public Nullable<Int32> Scrap_Temperature { get; set; }

        public Nullable<Guid> Scrap1_MINP_GD_Material { get; set; }

        public Int32 Scrap1_t { get; set; }

        public Nullable<Guid> Scrap2_MINP_GD_Material { get; set; }

        public Int32 Scrap2_t { get; set; }

        public Nullable<Guid> Scrap3_MINP_GD_Material { get; set; }

        public Int32 Scrap3_t { get; set; }

        public Nullable<Guid> Scrap4_MINP_GD_Material { get; set; }

        public Int32 Scrap4_t { get; set; }

        public Nullable<Guid> Scrap5_MINP_GD_Material { get; set; }

        public Int32 Scrap5_t { get; set; }

        public Nullable<Guid> Scrap6_MINP_GD_Material { get; set; }

        public Int32 Scrap6_t { get; set; }

        public Nullable<Guid> Scrap7_MINP_GD_Material { get; set; }

        public Int32 Scrap7_t { get; set; }

        public Nullable<Guid> Scrap8_MINP_GD_Material { get; set; }

        public Int32 Scrap8_t { get; set; }

        public Nullable<Guid> Scrap9_MINP_GD_Material { get; set; }

        public Int32 Scrap9_t { get; set; }

        public Nullable<Guid> Scrap10_MINP_GD_Material { get; set; }

        public Int32 Scrap10_t { get; set; }

        public Nullable<Guid> Scrap11_MINP_GD_Material { get; set; }

        public Int32 Scrap11_t { get; set; }

        public Nullable<Guid> Scrap12_MINP_GD_Material { get; set; }

        public Int32 Scrap12_t { get; set; }

        public Int32 State { get; set; }

        public Int32 FeO_p { get; set; }

        public Int32 MgO_p { get; set; }

        public MINP_GD_MaterialDTO MINP_GD_MaterialHM1 { get; set; }

        public MINP_GD_MaterialDTO MINP_GD_MaterialHM2 { get; set; }

        public MINP_GD_MaterialDTO MINP_GD_MaterialHM3 { get; set; }

        public MINP_GD_MaterialDTO MINP_GD_MaterialS1 { get; set; }

        public MINP_GD_MaterialDTO MINP_GD_MaterialS10 { get; set; }

        public MINP_GD_MaterialDTO MINP_GD_MaterialS11 { get; set; }

        public MINP_GD_MaterialDTO MINP_GD_MaterialS12 { get; set; }

        public MINP_GD_MaterialDTO MINP_GD_MaterialS2 { get; set; }

        public MINP_GD_MaterialDTO MINP_GD_MaterialS3 { get; set; }

        public MINP_GD_MaterialDTO MINP_GD_MaterialS4 { get; set; }

        public MINP_GD_MaterialDTO MINP_GD_MaterialS5 { get; set; }

        public MINP_GD_MaterialDTO MINP_GD_MaterialS6 { get; set; }

        public MINP_GD_MaterialDTO MINP_GD_MaterialS7 { get; set; }

        public MINP_GD_MaterialDTO MINP_GD_MaterialS8 { get; set; }

        public MINP_GD_MaterialDTO MINP_GD_MaterialS9 { get; set; }

        public MINP_GD_SteelGradeDTO MINP_GD_SteelGrade { get; set; }
    }
}
