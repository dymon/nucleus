// Generated with EntitiesToDTOs.v2.1 (entitiestodtos.codeplex.com).
// Timestamp: 29.6.2012 - 0:14:52
using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public partial class MISC_WatchdogDTO
    {
        public String Code { get; set; }

        public Int32 Counter { get; set; }
    }
}
