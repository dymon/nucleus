// Generated with EntitiesToDTOs.v2.1 (entitiestodtos.codeplex.com).
// Timestamp: 29.6.2012 - 0:14:44
using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public partial class LAL2_SlagAnalysisDTO
    {
        public Guid ID { get; set; }

        public DateTime C__Created { get; set; }

        public String HeatNumber { get; set; }

        public String SampleCode { get; set; }

        public Nullable<Double> CaO { get; set; }

        public Nullable<Double> SiO2 { get; set; }

        public Nullable<Double> Cr2O3 { get; set; }

        public Nullable<Double> MnO { get; set; }

        public Nullable<Double> NiO { get; set; }

        public Nullable<Double> P2O5 { get; set; }

        public Nullable<Double> S { get; set; }

        public Nullable<Double> TiO2 { get; set; }

        public Nullable<Double> TiFeO { get; set; }

        public Nullable<Double> CaF2 { get; set; }

        public Nullable<Double> Fe { get; set; }

        public Nullable<Double> FeO { get; set; }

        public Nullable<Double> Al2O3 { get; set; }

        public Nullable<Double> MgO { get; set; }

        public Nullable<Double> CaC2 { get; set; }
    }
}
