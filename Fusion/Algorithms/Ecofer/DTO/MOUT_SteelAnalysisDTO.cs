// Generated with EntitiesToDTOs.v2.1 (entitiestodtos.codeplex.com).
// Timestamp: 29.6.2012 - 0:14:55
using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public partial class MOUT_SteelAnalysisDTO
    {
        public Guid ID { get; set; }

        public DateTime C__Created { get; set; }

        public Boolean Considered { get; set; }

        public Guid MINP_HeatID { get; set; }

        public Int32 SteelWeight_kg { get; set; }

        public Nullable<Double> C { get; set; }

        public Nullable<Double> Si { get; set; }

        public Nullable<Double> Mn { get; set; }

        public Nullable<Double> P { get; set; }

        public Nullable<Double> S { get; set; }

        public Nullable<Double> Al { get; set; }

        public Nullable<Double> Cu { get; set; }

        public Nullable<Double> Cr { get; set; }

        public Nullable<Double> Mo { get; set; }

        public Nullable<Double> Ni { get; set; }

        public Nullable<Double> V { get; set; }

        public Nullable<Double> Ti { get; set; }

        public Nullable<Double> Nb { get; set; }

        public Nullable<Double> Ca { get; set; }

        public Nullable<Double> Co { get; set; }

        public Nullable<Double> Pb { get; set; }

        public Nullable<Double> W { get; set; }

        public Nullable<Double> Mg { get; set; }

        public Nullable<Double> Ce { get; set; }

        public Nullable<Double> B { get; set; }

        public Nullable<Double> As_ { get; set; }

        public Nullable<Double> Sn { get; set; }

        public Nullable<Double> Bi { get; set; }

        public Nullable<Double> Sb { get; set; }

        public Nullable<Double> Zn { get; set; }

        public Nullable<Double> Ta { get; set; }

        public Nullable<Double> Zr { get; set; }

        public Nullable<Double> Se { get; set; }

        public Nullable<Double> O { get; set; }

        public Nullable<Double> N { get; set; }

        public Nullable<Double> H { get; set; }

        public Nullable<Double> La { get; set; }

        public Nullable<Double> Fe { get; set; }

        public MINP_HeatDTO MINP_Heat { get; set; }
    }
}
