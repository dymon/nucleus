﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.269
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Common.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resource_BL {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resource_BL() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Common.Properties.Resource.BL", typeof(Resource_BL).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Process pattern is not defined correctly. Heat announce cannot continue..
        /// </summary>
        public static string ProcessPattern_InValid {
            get {
                return ResourceManager.GetString("ProcessPattern_InValid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pattern {0} : O2 amounts are defined non-increasing..
        /// </summary>
        public static string ProcessPattern_IsValid1 {
            get {
                return ResourceManager.GetString("ProcessPattern_IsValid1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pattern {0} : Rows with empty O2 amounts (calculated by the model) must be at the end of oxygen blowing..
        /// </summary>
        public static string ProcessPattern_IsValid2 {
            get {
                return ResourceManager.GetString("ProcessPattern_IsValid2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pattern {0} : Correction phase is missing or defined more than once..
        /// </summary>
        public static string ProcessPattern_IsValid3 {
            get {
                return ResourceManager.GetString("ProcessPattern_IsValid3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pattern {0} : O2 amount in correction phase must be empty (calculated by the model)..
        /// </summary>
        public static string ProcessPattern_IsValid4 {
            get {
                return ResourceManager.GetString("ProcessPattern_IsValid4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pattern {0} : No rows defined for main oxygen blowing..
        /// </summary>
        public static string ProcessPattern_IsValid5 {
            get {
                return ResourceManager.GetString("ProcessPattern_IsValid5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pattern {0} : Empty O2 amount is missing in main oxygen blowing for model calculation..
        /// </summary>
        public static string ProcessPattern_IsValid6 {
            get {
                return ResourceManager.GetString("ProcessPattern_IsValid6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pattern {0} : Sum of any of slag formers does not meet requirement 100%..
        /// </summary>
        public static string ProcessPattern_IsValidSlag {
            get {
                return ResourceManager.GetString("ProcessPattern_IsValidSlag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Continue?.
        /// </summary>
        public static string ProcessPattern_Validation_Continue {
            get {
                return ResourceManager.GetString("ProcessPattern_Validation_Continue", resourceCulture);
            }
        }
    }
}
