﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.269
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Common.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resource_Enumerations {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resource_Enumerations() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Common.Properties.Resource.Enumerations", typeof(Resource_Enumerations).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CaCO3.
        /// </summary>
        public static string MINP_GD_Material_ModelMaterial_CaCO3 {
            get {
                return ResourceManager.GetString("MINP_GD_Material_ModelMaterial_CaCO3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CaO.
        /// </summary>
        public static string MINP_GD_Material_ModelMaterial_CaO {
            get {
                return ResourceManager.GetString("MINP_GD_Material_ModelMaterial_CaO", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Coke.
        /// </summary>
        public static string MINP_GD_Material_ModelMaterial_Coke {
            get {
                return ResourceManager.GetString("MINP_GD_Material_ModelMaterial_Coke", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dolomite.
        /// </summary>
        public static string MINP_GD_Material_ModelMaterial_Dolomite {
            get {
                return ResourceManager.GetString("MINP_GD_Material_ModelMaterial_Dolomite", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to FOM.
        /// </summary>
        public static string MINP_GD_Material_ModelMaterial_FOM {
            get {
                return ResourceManager.GetString("MINP_GD_Material_ModelMaterial_FOM", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Odprasky.
        /// </summary>
        public static string MINP_GD_Material_ModelMaterial_Odprasky {
            get {
                return ResourceManager.GetString("MINP_GD_Material_ModelMaterial_Odprasky", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Slag.
        /// </summary>
        public static string MINP_GD_Material_ModelMaterial_Slag {
            get {
                return ResourceManager.GetString("MINP_GD_Material_ModelMaterial_Slag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Slag Former 1.
        /// </summary>
        public static string MINP_GD_Material_ModelMaterial_SlagFormer1 {
            get {
                return ResourceManager.GetString("MINP_GD_Material_ModelMaterial_SlagFormer1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Slag Former 2.
        /// </summary>
        public static string MINP_GD_Material_ModelMaterial_SlagFormer2 {
            get {
                return ResourceManager.GetString("MINP_GD_Material_ModelMaterial_SlagFormer2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Steel.
        /// </summary>
        public static string MINP_GD_Material_ModelMaterial_Steel {
            get {
                return ResourceManager.GetString("MINP_GD_Material_ModelMaterial_Steel", resourceCulture);
            }
        }
    }
}
