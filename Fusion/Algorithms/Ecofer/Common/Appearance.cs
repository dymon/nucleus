﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Common
{
    public static class Appearance
    {
        public static Color PhaseHighLightColor = Color.LightGreen;
        public static Color PhaseConfirmationColor = Color.FromArgb(0xF6, 0xAE, 0x74);
    }
}
