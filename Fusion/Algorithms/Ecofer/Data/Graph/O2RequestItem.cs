﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data.Graph
{
    public class O2RequestItem
    {
        public DateTime TimeProcessed { get; set; }
        public double O2Request { get; set; }
    }
}
