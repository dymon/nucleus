﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using ConnectionProvider;
using Converter;
using Implements;
using System.IO;

namespace Charge5 {
    internal partial class Program {
        private static void Main(string[] args) {
            Init();
            Console.WriteLine("Charge5 is running, press enter to exit");
            Console.ReadLine();
        }
    }
}