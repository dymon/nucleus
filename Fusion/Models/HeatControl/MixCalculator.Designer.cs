﻿namespace HeatControl
{
    partial class MixCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.heading = new System.Windows.Forms.FlowLayoutPanel();
            this.panTitleHeading = new System.Windows.Forms.Panel();
            this.lblTitleHeading = new System.Windows.Forms.Label();
            this.panHeatNum = new System.Windows.Forms.Panel();
            this.txbHeatNum = new System.Windows.Forms.TextBox();
            this.lblHeatNum = new System.Windows.Forms.Label();
            this.panSteelBrand = new System.Windows.Forms.Panel();
            this.cmbSteelBrand = new System.Windows.Forms.ComboBox();
            this.lblSteelBrand = new System.Windows.Forms.Label();
            this.panelSteelGroup = new System.Windows.Forms.Panel();
            this.cmbSteelGroup = new System.Windows.Forms.ComboBox();
            this.lblSteelGroup = new System.Windows.Forms.Label();
            this.panSteelTemp = new System.Windows.Forms.Panel();
            this.txbSteelTemp = new System.Windows.Forms.TextBox();
            this.lblSteelTemp = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblTgtMgO = new System.Windows.Forms.Label();
            this.txbMgO = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblTgtFeO = new System.Windows.Forms.Label();
            this.txbFeO = new System.Windows.Forms.TextBox();
            this.panBasiticy = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.txbBasiticy = new System.Windows.Forms.TextBox();
            this.panInput = new System.Windows.Forms.TableLayoutPanel();
            this.panIronTask = new System.Windows.Forms.Panel();
            this.txbIronTask = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panIronMass = new System.Windows.Forms.Panel();
            this.txbIronCalc = new System.Windows.Forms.TextBox();
            this.lblIronMass = new System.Windows.Forms.Label();
            this.panScrapTemp = new System.Windows.Forms.Panel();
            this.lblScrapTemp = new System.Windows.Forms.Label();
            this.txbScrapTemp = new System.Windows.Forms.TextBox();
            this.btnIronChem = new System.Windows.Forms.Button();
            this.btnScrapChem = new System.Windows.Forms.Button();
            this.lblScrap = new System.Windows.Forms.Label();
            this.lblIron = new System.Windows.Forms.Label();
            this.panIronTemp = new System.Windows.Forms.Panel();
            this.lblIronTemp = new System.Windows.Forms.Label();
            this.txbIronTemp = new System.Windows.Forms.TextBox();
            this.btnIronSel = new System.Windows.Forms.Button();
            this.btnScrapSel = new System.Windows.Forms.Button();
            this.lblSteel = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txbScrapOut = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txbSteelOut = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnApprove = new System.Windows.Forms.Button();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.rtbConvState = new System.Windows.Forms.RichTextBox();
            this.central = new System.Windows.Forms.SplitContainer();
            this.heats = new System.Windows.Forms.DataGridView();
            this.heatNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heatKind = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heatMark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ironWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ironTemp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heatPSi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scrapWait = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.steelWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panSteelTask = new System.Windows.Forms.Panel();
            this.txbSteelTask = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panUNRS = new System.Windows.Forms.Panel();
            this.cmbUNRS = new System.Windows.Forms.ComboBox();
            this.lblUNRS = new System.Windows.Forms.Label();
            this.heading.SuspendLayout();
            this.panTitleHeading.SuspendLayout();
            this.panHeatNum.SuspendLayout();
            this.panSteelBrand.SuspendLayout();
            this.panelSteelGroup.SuspendLayout();
            this.panSteelTemp.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panBasiticy.SuspendLayout();
            this.panInput.SuspendLayout();
            this.panIronTask.SuspendLayout();
            this.panIronMass.SuspendLayout();
            this.panScrapTemp.SuspendLayout();
            this.panIronTemp.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.central)).BeginInit();
            this.central.Panel1.SuspendLayout();
            this.central.Panel2.SuspendLayout();
            this.central.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.heats)).BeginInit();
            this.panSteelTask.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panUNRS.SuspendLayout();
            this.SuspendLayout();
            // 
            // heading
            // 
            this.heading.BackColor = System.Drawing.Color.LightSalmon;
            this.heading.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.heading.Controls.Add(this.panTitleHeading);
            this.heading.Controls.Add(this.panHeatNum);
            this.heading.Controls.Add(this.panUNRS);
            this.heading.Controls.Add(this.panelSteelGroup);
            this.heading.Controls.Add(this.panSteelBrand);
            this.heading.Controls.Add(this.panSteelTemp);
            this.heading.Controls.Add(this.panel1);
            this.heading.Controls.Add(this.panel2);
            this.heading.Controls.Add(this.panBasiticy);
            this.heading.Dock = System.Windows.Forms.DockStyle.Top;
            this.heading.Location = new System.Drawing.Point(0, 0);
            this.heading.Name = "heading";
            this.heading.Size = new System.Drawing.Size(732, 158);
            this.heading.TabIndex = 1;
            // 
            // panTitleHeading
            // 
            this.panTitleHeading.Controls.Add(this.lblTitleHeading);
            this.panTitleHeading.Location = new System.Drawing.Point(3, 3);
            this.panTitleHeading.Name = "panTitleHeading";
            this.panTitleHeading.Size = new System.Drawing.Size(365, 43);
            this.panTitleHeading.TabIndex = 29;
            // 
            // lblTitleHeading
            // 
            this.lblTitleHeading.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitleHeading.AutoSize = true;
            this.lblTitleHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTitleHeading.Location = new System.Drawing.Point(14, 11);
            this.lblTitleHeading.Name = "lblTitleHeading";
            this.lblTitleHeading.Size = new System.Drawing.Size(330, 24);
            this.lblTitleHeading.TabIndex = 28;
            this.lblTitleHeading.Text = "Конвертер №X плавка YYYYYYY";
            // 
            // panHeatNum
            // 
            this.panHeatNum.Controls.Add(this.txbHeatNum);
            this.panHeatNum.Controls.Add(this.lblHeatNum);
            this.panHeatNum.Location = new System.Drawing.Point(374, 3);
            this.panHeatNum.Name = "panHeatNum";
            this.panHeatNum.Size = new System.Drawing.Size(161, 43);
            this.panHeatNum.TabIndex = 24;
            // 
            // txbHeatNum
            // 
            this.txbHeatNum.Enabled = false;
            this.txbHeatNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbHeatNum.Location = new System.Drawing.Point(4, 18);
            this.txbHeatNum.Name = "txbHeatNum";
            this.txbHeatNum.Size = new System.Drawing.Size(143, 21);
            this.txbHeatNum.TabIndex = 1;
            this.txbHeatNum.TabStop = false;
            this.txbHeatNum.TextChanged += new System.EventHandler(this.txbHeatNum_TextChanged);
            // 
            // lblHeatNum
            // 
            this.lblHeatNum.AutoSize = true;
            this.lblHeatNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblHeatNum.Location = new System.Drawing.Point(1, 3);
            this.lblHeatNum.Name = "lblHeatNum";
            this.lblHeatNum.Size = new System.Drawing.Size(110, 13);
            this.lblHeatNum.TabIndex = 18;
            this.lblHeatNum.Text = "Расчет плавки №";
            // 
            // panSteelBrand
            // 
            this.panSteelBrand.Controls.Add(this.cmbSteelBrand);
            this.panSteelBrand.Controls.Add(this.lblSteelBrand);
            this.panSteelBrand.Location = new System.Drawing.Point(123, 52);
            this.panSteelBrand.Name = "panSteelBrand";
            this.panSteelBrand.Size = new System.Drawing.Size(114, 43);
            this.panSteelBrand.TabIndex = 23;
            // 
            // cmbSteelBrand
            // 
            this.cmbSteelBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbSteelBrand.FormattingEnabled = true;
            this.cmbSteelBrand.Items.AddRange(new object[] {
            "Группа 1",
            "Группа 2",
            "Группа 3",
            "Группа 4",
            "Группа 5"});
            this.cmbSteelBrand.Location = new System.Drawing.Point(3, 17);
            this.cmbSteelBrand.Name = "cmbSteelBrand";
            this.cmbSteelBrand.Size = new System.Drawing.Size(92, 23);
            this.cmbSteelBrand.TabIndex = 2;
            this.cmbSteelBrand.TabStop = false;
            // 
            // lblSteelBrand
            // 
            this.lblSteelBrand.AutoSize = true;
            this.lblSteelBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSteelBrand.Location = new System.Drawing.Point(1, 1);
            this.lblSteelBrand.Name = "lblSteelBrand";
            this.lblSteelBrand.Size = new System.Drawing.Size(83, 13);
            this.lblSteelBrand.TabIndex = 14;
            this.lblSteelBrand.Text = "Марка стали";
            // 
            // panelSteelGroup
            // 
            this.panelSteelGroup.Controls.Add(this.cmbSteelGroup);
            this.panelSteelGroup.Controls.Add(this.lblSteelGroup);
            this.panelSteelGroup.Location = new System.Drawing.Point(3, 52);
            this.panelSteelGroup.Name = "panelSteelGroup";
            this.panelSteelGroup.Size = new System.Drawing.Size(114, 43);
            this.panelSteelGroup.TabIndex = 31;
            // 
            // cmbSteelGroup
            // 
            this.cmbSteelGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbSteelGroup.FormattingEnabled = true;
            this.cmbSteelGroup.Items.AddRange(new object[] {
            "Группа 1",
            "Группа 2",
            "Группа 3",
            "Группа 4",
            "Группа 5"});
            this.cmbSteelGroup.Location = new System.Drawing.Point(3, 17);
            this.cmbSteelGroup.Name = "cmbSteelGroup";
            this.cmbSteelGroup.Size = new System.Drawing.Size(92, 23);
            this.cmbSteelGroup.TabIndex = 3;
            this.cmbSteelGroup.TabStop = false;
            // 
            // lblSteelGroup
            // 
            this.lblSteelGroup.AutoSize = true;
            this.lblSteelGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSteelGroup.Location = new System.Drawing.Point(1, 1);
            this.lblSteelGroup.Name = "lblSteelGroup";
            this.lblSteelGroup.Size = new System.Drawing.Size(86, 13);
            this.lblSteelGroup.TabIndex = 14;
            this.lblSteelGroup.Text = "Группа стали";
            // 
            // panSteelTemp
            // 
            this.panSteelTemp.Controls.Add(this.txbSteelTemp);
            this.panSteelTemp.Controls.Add(this.lblSteelTemp);
            this.panSteelTemp.Location = new System.Drawing.Point(243, 52);
            this.panSteelTemp.Name = "panSteelTemp";
            this.panSteelTemp.Size = new System.Drawing.Size(119, 43);
            this.panSteelTemp.TabIndex = 26;
            // 
            // txbSteelTemp
            // 
            this.txbSteelTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbSteelTemp.Location = new System.Drawing.Point(4, 20);
            this.txbSteelTemp.Name = "txbSteelTemp";
            this.txbSteelTemp.Size = new System.Drawing.Size(100, 21);
            this.txbSteelTemp.TabIndex = 4;
            // 
            // lblSteelTemp
            // 
            this.lblSteelTemp.AutoSize = true;
            this.lblSteelTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSteelTemp.Location = new System.Drawing.Point(-1, 2);
            this.lblSteelTemp.Name = "lblSteelTemp";
            this.lblSteelTemp.Size = new System.Drawing.Size(106, 13);
            this.lblSteelTemp.TabIndex = 22;
            this.lblSteelTemp.Text = "Температура, °C";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblTgtMgO);
            this.panel1.Controls.Add(this.txbMgO);
            this.panel1.Location = new System.Drawing.Point(368, 52);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(120, 43);
            this.panel1.TabIndex = 28;
            // 
            // lblTgtMgO
            // 
            this.lblTgtMgO.AutoSize = true;
            this.lblTgtMgO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTgtMgO.Location = new System.Drawing.Point(0, 2);
            this.lblTgtMgO.Name = "lblTgtMgO";
            this.lblTgtMgO.Size = new System.Drawing.Size(87, 13);
            this.lblTgtMgO.TabIndex = 20;
            this.lblTgtMgO.Text = "Конц. MgO, %";
            // 
            // txbMgO
            // 
            this.txbMgO.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbMgO.Location = new System.Drawing.Point(3, 20);
            this.txbMgO.Name = "txbMgO";
            this.txbMgO.Size = new System.Drawing.Size(100, 21);
            this.txbMgO.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblTgtFeO);
            this.panel2.Controls.Add(this.txbFeO);
            this.panel2.Location = new System.Drawing.Point(494, 52);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(120, 43);
            this.panel2.TabIndex = 30;
            // 
            // lblTgtFeO
            // 
            this.lblTgtFeO.AutoSize = true;
            this.lblTgtFeO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTgtFeO.Location = new System.Drawing.Point(0, 2);
            this.lblTgtFeO.Name = "lblTgtFeO";
            this.lblTgtFeO.Size = new System.Drawing.Size(84, 13);
            this.lblTgtFeO.TabIndex = 20;
            this.lblTgtFeO.Text = "Конц. FeO, %";
            // 
            // txbFeO
            // 
            this.txbFeO.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbFeO.Location = new System.Drawing.Point(3, 20);
            this.txbFeO.Name = "txbFeO";
            this.txbFeO.Size = new System.Drawing.Size(100, 21);
            this.txbFeO.TabIndex = 6;
            // 
            // panBasiticy
            // 
            this.panBasiticy.Controls.Add(this.label2);
            this.panBasiticy.Controls.Add(this.txbBasiticy);
            this.panBasiticy.Location = new System.Drawing.Point(3, 101);
            this.panBasiticy.Name = "panBasiticy";
            this.panBasiticy.Size = new System.Drawing.Size(161, 43);
            this.panBasiticy.TabIndex = 28;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(0, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Отношение CaO/SiO2";
            // 
            // txbBasiticy
            // 
            this.txbBasiticy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbBasiticy.Location = new System.Drawing.Point(3, 20);
            this.txbBasiticy.Name = "txbBasiticy";
            this.txbBasiticy.Size = new System.Drawing.Size(144, 21);
            this.txbBasiticy.TabIndex = 7;
            // 
            // panInput
            // 
            this.panInput.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.panInput.ColumnCount = 6;
            this.panInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.panInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.6094F));
            this.panInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.04453F));
            this.panInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.11536F));
            this.panInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.11536F));
            this.panInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.11536F));
            this.panInput.Controls.Add(this.lblIron, 0, 0);
            this.panInput.Controls.Add(this.btnCalculate, 4, 3);
            this.panInput.Controls.Add(this.btnApprove, 5, 3);
            this.panInput.Controls.Add(this.panSteelTask, 1, 2);
            this.panInput.Controls.Add(this.panIronTask, 1, 0);
            this.panInput.Controls.Add(this.panIronMass, 2, 0);
            this.panInput.Controls.Add(this.panScrapTemp, 3, 1);
            this.panInput.Controls.Add(this.btnIronChem, 4, 0);
            this.panInput.Controls.Add(this.btnScrapChem, 4, 1);
            this.panInput.Controls.Add(this.lblScrap, 0, 1);
            this.panInput.Controls.Add(this.panIronTemp, 3, 0);
            this.panInput.Controls.Add(this.btnIronSel, 5, 0);
            this.panInput.Controls.Add(this.btnScrapSel, 5, 1);
            this.panInput.Controls.Add(this.panel4, 2, 1);
            this.panInput.Controls.Add(this.panel5, 2, 2);
            this.panInput.Controls.Add(this.lblSteel, 0, 2);
            this.panInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panInput.Location = new System.Drawing.Point(0, 0);
            this.panInput.Name = "panInput";
            this.panInput.RowCount = 4;
            this.panInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.panInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.panInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.panInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.99813F));
            this.panInput.Size = new System.Drawing.Size(732, 186);
            this.panInput.TabIndex = 31;
            // 
            // panIronTask
            // 
            this.panIronTask.Controls.Add(this.txbIronTask);
            this.panIronTask.Controls.Add(this.label1);
            this.panIronTask.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panIronTask.Location = new System.Drawing.Point(90, 4);
            this.panIronTask.Name = "panIronTask";
            this.panIronTask.Size = new System.Drawing.Size(93, 39);
            this.panIronTask.TabIndex = 63;
            // 
            // txbIronTask
            // 
            this.txbIronTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbIronTask.Location = new System.Drawing.Point(4, 14);
            this.txbIronTask.Name = "txbIronTask";
            this.txbIronTask.Size = new System.Drawing.Size(54, 21);
            this.txbIronTask.TabIndex = 11;
            this.txbIronTask.TextChanged += new System.EventHandler(this.txbIronTask_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Заказ, т";
            // 
            // panIronMass
            // 
            this.panIronMass.Controls.Add(this.txbIronCalc);
            this.panIronMass.Controls.Add(this.lblIronMass);
            this.panIronMass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panIronMass.Location = new System.Drawing.Point(190, 4);
            this.panIronMass.Name = "panIronMass";
            this.panIronMass.Size = new System.Drawing.Size(90, 39);
            this.panIronMass.TabIndex = 62;
            // 
            // txbIronCalc
            // 
            this.txbIronCalc.Enabled = false;
            this.txbIronCalc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbIronCalc.Location = new System.Drawing.Point(4, 14);
            this.txbIronCalc.Name = "txbIronCalc";
            this.txbIronCalc.Size = new System.Drawing.Size(54, 21);
            this.txbIronCalc.TabIndex = 29;
            // 
            // lblIronMass
            // 
            this.lblIronMass.AutoSize = true;
            this.lblIronMass.Location = new System.Drawing.Point(3, 0);
            this.lblIronMass.Name = "lblIronMass";
            this.lblIronMass.Size = new System.Drawing.Size(53, 13);
            this.lblIronMass.TabIndex = 22;
            this.lblIronMass.Text = "Расчет, т";
            // 
            // panScrapTemp
            // 
            this.panScrapTemp.Controls.Add(this.lblScrapTemp);
            this.panScrapTemp.Controls.Add(this.txbScrapTemp);
            this.panScrapTemp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panScrapTemp.Location = new System.Drawing.Point(287, 50);
            this.panScrapTemp.Name = "panScrapTemp";
            this.panScrapTemp.Size = new System.Drawing.Size(141, 39);
            this.panScrapTemp.TabIndex = 28;
            // 
            // lblScrapTemp
            // 
            this.lblScrapTemp.AutoSize = true;
            this.lblScrapTemp.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblScrapTemp.Location = new System.Drawing.Point(0, 0);
            this.lblScrapTemp.Name = "lblScrapTemp";
            this.lblScrapTemp.Size = new System.Drawing.Size(91, 13);
            this.lblScrapTemp.TabIndex = 22;
            this.lblScrapTemp.Text = "Температура, °C";
            // 
            // txbScrapTemp
            // 
            this.txbScrapTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbScrapTemp.Location = new System.Drawing.Point(2, 15);
            this.txbScrapTemp.Name = "txbScrapTemp";
            this.txbScrapTemp.Size = new System.Drawing.Size(100, 21);
            this.txbScrapTemp.TabIndex = 13;
            // 
            // btnIronChem
            // 
            this.btnIronChem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnIronChem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnIronChem.Location = new System.Drawing.Point(435, 4);
            this.btnIronChem.Name = "btnIronChem";
            this.btnIronChem.Size = new System.Drawing.Size(141, 39);
            this.btnIronChem.TabIndex = 30;
            this.btnIronChem.TabStop = false;
            this.btnIronChem.Text = "Хим.состав\r\n";
            this.btnIronChem.UseVisualStyleBackColor = true;
            this.btnIronChem.Click += new System.EventHandler(this.btnIronChem_Click);
            // 
            // btnScrapChem
            // 
            this.btnScrapChem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnScrapChem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnScrapChem.Location = new System.Drawing.Point(435, 50);
            this.btnScrapChem.Name = "btnScrapChem";
            this.btnScrapChem.Size = new System.Drawing.Size(141, 39);
            this.btnScrapChem.TabIndex = 28;
            this.btnScrapChem.TabStop = false;
            this.btnScrapChem.Text = "Хим.состав\r\n";
            this.btnScrapChem.UseVisualStyleBackColor = true;
            this.btnScrapChem.Click += new System.EventHandler(this.btnScrapChem_Click);
            // 
            // lblScrap
            // 
            this.lblScrap.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblScrap.AutoSize = true;
            this.lblScrap.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblScrap.Location = new System.Drawing.Point(4, 59);
            this.lblScrap.Name = "lblScrap";
            this.lblScrap.Size = new System.Drawing.Size(44, 20);
            this.lblScrap.TabIndex = 0;
            this.lblScrap.Text = "Лом";
            // 
            // lblIron
            // 
            this.lblIron.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblIron.AutoSize = true;
            this.lblIron.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblIron.Location = new System.Drawing.Point(4, 13);
            this.lblIron.Name = "lblIron";
            this.lblIron.Size = new System.Drawing.Size(56, 20);
            this.lblIron.TabIndex = 0;
            this.lblIron.Text = "Чугун";
            // 
            // panIronTemp
            // 
            this.panIronTemp.Controls.Add(this.lblIronTemp);
            this.panIronTemp.Controls.Add(this.txbIronTemp);
            this.panIronTemp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panIronTemp.Location = new System.Drawing.Point(287, 4);
            this.panIronTemp.Name = "panIronTemp";
            this.panIronTemp.Size = new System.Drawing.Size(141, 39);
            this.panIronTemp.TabIndex = 27;
            // 
            // lblIronTemp
            // 
            this.lblIronTemp.AutoSize = true;
            this.lblIronTemp.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblIronTemp.Location = new System.Drawing.Point(0, 0);
            this.lblIronTemp.Name = "lblIronTemp";
            this.lblIronTemp.Size = new System.Drawing.Size(91, 13);
            this.lblIronTemp.TabIndex = 22;
            this.lblIronTemp.Text = "Температура, °C";
            // 
            // txbIronTemp
            // 
            this.txbIronTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbIronTemp.Location = new System.Drawing.Point(4, 13);
            this.txbIronTemp.Name = "txbIronTemp";
            this.txbIronTemp.Size = new System.Drawing.Size(100, 21);
            this.txbIronTemp.TabIndex = 12;
            // 
            // btnIronSel
            // 
            this.btnIronSel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnIronSel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnIronSel.Location = new System.Drawing.Point(583, 4);
            this.btnIronSel.Name = "btnIronSel";
            this.btnIronSel.Size = new System.Drawing.Size(145, 39);
            this.btnIronSel.TabIndex = 58;
            this.btnIronSel.Text = "Выбрать...";
            this.btnIronSel.UseVisualStyleBackColor = true;
            this.btnIronSel.Click += new System.EventHandler(this.btnIronSel_Click);
            // 
            // btnScrapSel
            // 
            this.btnScrapSel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnScrapSel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnScrapSel.Location = new System.Drawing.Point(583, 50);
            this.btnScrapSel.Name = "btnScrapSel";
            this.btnScrapSel.Size = new System.Drawing.Size(145, 39);
            this.btnScrapSel.TabIndex = 59;
            this.btnScrapSel.Text = "Выбрать...";
            this.btnScrapSel.UseVisualStyleBackColor = true;
            this.btnScrapSel.Click += new System.EventHandler(this.btnScrapSel_Click);
            // 
            // lblSteel
            // 
            this.lblSteel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSteel.AutoSize = true;
            this.lblSteel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSteel.Location = new System.Drawing.Point(4, 105);
            this.lblSteel.Name = "lblSteel";
            this.lblSteel.Size = new System.Drawing.Size(62, 20);
            this.lblSteel.TabIndex = 60;
            this.lblSteel.Text = "Сталь";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.txbScrapOut);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(190, 50);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(90, 39);
            this.panel4.TabIndex = 64;
            // 
            // txbScrapOut
            // 
            this.txbScrapOut.Enabled = false;
            this.txbScrapOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbScrapOut.Location = new System.Drawing.Point(4, 11);
            this.txbScrapOut.Name = "txbScrapOut";
            this.txbScrapOut.Size = new System.Drawing.Size(54, 21);
            this.txbScrapOut.TabIndex = 29;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Расчет, т";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.txbSteelOut);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(190, 96);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(90, 39);
            this.panel5.TabIndex = 65;
            // 
            // txbSteelOut
            // 
            this.txbSteelOut.Enabled = false;
            this.txbSteelOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbSteelOut.Location = new System.Drawing.Point(4, 11);
            this.txbSteelOut.Name = "txbSteelOut";
            this.txbSteelOut.Size = new System.Drawing.Size(54, 21);
            this.txbSteelOut.TabIndex = 29;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Расчет, т";
            // 
            // btnApprove
            // 
            this.btnApprove.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnApprove.ForeColor = System.Drawing.Color.Black;
            this.btnApprove.Location = new System.Drawing.Point(583, 142);
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btnApprove.Size = new System.Drawing.Size(145, 40);
            this.btnApprove.TabIndex = 101;
            this.btnApprove.Text = "Подтв.\r\n";
            this.btnApprove.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnApprove.UseVisualStyleBackColor = true;
            this.btnApprove.Click += new System.EventHandler(this.btnApprove_Click);
            // 
            // btnCalculate
            // 
            this.btnCalculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCalculate.ForeColor = System.Drawing.Color.Black;
            this.btnCalculate.Location = new System.Drawing.Point(435, 142);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btnCalculate.Size = new System.Drawing.Size(141, 40);
            this.btnCalculate.TabIndex = 102;
            this.btnCalculate.Text = "Расчёт";
            this.btnCalculate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // rtbConvState
            // 
            this.rtbConvState.BackColor = System.Drawing.Color.DimGray;
            this.rtbConvState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbConvState.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbConvState.ForeColor = System.Drawing.Color.GreenYellow;
            this.rtbConvState.Location = new System.Drawing.Point(0, 0);
            this.rtbConvState.Name = "rtbConvState";
            this.rtbConvState.Size = new System.Drawing.Size(732, 100);
            this.rtbConvState.TabIndex = 0;
            this.rtbConvState.Text = "Welcome!";
            // 
            // central
            // 
            this.central.Dock = System.Windows.Forms.DockStyle.Fill;
            this.central.Location = new System.Drawing.Point(0, 158);
            this.central.Name = "central";
            this.central.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // central.Panel1
            // 
            this.central.Panel1.BackColor = System.Drawing.Color.Khaki;
            this.central.Panel1.Controls.Add(this.panInput);
            this.central.Panel1MinSize = 150;
            // 
            // central.Panel2
            // 
            this.central.Panel2.Controls.Add(this.heats);
            this.central.Panel2.Controls.Add(this.panel3);
            this.central.Size = new System.Drawing.Size(732, 546);
            this.central.SplitterDistance = 186;
            this.central.TabIndex = 33;
            // 
            // heats
            // 
            this.heats.AllowUserToAddRows = false;
            this.heats.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.heats.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.heats.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.heats.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.heatNum,
            this.heatKind,
            this.heatMark,
            this.ironWeight,
            this.ironTemp,
            this.heatPSi,
            this.scrapWait,
            this.steelWeight,
            this.status});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.heats.DefaultCellStyle = dataGridViewCellStyle2;
            this.heats.Dock = System.Windows.Forms.DockStyle.Fill;
            this.heats.Location = new System.Drawing.Point(0, 0);
            this.heats.Name = "heats";
            this.heats.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.heats.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.heats.Size = new System.Drawing.Size(732, 256);
            this.heats.TabIndex = 0;
            this.heats.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.heats_CellContentClick);
            this.heats.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.heats_CellDoubleClick);
            // 
            // heatNum
            // 
            this.heatNum.HeaderText = "№ плавки";
            this.heatNum.Name = "heatNum";
            this.heatNum.ReadOnly = true;
            // 
            // heatKind
            // 
            this.heatKind.HeaderText = "";
            this.heatKind.Name = "heatKind";
            this.heatKind.ReadOnly = true;
            this.heatKind.Width = 50;
            // 
            // heatMark
            // 
            this.heatMark.HeaderText = "Марка стали";
            this.heatMark.Name = "heatMark";
            this.heatMark.ReadOnly = true;
            this.heatMark.Width = 80;
            // 
            // ironWeight
            // 
            this.ironWeight.HeaderText = "Вес чугуна";
            this.ironWeight.Name = "ironWeight";
            this.ironWeight.ReadOnly = true;
            this.ironWeight.Width = 80;
            // 
            // ironTemp
            // 
            this.ironTemp.HeaderText = "Темп.чугуна";
            this.ironTemp.Name = "ironTemp";
            this.ironTemp.ReadOnly = true;
            this.ironTemp.Width = 80;
            // 
            // heatPSi
            // 
            this.heatPSi.HeaderText = "%Si в чугуне";
            this.heatPSi.Name = "heatPSi";
            this.heatPSi.ReadOnly = true;
            this.heatPSi.Width = 50;
            // 
            // scrapWait
            // 
            this.scrapWait.HeaderText = "Вес лома";
            this.scrapWait.Name = "scrapWait";
            this.scrapWait.ReadOnly = true;
            this.scrapWait.Width = 80;
            // 
            // steelWeight
            // 
            this.steelWeight.HeaderText = "Вес стали";
            this.steelWeight.Name = "steelWeight";
            this.steelWeight.ReadOnly = true;
            this.steelWeight.Width = 80;
            // 
            // status
            // 
            this.status.HeaderText = "Статус";
            this.status.Name = "status";
            this.status.ReadOnly = true;
            this.status.Width = 110;
            // 
            // panSteelTask
            // 
            this.panSteelTask.Controls.Add(this.txbSteelTask);
            this.panSteelTask.Controls.Add(this.label5);
            this.panSteelTask.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panSteelTask.Location = new System.Drawing.Point(90, 96);
            this.panSteelTask.Name = "panSteelTask";
            this.panSteelTask.Size = new System.Drawing.Size(93, 39);
            this.panSteelTask.TabIndex = 67;
            // 
            // txbSteelTask
            // 
            this.txbSteelTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbSteelTask.Location = new System.Drawing.Point(4, 14);
            this.txbSteelTask.Name = "txbSteelTask";
            this.txbSteelTask.Size = new System.Drawing.Size(54, 21);
            this.txbSteelTask.TabIndex = 11;
            this.txbSteelTask.TextChanged += new System.EventHandler(this.txbSteelTask_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Заказ, т";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.rtbConvState);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 256);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(732, 100);
            this.panel3.TabIndex = 1;
            // 
            // panUNRS
            // 
            this.panUNRS.Controls.Add(this.cmbUNRS);
            this.panUNRS.Controls.Add(this.lblUNRS);
            this.panUNRS.Location = new System.Drawing.Point(541, 3);
            this.panUNRS.Name = "panUNRS";
            this.panUNRS.Size = new System.Drawing.Size(114, 43);
            this.panUNRS.TabIndex = 32;
            // 
            // cmbUNRS
            // 
            this.cmbUNRS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbUNRS.FormattingEnabled = true;
            this.cmbUNRS.Items.AddRange(new object[] {
            "УНРС-1",
            "УНРС-2",
            "УНРС-3",
            "УНРС-4",
            "УНРС-5"});
            this.cmbUNRS.Location = new System.Drawing.Point(3, 17);
            this.cmbUNRS.Name = "cmbUNRS";
            this.cmbUNRS.Size = new System.Drawing.Size(92, 23);
            this.cmbUNRS.TabIndex = 3;
            this.cmbUNRS.TabStop = false;
            // 
            // lblUNRS
            // 
            this.lblUNRS.AutoSize = true;
            this.lblUNRS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblUNRS.Location = new System.Drawing.Point(1, 1);
            this.lblUNRS.Name = "lblUNRS";
            this.lblUNRS.Size = new System.Drawing.Size(83, 13);
            this.lblUNRS.TabIndex = 14;
            this.lblUNRS.Text = "Выбор УНРС";
            // 
            // MixCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.central);
            this.Controls.Add(this.heading);
            this.Name = "MixCalculator";
            this.Size = new System.Drawing.Size(732, 704);
            this.Load += new System.EventHandler(this.MixCalculator_Load);
            this.heading.ResumeLayout(false);
            this.panTitleHeading.ResumeLayout(false);
            this.panTitleHeading.PerformLayout();
            this.panHeatNum.ResumeLayout(false);
            this.panHeatNum.PerformLayout();
            this.panSteelBrand.ResumeLayout(false);
            this.panSteelBrand.PerformLayout();
            this.panelSteelGroup.ResumeLayout(false);
            this.panelSteelGroup.PerformLayout();
            this.panSteelTemp.ResumeLayout(false);
            this.panSteelTemp.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panBasiticy.ResumeLayout(false);
            this.panBasiticy.PerformLayout();
            this.panInput.ResumeLayout(false);
            this.panInput.PerformLayout();
            this.panIronTask.ResumeLayout(false);
            this.panIronTask.PerformLayout();
            this.panIronMass.ResumeLayout(false);
            this.panIronMass.PerformLayout();
            this.panScrapTemp.ResumeLayout(false);
            this.panScrapTemp.PerformLayout();
            this.panIronTemp.ResumeLayout(false);
            this.panIronTemp.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.central.Panel1.ResumeLayout(false);
            this.central.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.central)).EndInit();
            this.central.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.heats)).EndInit();
            this.panSteelTask.ResumeLayout(false);
            this.panSteelTask.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panUNRS.ResumeLayout(false);
            this.panUNRS.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label lblTitleHeading;
        public System.Windows.Forms.TextBox txbHeatNum;
        public System.Windows.Forms.TextBox txbSteelTemp;
        public System.Windows.Forms.TextBox txbMgO;
        public System.Windows.Forms.TextBox txbFeO;
        public System.Windows.Forms.TextBox txbBasiticy;
        public System.Windows.Forms.TextBox txbIronTemp;
        public System.Windows.Forms.TextBox txbIronTask;

        private System.Windows.Forms.FlowLayoutPanel heading;
        private System.Windows.Forms.Panel panTitleHeading;
        private System.Windows.Forms.Panel panHeatNum;
        private System.Windows.Forms.Label lblHeatNum;
        private System.Windows.Forms.Panel panSteelBrand;
        private System.Windows.Forms.ComboBox cmbSteelBrand;
        private System.Windows.Forms.Label lblSteelBrand;
        private System.Windows.Forms.Panel panSteelTemp;
        private System.Windows.Forms.Label lblSteelTemp;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTgtMgO;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblTgtFeO;
        private System.Windows.Forms.Panel panBasiticy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel panInput;
        private System.Windows.Forms.Panel panScrapTemp;
        private System.Windows.Forms.Label lblScrapTemp;
        private System.Windows.Forms.Button btnIronChem;
        private System.Windows.Forms.Button btnScrapChem;
        private System.Windows.Forms.Label lblScrap;
        private System.Windows.Forms.Label lblIron;
        private System.Windows.Forms.Panel panIronTemp;
        private System.Windows.Forms.Label lblIronTemp;
        private System.Windows.Forms.Button btnIronSel;
        private System.Windows.Forms.Button btnScrapSel;
        private System.Windows.Forms.Label lblSteel;
        private System.Windows.Forms.SplitContainer central;
        private System.Windows.Forms.DataGridView heats;
        private System.Windows.Forms.RichTextBox rtbConvState;
        public System.Windows.Forms.TextBox txbScrapTemp;
        private System.Windows.Forms.Panel panIronTask;
        public System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panIronMass;
        private System.Windows.Forms.TextBox txbIronCalc;
        public System.Windows.Forms.Label lblIronMass;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txbScrapOut;
        public System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txbSteelOut;
        public System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panelSteelGroup;
        private System.Windows.Forms.ComboBox cmbSteelGroup;
        private System.Windows.Forms.Label lblSteelGroup;
        private System.Windows.Forms.Button btnApprove;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.DataGridViewTextBoxColumn heatNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn heatKind;
        private System.Windows.Forms.DataGridViewTextBoxColumn heatMark;
        private System.Windows.Forms.DataGridViewTextBoxColumn ironWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn ironTemp;
        private System.Windows.Forms.DataGridViewTextBoxColumn heatPSi;
        private System.Windows.Forms.DataGridViewTextBoxColumn scrapWait;
        private System.Windows.Forms.DataGridViewTextBoxColumn steelWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn status;
        private System.Windows.Forms.Panel panSteelTask;
        public System.Windows.Forms.TextBox txbSteelTask;
        public System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panUNRS;
        private System.Windows.Forms.ComboBox cmbUNRS;
        private System.Windows.Forms.Label lblUNRS;
    }
}
