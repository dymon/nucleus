﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Charge5Classes {
    public class OutData {
        /// <summary>
        /// Масса чугуна
        /// </summary>
        public int MHi;

        /// <summary>
        /// Масса скрапа
        /// </summary>
        public int MSc;

        /// <summary>
        /// Масса извести
        /// </summary>
        public int MLi;

        /// <summary>
        /// Масса доломита или МАХГ
        /// </summary>
        public int MDlm;

        /// <summary>
        /// Масса фома
        /// </summary>
        public int MFom;

        /// <summary>
        /// Масса долмс
        /// </summary>
        public int MDlms;

        /// <summary>
        /// Входные параметры были найдены в таблицах
        /// </summary>
        public bool IsFound;
    }
}