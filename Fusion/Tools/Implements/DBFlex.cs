﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Implements
{
    public class DBFlex
    {
        public const string ArgEventName = "@EventName";
        public const string ArgCommandName = "@Command";
        public const string ArgCountName = "@Count";
        public const string ArgErrorCodeName = "@ErrorCode";
        public const string ArgErrorStringName = "@ErrorString";
    }
}
