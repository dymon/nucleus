﻿namespace DataGathering
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.открытьФайлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьФайлToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.прпрToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.папкуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сводногоОтчетаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tPassport = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridViewFusion = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridViewOffGas = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridViewLance = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dataGridViewTrakt = new System.Windows.Forms.DataGridView();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.dataGridViewMetalMirror = new System.Windows.Forms.DataGridView();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.dataGridViewScrapBuckets = new System.Windows.Forms.DataGridView();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.dataGridViewHotMetal = new System.Windows.Forms.DataGridView();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.dataGridViewSlagAnalysys = new System.Windows.Forms.DataGridView();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.dataGridViewSteelAnalysys = new System.Windows.Forms.DataGridView();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.dataGridViewSublance = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.zGraph = new ZedGraph.ZedGraphControl();
            this.gbValues = new System.Windows.Forms.GroupBox();
            this.cbRealCO2 = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbRealFlow = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbRealCO = new System.Windows.Forms.CheckBox();
            this.lbRealCO = new System.Windows.Forms.Label();
            this.cbFlow = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbTemp = new System.Windows.Forms.CheckBox();
            this.lalbel2333 = new System.Windows.Forms.Label();
            this.gbVars = new System.Windows.Forms.GroupBox();
            this.cbOFlow = new System.Windows.Forms.CheckBox();
            this.lbOFlowValue = new System.Windows.Forms.Label();
            this.lbOFlow = new System.Windows.Forms.Label();
            this.cbLance = new System.Windows.Forms.CheckBox();
            this.lbLanceValue = new System.Windows.Forms.Label();
            this.lbLance = new System.Windows.Forms.Label();
            this.lbTimeValue = new System.Windows.Forms.Label();
            this.lbTime = new System.Windows.Forms.Label();
            this.cbO2 = new System.Windows.Forms.CheckBox();
            this.cbCO = new System.Windows.Forms.CheckBox();
            this.cbCO2 = new System.Windows.Forms.CheckBox();
            this.cbN2 = new System.Windows.Forms.CheckBox();
            this.cbAr = new System.Windows.Forms.CheckBox();
            this.cbH2 = new System.Windows.Forms.CheckBox();
            this.lbArValue = new System.Windows.Forms.Label();
            this.lbO2Value = new System.Windows.Forms.Label();
            this.lbCOValue = new System.Windows.Forms.Label();
            this.lbN2Value = new System.Windows.Forms.Label();
            this.lbCO2Value = new System.Windows.Forms.Label();
            this.lbH2Value = new System.Windows.Forms.Label();
            this.lbAr = new System.Windows.Forms.Label();
            this.lbO2 = new System.Windows.Forms.Label();
            this.lbCO = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbCO2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbN2 = new System.Windows.Forms.Label();
            this.lbH2 = new System.Windows.Forms.Label();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.textBoxPassport = new System.Windows.Forms.TextBox();
            this.tbPassport = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.tPassport.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFusion)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOffGas)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLance)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTrakt)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMetalMirror)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewScrapBuckets)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHotMetal)).BeginInit();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSlagAnalysys)).BeginInit();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSteelAnalysys)).BeginInit();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSublance)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.gbValues.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьФайлToolStripMenuItem,
            this.прпрToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(916, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // открытьФайлToolStripMenuItem
            // 
            this.открытьФайлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьФайлToolStripMenuItem1,
            this.выходToolStripMenuItem});
            this.открытьФайлToolStripMenuItem.Name = "открытьФайлToolStripMenuItem";
            this.открытьФайлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.открытьФайлToolStripMenuItem.Text = "Файл";
            // 
            // открытьФайлToolStripMenuItem1
            // 
            this.открытьФайлToolStripMenuItem1.Name = "открытьФайлToolStripMenuItem1";
            this.открытьФайлToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.открытьФайлToolStripMenuItem1.Text = "Открыть файл";
            this.открытьФайлToolStripMenuItem1.Click += new System.EventHandler(this.открытьФайлToolStripMenuItem1_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // прпрToolStripMenuItem
            // 
            this.прпрToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.папкуToolStripMenuItem,
            this.сводногоОтчетаToolStripMenuItem});
            this.прпрToolStripMenuItem.Name = "прпрToolStripMenuItem";
            this.прпрToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.прпрToolStripMenuItem.Text = "Экспорт";
            // 
            // папкуToolStripMenuItem
            // 
            this.папкуToolStripMenuItem.Name = "папкуToolStripMenuItem";
            this.папкуToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.папкуToolStripMenuItem.Text = "Папки";
            this.папкуToolStripMenuItem.Click += new System.EventHandler(this.ПапкуToolStripMenuItemClick);
            // 
            // сводногоОтчетаToolStripMenuItem
            // 
            this.сводногоОтчетаToolStripMenuItem.Name = "сводногоОтчетаToolStripMenuItem";
            this.сводногоОтчетаToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.сводногоОтчетаToolStripMenuItem.Text = "Сводного отчета";
            this.сводногоОтчетаToolStripMenuItem.Click += new System.EventHandler(this.сводногоОтчетаToolStripMenuItem_Click);
            // 
            // tPassport
            // 
            this.tPassport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tPassport.Controls.Add(this.tabPage1);
            this.tPassport.Controls.Add(this.tabPage2);
            this.tPassport.Controls.Add(this.tabPage3);
            this.tPassport.Controls.Add(this.tabPage4);
            this.tPassport.Controls.Add(this.tabPage6);
            this.tPassport.Controls.Add(this.tabPage7);
            this.tPassport.Controls.Add(this.tabPage8);
            this.tPassport.Controls.Add(this.tabPage11);
            this.tPassport.Controls.Add(this.tabPage10);
            this.tPassport.Controls.Add(this.tabPage12);
            this.tPassport.Controls.Add(this.tabPage5);
            this.tPassport.Controls.Add(this.tabPage9);
            this.tPassport.Location = new System.Drawing.Point(12, 27);
            this.tPassport.Name = "tPassport";
            this.tPassport.SelectedIndex = 0;
            this.tPassport.Size = new System.Drawing.Size(904, 544);
            this.tPassport.TabIndex = 13;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridViewFusion);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(896, 518);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Плавка";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridViewFusion
            // 
            this.dataGridViewFusion.AllowUserToAddRows = false;
            this.dataGridViewFusion.AllowUserToDeleteRows = false;
            this.dataGridViewFusion.AllowUserToOrderColumns = true;
            this.dataGridViewFusion.AllowUserToResizeRows = false;
            this.dataGridViewFusion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFusion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewFusion.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewFusion.MultiSelect = false;
            this.dataGridViewFusion.Name = "dataGridViewFusion";
            this.dataGridViewFusion.ReadOnly = true;
            this.dataGridViewFusion.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewFusion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewFusion.Size = new System.Drawing.Size(890, 512);
            this.dataGridViewFusion.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridViewOffGas);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(896, 518);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Отходящие газы";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridViewOffGas
            // 
            this.dataGridViewOffGas.AllowUserToAddRows = false;
            this.dataGridViewOffGas.AllowUserToDeleteRows = false;
            this.dataGridViewOffGas.AllowUserToOrderColumns = true;
            this.dataGridViewOffGas.AllowUserToResizeRows = false;
            this.dataGridViewOffGas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewOffGas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewOffGas.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewOffGas.MultiSelect = false;
            this.dataGridViewOffGas.Name = "dataGridViewOffGas";
            this.dataGridViewOffGas.ReadOnly = true;
            this.dataGridViewOffGas.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewOffGas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewOffGas.Size = new System.Drawing.Size(890, 492);
            this.dataGridViewOffGas.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridViewLance);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(896, 518);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Фурма";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridViewLance
            // 
            this.dataGridViewLance.AllowUserToAddRows = false;
            this.dataGridViewLance.AllowUserToDeleteRows = false;
            this.dataGridViewLance.AllowUserToOrderColumns = true;
            this.dataGridViewLance.AllowUserToResizeRows = false;
            this.dataGridViewLance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewLance.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewLance.MultiSelect = false;
            this.dataGridViewLance.Name = "dataGridViewLance";
            this.dataGridViewLance.ReadOnly = true;
            this.dataGridViewLance.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewLance.Size = new System.Drawing.Size(890, 492);
            this.dataGridViewLance.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dataGridViewTrakt);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(896, 518);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Тракт сыпучих";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dataGridViewTrakt
            // 
            this.dataGridViewTrakt.AllowUserToAddRows = false;
            this.dataGridViewTrakt.AllowUserToDeleteRows = false;
            this.dataGridViewTrakt.AllowUserToOrderColumns = true;
            this.dataGridViewTrakt.AllowUserToResizeRows = false;
            this.dataGridViewTrakt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTrakt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTrakt.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewTrakt.MultiSelect = false;
            this.dataGridViewTrakt.Name = "dataGridViewTrakt";
            this.dataGridViewTrakt.ReadOnly = true;
            this.dataGridViewTrakt.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewTrakt.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewTrakt.Size = new System.Drawing.Size(890, 492);
            this.dataGridViewTrakt.TabIndex = 2;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.dataGridViewMetalMirror);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(896, 518);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Зеркало металла";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // dataGridViewMetalMirror
            // 
            this.dataGridViewMetalMirror.AllowUserToAddRows = false;
            this.dataGridViewMetalMirror.AllowUserToDeleteRows = false;
            this.dataGridViewMetalMirror.AllowUserToOrderColumns = true;
            this.dataGridViewMetalMirror.AllowUserToResizeRows = false;
            this.dataGridViewMetalMirror.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMetalMirror.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewMetalMirror.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewMetalMirror.MultiSelect = false;
            this.dataGridViewMetalMirror.Name = "dataGridViewMetalMirror";
            this.dataGridViewMetalMirror.ReadOnly = true;
            this.dataGridViewMetalMirror.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewMetalMirror.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewMetalMirror.Size = new System.Drawing.Size(890, 492);
            this.dataGridViewMetalMirror.TabIndex = 3;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.dataGridViewScrapBuckets);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(896, 518);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Шихта";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // dataGridViewScrapBuckets
            // 
            this.dataGridViewScrapBuckets.AllowUserToAddRows = false;
            this.dataGridViewScrapBuckets.AllowUserToDeleteRows = false;
            this.dataGridViewScrapBuckets.AllowUserToOrderColumns = true;
            this.dataGridViewScrapBuckets.AllowUserToResizeRows = false;
            this.dataGridViewScrapBuckets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewScrapBuckets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewScrapBuckets.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewScrapBuckets.MultiSelect = false;
            this.dataGridViewScrapBuckets.Name = "dataGridViewScrapBuckets";
            this.dataGridViewScrapBuckets.ReadOnly = true;
            this.dataGridViewScrapBuckets.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewScrapBuckets.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewScrapBuckets.Size = new System.Drawing.Size(896, 498);
            this.dataGridViewScrapBuckets.TabIndex = 4;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.dataGridViewHotMetal);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(896, 518);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "Чугун";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // dataGridViewHotMetal
            // 
            this.dataGridViewHotMetal.AllowUserToAddRows = false;
            this.dataGridViewHotMetal.AllowUserToDeleteRows = false;
            this.dataGridViewHotMetal.AllowUserToOrderColumns = true;
            this.dataGridViewHotMetal.AllowUserToResizeRows = false;
            this.dataGridViewHotMetal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewHotMetal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewHotMetal.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewHotMetal.MultiSelect = false;
            this.dataGridViewHotMetal.Name = "dataGridViewHotMetal";
            this.dataGridViewHotMetal.ReadOnly = true;
            this.dataGridViewHotMetal.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewHotMetal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewHotMetal.Size = new System.Drawing.Size(896, 498);
            this.dataGridViewHotMetal.TabIndex = 5;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.dataGridViewSlagAnalysys);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(896, 518);
            this.tabPage11.TabIndex = 10;
            this.tabPage11.Text = "Шлак";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // dataGridViewSlagAnalysys
            // 
            this.dataGridViewSlagAnalysys.AllowUserToAddRows = false;
            this.dataGridViewSlagAnalysys.AllowUserToDeleteRows = false;
            this.dataGridViewSlagAnalysys.AllowUserToOrderColumns = true;
            this.dataGridViewSlagAnalysys.AllowUserToResizeRows = false;
            this.dataGridViewSlagAnalysys.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSlagAnalysys.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewSlagAnalysys.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewSlagAnalysys.MultiSelect = false;
            this.dataGridViewSlagAnalysys.Name = "dataGridViewSlagAnalysys";
            this.dataGridViewSlagAnalysys.ReadOnly = true;
            this.dataGridViewSlagAnalysys.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewSlagAnalysys.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewSlagAnalysys.Size = new System.Drawing.Size(890, 492);
            this.dataGridViewSlagAnalysys.TabIndex = 1;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.dataGridViewSteelAnalysys);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(896, 518);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "Сталь на повалке";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // dataGridViewSteelAnalysys
            // 
            this.dataGridViewSteelAnalysys.AllowUserToAddRows = false;
            this.dataGridViewSteelAnalysys.AllowUserToDeleteRows = false;
            this.dataGridViewSteelAnalysys.AllowUserToOrderColumns = true;
            this.dataGridViewSteelAnalysys.AllowUserToResizeRows = false;
            this.dataGridViewSteelAnalysys.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSteelAnalysys.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewSteelAnalysys.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewSteelAnalysys.MultiSelect = false;
            this.dataGridViewSteelAnalysys.Name = "dataGridViewSteelAnalysys";
            this.dataGridViewSteelAnalysys.ReadOnly = true;
            this.dataGridViewSteelAnalysys.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewSteelAnalysys.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewSteelAnalysys.Size = new System.Drawing.Size(890, 492);
            this.dataGridViewSteelAnalysys.TabIndex = 1;
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.dataGridViewSublance);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(896, 518);
            this.tabPage12.TabIndex = 11;
            this.tabPage12.Text = "Измерительный зонд";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // dataGridViewSublance
            // 
            this.dataGridViewSublance.AllowUserToAddRows = false;
            this.dataGridViewSublance.AllowUserToDeleteRows = false;
            this.dataGridViewSublance.AllowUserToOrderColumns = true;
            this.dataGridViewSublance.AllowUserToResizeRows = false;
            this.dataGridViewSublance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSublance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewSublance.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewSublance.MultiSelect = false;
            this.dataGridViewSublance.Name = "dataGridViewSublance";
            this.dataGridViewSublance.ReadOnly = true;
            this.dataGridViewSublance.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewSublance.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewSublance.Size = new System.Drawing.Size(896, 498);
            this.dataGridViewSublance.TabIndex = 2;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.zGraph);
            this.tabPage5.Controls.Add(this.gbValues);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(896, 518);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Графики";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // zGraph
            // 
            this.zGraph.AutoSize = true;
            this.zGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zGraph.Location = new System.Drawing.Point(157, 0);
            this.zGraph.Name = "zGraph";
            this.zGraph.ScrollGrace = 0D;
            this.zGraph.ScrollMaxX = 0D;
            this.zGraph.ScrollMaxY = 0D;
            this.zGraph.ScrollMaxY2 = 0D;
            this.zGraph.ScrollMinX = 0D;
            this.zGraph.ScrollMinY = 0D;
            this.zGraph.ScrollMinY2 = 0D;
            this.zGraph.Size = new System.Drawing.Size(739, 518);
            this.zGraph.TabIndex = 2;
            this.zGraph.MouseMoveEvent += new ZedGraph.ZedGraphControl.ZedMouseEventHandler(this.zGraph_MouseMoveEvent);
            // 
            // gbValues
            // 
            this.gbValues.Controls.Add(this.cbRealCO2);
            this.gbValues.Controls.Add(this.label7);
            this.gbValues.Controls.Add(this.cbRealFlow);
            this.gbValues.Controls.Add(this.label2);
            this.gbValues.Controls.Add(this.cbRealCO);
            this.gbValues.Controls.Add(this.lbRealCO);
            this.gbValues.Controls.Add(this.cbFlow);
            this.gbValues.Controls.Add(this.label3);
            this.gbValues.Controls.Add(this.cbTemp);
            this.gbValues.Controls.Add(this.lalbel2333);
            this.gbValues.Controls.Add(this.gbVars);
            this.gbValues.Controls.Add(this.cbOFlow);
            this.gbValues.Controls.Add(this.lbOFlowValue);
            this.gbValues.Controls.Add(this.lbOFlow);
            this.gbValues.Controls.Add(this.cbLance);
            this.gbValues.Controls.Add(this.lbLanceValue);
            this.gbValues.Controls.Add(this.lbLance);
            this.gbValues.Controls.Add(this.lbTimeValue);
            this.gbValues.Controls.Add(this.lbTime);
            this.gbValues.Controls.Add(this.cbO2);
            this.gbValues.Controls.Add(this.cbCO);
            this.gbValues.Controls.Add(this.cbCO2);
            this.gbValues.Controls.Add(this.cbN2);
            this.gbValues.Controls.Add(this.cbAr);
            this.gbValues.Controls.Add(this.cbH2);
            this.gbValues.Controls.Add(this.lbArValue);
            this.gbValues.Controls.Add(this.lbO2Value);
            this.gbValues.Controls.Add(this.lbCOValue);
            this.gbValues.Controls.Add(this.lbN2Value);
            this.gbValues.Controls.Add(this.lbCO2Value);
            this.gbValues.Controls.Add(this.lbH2Value);
            this.gbValues.Controls.Add(this.lbAr);
            this.gbValues.Controls.Add(this.lbO2);
            this.gbValues.Controls.Add(this.lbCO);
            this.gbValues.Controls.Add(this.label5);
            this.gbValues.Controls.Add(this.lbCO2);
            this.gbValues.Controls.Add(this.label6);
            this.gbValues.Controls.Add(this.lbN2);
            this.gbValues.Controls.Add(this.lbH2);
            this.gbValues.Dock = System.Windows.Forms.DockStyle.Left;
            this.gbValues.Location = new System.Drawing.Point(0, 0);
            this.gbValues.Name = "gbValues";
            this.gbValues.Size = new System.Drawing.Size(157, 518);
            this.gbValues.TabIndex = 1;
            this.gbValues.TabStop = false;
            this.gbValues.Text = "Значения";
            this.gbValues.Enter += new System.EventHandler(this.gbValues_Enter);
            // 
            // cbRealCO2
            // 
            this.cbRealCO2.AutoSize = true;
            this.cbRealCO2.Checked = true;
            this.cbRealCO2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbRealCO2.Location = new System.Drawing.Point(7, 223);
            this.cbRealCO2.Name = "cbRealCO2";
            this.cbRealCO2.Size = new System.Drawing.Size(15, 14);
            this.cbRealCO2.TabIndex = 35;
            this.cbRealCO2.UseVisualStyleBackColor = true;
            this.cbRealCO2.CheckedChanged += new System.EventHandler(this.cbRealCO2_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.Fuchsia;
            this.label7.Location = new System.Drawing.Point(24, 221);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 16);
            this.label7.TabIndex = 34;
            this.label7.Text = "Реал СО2";
            // 
            // cbRealFlow
            // 
            this.cbRealFlow.AutoSize = true;
            this.cbRealFlow.Checked = true;
            this.cbRealFlow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbRealFlow.Location = new System.Drawing.Point(7, 239);
            this.cbRealFlow.Name = "cbRealFlow";
            this.cbRealFlow.Size = new System.Drawing.Size(15, 14);
            this.cbRealFlow.TabIndex = 33;
            this.cbRealFlow.UseVisualStyleBackColor = true;
            this.cbRealFlow.CheckedChanged += new System.EventHandler(this.cbRealFlow_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label2.Location = new System.Drawing.Point(24, 237);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 16);
            this.label2.TabIndex = 32;
            this.label2.Text = "Реал Объем";
            // 
            // cbRealCO
            // 
            this.cbRealCO.AutoSize = true;
            this.cbRealCO.Checked = true;
            this.cbRealCO.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbRealCO.Location = new System.Drawing.Point(7, 207);
            this.cbRealCO.Name = "cbRealCO";
            this.cbRealCO.Size = new System.Drawing.Size(15, 14);
            this.cbRealCO.TabIndex = 31;
            this.cbRealCO.UseVisualStyleBackColor = true;
            this.cbRealCO.CheckedChanged += new System.EventHandler(this.cbRealCO_CheckedChanged);
            // 
            // lbRealCO
            // 
            this.lbRealCO.AutoSize = true;
            this.lbRealCO.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbRealCO.ForeColor = System.Drawing.Color.DarkGray;
            this.lbRealCO.Location = new System.Drawing.Point(24, 205);
            this.lbRealCO.Name = "lbRealCO";
            this.lbRealCO.Size = new System.Drawing.Size(70, 16);
            this.lbRealCO.TabIndex = 30;
            this.lbRealCO.Text = "Реал СО";
            // 
            // cbFlow
            // 
            this.cbFlow.AutoSize = true;
            this.cbFlow.Checked = true;
            this.cbFlow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbFlow.Location = new System.Drawing.Point(7, 190);
            this.cbFlow.Name = "cbFlow";
            this.cbFlow.Size = new System.Drawing.Size(15, 14);
            this.cbFlow.TabIndex = 29;
            this.cbFlow.UseVisualStyleBackColor = true;
            this.cbFlow.CheckedChanged += new System.EventHandler(this.cbFlow_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.SaddleBrown;
            this.label3.Location = new System.Drawing.Point(24, 188);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 16);
            this.label3.TabIndex = 28;
            this.label3.Text = "Объем";
            // 
            // cbTemp
            // 
            this.cbTemp.AutoSize = true;
            this.cbTemp.Checked = true;
            this.cbTemp.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbTemp.Location = new System.Drawing.Point(7, 174);
            this.cbTemp.Name = "cbTemp";
            this.cbTemp.Size = new System.Drawing.Size(15, 14);
            this.cbTemp.TabIndex = 27;
            this.cbTemp.UseVisualStyleBackColor = true;
            this.cbTemp.CheckedChanged += new System.EventHandler(this.cbTemp_CheckedChanged);
            // 
            // lalbel2333
            // 
            this.lalbel2333.AutoSize = true;
            this.lalbel2333.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lalbel2333.ForeColor = System.Drawing.Color.OliveDrab;
            this.lalbel2333.Location = new System.Drawing.Point(24, 172);
            this.lalbel2333.Name = "lalbel2333";
            this.lalbel2333.Size = new System.Drawing.Size(108, 16);
            this.lalbel2333.TabIndex = 26;
            this.lalbel2333.Text = "Температура";
            // 
            // gbVars
            // 
            this.gbVars.Location = new System.Drawing.Point(0, 251);
            this.gbVars.Name = "gbVars";
            this.gbVars.Size = new System.Drawing.Size(154, 199);
            this.gbVars.TabIndex = 25;
            this.gbVars.TabStop = false;
            this.gbVars.Text = "Уравнения";
            // 
            // cbOFlow
            // 
            this.cbOFlow.AutoSize = true;
            this.cbOFlow.Checked = true;
            this.cbOFlow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbOFlow.Location = new System.Drawing.Point(7, 156);
            this.cbOFlow.Name = "cbOFlow";
            this.cbOFlow.Size = new System.Drawing.Size(15, 14);
            this.cbOFlow.TabIndex = 24;
            this.cbOFlow.UseVisualStyleBackColor = true;
            this.cbOFlow.CheckedChanged += new System.EventHandler(this.cbOFlow_CheckedChanged);
            // 
            // lbOFlowValue
            // 
            this.lbOFlowValue.AutoSize = true;
            this.lbOFlowValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbOFlowValue.Location = new System.Drawing.Point(78, 154);
            this.lbOFlowValue.Name = "lbOFlowValue";
            this.lbOFlowValue.Size = new System.Drawing.Size(16, 16);
            this.lbOFlowValue.TabIndex = 23;
            this.lbOFlowValue.Text = "0";
            // 
            // lbOFlow
            // 
            this.lbOFlow.AutoSize = true;
            this.lbOFlow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbOFlow.ForeColor = System.Drawing.Color.Magenta;
            this.lbOFlow.Location = new System.Drawing.Point(24, 154);
            this.lbOFlow.Name = "lbOFlow";
            this.lbOFlow.Size = new System.Drawing.Size(51, 16);
            this.lbOFlow.TabIndex = 22;
            this.lbOFlow.Text = "O Инт";
            // 
            // cbLance
            // 
            this.cbLance.AutoSize = true;
            this.cbLance.Checked = true;
            this.cbLance.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbLance.Location = new System.Drawing.Point(7, 137);
            this.cbLance.Name = "cbLance";
            this.cbLance.Size = new System.Drawing.Size(15, 14);
            this.cbLance.TabIndex = 21;
            this.cbLance.UseVisualStyleBackColor = true;
            this.cbLance.CheckedChanged += new System.EventHandler(this.cbLance_CheckedChanged);
            // 
            // lbLanceValue
            // 
            this.lbLanceValue.AutoSize = true;
            this.lbLanceValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbLanceValue.Location = new System.Drawing.Point(79, 135);
            this.lbLanceValue.Name = "lbLanceValue";
            this.lbLanceValue.Size = new System.Drawing.Size(16, 16);
            this.lbLanceValue.TabIndex = 20;
            this.lbLanceValue.Text = "0";
            // 
            // lbLance
            // 
            this.lbLance.AutoSize = true;
            this.lbLance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbLance.ForeColor = System.Drawing.Color.Lime;
            this.lbLance.Location = new System.Drawing.Point(25, 135);
            this.lbLance.Name = "lbLance";
            this.lbLance.Size = new System.Drawing.Size(57, 16);
            this.lbLance.TabIndex = 19;
            this.lbLance.Text = "Фурма";
            // 
            // lbTimeValue
            // 
            this.lbTimeValue.AutoSize = true;
            this.lbTimeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbTimeValue.Location = new System.Drawing.Point(62, 471);
            this.lbTimeValue.Name = "lbTimeValue";
            this.lbTimeValue.Size = new System.Drawing.Size(0, 16);
            this.lbTimeValue.TabIndex = 18;
            // 
            // lbTime
            // 
            this.lbTime.AutoSize = true;
            this.lbTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbTime.Location = new System.Drawing.Point(0, 468);
            this.lbTime.Name = "lbTime";
            this.lbTime.Size = new System.Drawing.Size(54, 16);
            this.lbTime.TabIndex = 17;
            this.lbTime.Text = "Время";
            // 
            // cbO2
            // 
            this.cbO2.AutoSize = true;
            this.cbO2.Checked = true;
            this.cbO2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbO2.Location = new System.Drawing.Point(7, 39);
            this.cbO2.Name = "cbO2";
            this.cbO2.Size = new System.Drawing.Size(15, 14);
            this.cbO2.TabIndex = 16;
            this.cbO2.UseVisualStyleBackColor = true;
            this.cbO2.CheckedChanged += new System.EventHandler(this.cbO2_CheckedChanged);
            // 
            // cbCO
            // 
            this.cbCO.AutoSize = true;
            this.cbCO.Checked = true;
            this.cbCO.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCO.Location = new System.Drawing.Point(7, 59);
            this.cbCO.Name = "cbCO";
            this.cbCO.Size = new System.Drawing.Size(15, 14);
            this.cbCO.TabIndex = 15;
            this.cbCO.UseVisualStyleBackColor = true;
            this.cbCO.CheckedChanged += new System.EventHandler(this.cbCO_CheckedChanged);
            // 
            // cbCO2
            // 
            this.cbCO2.AutoSize = true;
            this.cbCO2.Checked = true;
            this.cbCO2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCO2.Location = new System.Drawing.Point(7, 79);
            this.cbCO2.Name = "cbCO2";
            this.cbCO2.Size = new System.Drawing.Size(15, 14);
            this.cbCO2.TabIndex = 14;
            this.cbCO2.UseVisualStyleBackColor = true;
            this.cbCO2.CheckedChanged += new System.EventHandler(this.cbCO2_CheckedChanged);
            // 
            // cbN2
            // 
            this.cbN2.AutoSize = true;
            this.cbN2.Checked = true;
            this.cbN2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbN2.Location = new System.Drawing.Point(7, 98);
            this.cbN2.Name = "cbN2";
            this.cbN2.Size = new System.Drawing.Size(15, 14);
            this.cbN2.TabIndex = 13;
            this.cbN2.UseVisualStyleBackColor = true;
            this.cbN2.CheckedChanged += new System.EventHandler(this.cbN2_CheckedChanged);
            // 
            // cbAr
            // 
            this.cbAr.AutoSize = true;
            this.cbAr.Checked = true;
            this.cbAr.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAr.Location = new System.Drawing.Point(7, 118);
            this.cbAr.Name = "cbAr";
            this.cbAr.Size = new System.Drawing.Size(15, 14);
            this.cbAr.TabIndex = 12;
            this.cbAr.UseVisualStyleBackColor = true;
            this.cbAr.CheckedChanged += new System.EventHandler(this.cbAr_CheckedChanged);
            // 
            // cbH2
            // 
            this.cbH2.AutoSize = true;
            this.cbH2.Checked = true;
            this.cbH2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbH2.Location = new System.Drawing.Point(7, 19);
            this.cbH2.Name = "cbH2";
            this.cbH2.Size = new System.Drawing.Size(15, 14);
            this.cbH2.TabIndex = 5;
            this.cbH2.UseVisualStyleBackColor = true;
            this.cbH2.CheckedChanged += new System.EventHandler(this.cbH2_CheckedChanged);
            // 
            // lbArValue
            // 
            this.lbArValue.AutoSize = true;
            this.lbArValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbArValue.Location = new System.Drawing.Point(58, 116);
            this.lbArValue.Name = "lbArValue";
            this.lbArValue.Size = new System.Drawing.Size(16, 16);
            this.lbArValue.TabIndex = 11;
            this.lbArValue.Text = "0";
            // 
            // lbO2Value
            // 
            this.lbO2Value.AutoSize = true;
            this.lbO2Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbO2Value.Location = new System.Drawing.Point(57, 37);
            this.lbO2Value.Name = "lbO2Value";
            this.lbO2Value.Size = new System.Drawing.Size(16, 16);
            this.lbO2Value.TabIndex = 10;
            this.lbO2Value.Text = "0";
            // 
            // lbCOValue
            // 
            this.lbCOValue.AutoSize = true;
            this.lbCOValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbCOValue.Location = new System.Drawing.Point(58, 59);
            this.lbCOValue.Name = "lbCOValue";
            this.lbCOValue.Size = new System.Drawing.Size(16, 16);
            this.lbCOValue.TabIndex = 9;
            this.lbCOValue.Text = "0";
            // 
            // lbN2Value
            // 
            this.lbN2Value.AutoSize = true;
            this.lbN2Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbN2Value.Location = new System.Drawing.Point(58, 98);
            this.lbN2Value.Name = "lbN2Value";
            this.lbN2Value.Size = new System.Drawing.Size(16, 16);
            this.lbN2Value.TabIndex = 8;
            this.lbN2Value.Text = "0";
            // 
            // lbCO2Value
            // 
            this.lbCO2Value.AutoSize = true;
            this.lbCO2Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbCO2Value.Location = new System.Drawing.Point(58, 79);
            this.lbCO2Value.Name = "lbCO2Value";
            this.lbCO2Value.Size = new System.Drawing.Size(16, 16);
            this.lbCO2Value.TabIndex = 7;
            this.lbCO2Value.Text = "0";
            // 
            // lbH2Value
            // 
            this.lbH2Value.AutoSize = true;
            this.lbH2Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbH2Value.Location = new System.Drawing.Point(58, 16);
            this.lbH2Value.Name = "lbH2Value";
            this.lbH2Value.Size = new System.Drawing.Size(16, 16);
            this.lbH2Value.TabIndex = 6;
            this.lbH2Value.Text = "0";
            // 
            // lbAr
            // 
            this.lbAr.AutoSize = true;
            this.lbAr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbAr.ForeColor = System.Drawing.Color.Turquoise;
            this.lbAr.Location = new System.Drawing.Point(25, 116);
            this.lbAr.Name = "lbAr";
            this.lbAr.Size = new System.Drawing.Size(23, 16);
            this.lbAr.TabIndex = 5;
            this.lbAr.Text = "Ar";
            // 
            // lbO2
            // 
            this.lbO2.AutoSize = true;
            this.lbO2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbO2.ForeColor = System.Drawing.Color.Blue;
            this.lbO2.Location = new System.Drawing.Point(24, 37);
            this.lbO2.Name = "lbO2";
            this.lbO2.Size = new System.Drawing.Size(27, 16);
            this.lbO2.TabIndex = 4;
            this.lbO2.Text = "O2";
            // 
            // lbCO
            // 
            this.lbCO.AutoSize = true;
            this.lbCO.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbCO.ForeColor = System.Drawing.Color.Red;
            this.lbCO.Location = new System.Drawing.Point(24, 58);
            this.lbCO.Name = "lbCO";
            this.lbCO.Size = new System.Drawing.Size(29, 16);
            this.lbCO.TabIndex = 3;
            this.lbCO.Text = "CO";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.Orange;
            this.label5.Location = new System.Drawing.Point(24, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "CO2";
            // 
            // lbCO2
            // 
            this.lbCO2.AutoSize = true;
            this.lbCO2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbCO2.Location = new System.Drawing.Point(24, 79);
            this.lbCO2.Name = "lbCO2";
            this.lbCO2.Size = new System.Drawing.Size(37, 16);
            this.lbCO2.TabIndex = 2;
            this.lbCO2.Text = "CO2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(25, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 16);
            this.label6.TabIndex = 1;
            this.label6.Text = "N2";
            // 
            // lbN2
            // 
            this.lbN2.AutoSize = true;
            this.lbN2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbN2.Location = new System.Drawing.Point(25, 97);
            this.lbN2.Name = "lbN2";
            this.lbN2.Size = new System.Drawing.Size(27, 16);
            this.lbN2.TabIndex = 1;
            this.lbN2.Text = "N2";
            // 
            // lbH2
            // 
            this.lbH2.AutoSize = true;
            this.lbH2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbH2.ForeColor = System.Drawing.Color.Green;
            this.lbH2.Location = new System.Drawing.Point(25, 17);
            this.lbH2.Name = "lbH2";
            this.lbH2.Size = new System.Drawing.Size(27, 16);
            this.lbH2.TabIndex = 0;
            this.lbH2.Text = "H2";
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.textBoxPassport);
            this.tabPage9.Controls.Add(this.tbPassport);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(896, 518);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "Паспорт плавки";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // textBoxPassport
            // 
            this.textBoxPassport.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxPassport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxPassport.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxPassport.Location = new System.Drawing.Point(0, 0);
            this.textBoxPassport.Multiline = true;
            this.textBoxPassport.Name = "textBoxPassport";
            this.textBoxPassport.ReadOnly = true;
            this.textBoxPassport.Size = new System.Drawing.Size(896, 518);
            this.textBoxPassport.TabIndex = 1;
            // 
            // tbPassport
            // 
            this.tbPassport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbPassport.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbPassport.Location = new System.Drawing.Point(0, 0);
            this.tbPassport.Multiline = true;
            this.tbPassport.Name = "tbPassport";
            this.tbPassport.Size = new System.Drawing.Size(896, 518);
            this.tbPassport.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.Location = new System.Drawing.Point(8, 577);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(109, 23);
            this.button2.TabIndex = 14;
            this.button2.Text = "Сохранить в Excel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(278, 579);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(626, 20);
            this.textBox1.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(238, 582);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Путь:";
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button4.Location = new System.Drawing.Point(123, 577);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(109, 23);
            this.button4.TabIndex = 21;
            this.button4.Text = "Сводный в Excel";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(916, 611);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.tPassport);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Сбор данных";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tPassport.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFusion)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOffGas)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLance)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTrakt)).EndInit();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMetalMirror)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewScrapBuckets)).EndInit();
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHotMetal)).EndInit();
            this.tabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSlagAnalysys)).EndInit();
            this.tabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSteelAnalysys)).EndInit();
            this.tabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSublance)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.gbValues.ResumeLayout(false);
            this.gbValues.PerformLayout();
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem открытьФайлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьФайлToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.TabControl tPassport;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridViewFusion;
        private System.Windows.Forms.DataGridView dataGridViewOffGas;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dataGridViewLance;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dataGridViewTrakt;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox gbValues;
        private System.Windows.Forms.Label lbTimeValue;
        private System.Windows.Forms.Label lbTime;
        private System.Windows.Forms.CheckBox cbO2;
        private System.Windows.Forms.CheckBox cbCO;
        private System.Windows.Forms.CheckBox cbCO2;
        private System.Windows.Forms.CheckBox cbN2;
        private System.Windows.Forms.CheckBox cbAr;
        private System.Windows.Forms.CheckBox cbH2;
        private System.Windows.Forms.Label lbArValue;
        private System.Windows.Forms.Label lbO2Value;
        private System.Windows.Forms.Label lbCOValue;
        private System.Windows.Forms.Label lbN2Value;
        private System.Windows.Forms.Label lbCO2Value;
        private System.Windows.Forms.Label lbH2Value;
        private System.Windows.Forms.Label lbAr;
        private System.Windows.Forms.Label lbO2;
        private System.Windows.Forms.Label lbCO;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbCO2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbN2;
        private System.Windows.Forms.Label lbH2;
        private ZedGraph.ZedGraphControl zGraph;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.DataGridView dataGridViewMetalMirror;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.DataGridView dataGridViewScrapBuckets;
        private System.Windows.Forms.CheckBox cbLance;
        private System.Windows.Forms.Label lbLanceValue;
        private System.Windows.Forms.Label lbLance;
        private System.Windows.Forms.CheckBox cbOFlow;
        private System.Windows.Forms.Label lbOFlowValue;
        private System.Windows.Forms.Label lbOFlow;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.DataGridView dataGridViewHotMetal;
        private System.Windows.Forms.GroupBox gbVars;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.CheckBox cbFlow;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cbTemp;
        private System.Windows.Forms.Label lalbel2333;
        private System.Windows.Forms.CheckBox cbRealCO;
        private System.Windows.Forms.Label lbRealCO;
        private System.Windows.Forms.CheckBox cbRealCO2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox cbRealFlow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TextBox tbPassport;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.DataGridView dataGridViewSlagAnalysys;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.DataGridView dataGridViewSteelAnalysys;
        private System.Windows.Forms.ToolStripMenuItem прпрToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem папкуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сводногоОтчетаToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.DataGridView dataGridViewSublance;
        private System.Windows.Forms.TextBox textBoxPassport;
    }
}

