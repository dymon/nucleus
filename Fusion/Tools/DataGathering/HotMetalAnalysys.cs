﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatInfo
{
    public class HotMetalAnalysys
    {
        public double C { get; set; }
        public double Si { get; set; }
        public double Mn { get; set; }
        public double P { get; set; }
        public double S { get; set; }
    }
}
