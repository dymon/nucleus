﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EsmsFusionProtocol
{
    class Additions
    {
        public DateTime Time { get; set; }
        public float Weight { get; set; }
        public string Name { get; set; }
        public int Index { get; set; }
    }
}
