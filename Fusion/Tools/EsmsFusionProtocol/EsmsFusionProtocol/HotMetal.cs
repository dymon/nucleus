﻿using System;

namespace EsmsFusionProtocol
{
    class HotMetal
    {
        public DateTime Time { get; set; }
        public int LadleNUmber { get; set; }
        public float Weight { get; set; }
        public float Temperature { get; set; }
    }
}
