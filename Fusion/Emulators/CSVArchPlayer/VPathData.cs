﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSVArchPlayer {
    internal class VPathData {
        public double RB5 { get; set; }
        public double RB6 { get; set; }
        public double RB7 { get; set; }
        public double RB8 { get; set; }
        public double RB9 { get; set; }
        public double RB10 { get; set; }
        public double RB11 { get; set; }
        public double RB12 { get; set; }
    }
}