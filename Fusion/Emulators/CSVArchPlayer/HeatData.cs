﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSVArchPlayer {
    internal class HeatData {
        public DateTime DTime;
        public int HeightLance;
        public double RateO2;
        public double H2;
        public double O2;
        public double CO;
        public double CO2;
        public double N2;
        public double Ar;
        public double VOffGas;
        public double TOffGas;
        public int DecompressionOffGas;
        public double SublanceC;
        public VPathData Bunkers = new VPathData();
    }
}